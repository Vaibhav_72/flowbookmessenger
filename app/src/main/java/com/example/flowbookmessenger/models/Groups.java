package com.example.flowbookmessenger.models;

import java.util.ArrayList;

public class Groups {
    private String groupName;
    private String groupDescription;
    private String groupID;
    private String groupAdminID;
    private String groupAdminName;
    private String groupKey;
    private String search;
    private int groupParticipantsCount;
    private ArrayList<String> groupParticipantsID;
    private ArrayList<String> groupParticipantsName;

    public Groups(String groupName, String groupDescription, String groupID, String groupAdminID, String groupAdminName, String groupKey, String search, int groupParticipantsCount, ArrayList<String> groupParticipantsID, ArrayList<String> groupParticipantsName) {
        this.groupName = groupName;
        this.groupDescription = groupDescription;
        this.groupID = groupID;
        this.groupAdminID = groupAdminID;
        this.groupAdminName = groupAdminName;
        this.groupKey = groupKey;
        this.search = search;
        this.groupParticipantsCount = groupParticipantsCount;
        this.groupParticipantsID = groupParticipantsID;
        this.groupParticipantsName = groupParticipantsName;
    }

    public Groups() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public ArrayList<String> getGroupParticipantsID() {
        return groupParticipantsID;
    }

    public void setGroupParticipantsID(ArrayList<String> groupParticipantsID) {
        this.groupParticipantsID = groupParticipantsID;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getGroupAdminID() {
        return groupAdminID;
    }

    public void setGroupAdminID(String groupAdminID) {
        this.groupAdminID = groupAdminID;
    }

    public String getGroupAdminName() {
        return groupAdminName;
    }

    public void setGroupAdminName(String groupAdminName) {
        this.groupAdminName = groupAdminName;
    }

    public int getGroupParticipantsCount() {
        return groupParticipantsCount;
    }

    public void setGroupParticipantsCount(int groupParticipantsCount) {
        this.groupParticipantsCount = groupParticipantsCount;
    }

    public ArrayList<String> getGroupParticipantsName() {
        return groupParticipantsName;
    }

    public void setGroupParticipantsName(ArrayList<String> groupParticipantsName) {
        this.groupParticipantsName = groupParticipantsName;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public void setGroupKey(String groupKey) {
        this.groupKey = groupKey;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
