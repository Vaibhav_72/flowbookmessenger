package com.example.flowbookmessenger.models;

import java.util.ArrayList;

public class Canvas {

    private String canvasTime;
    private String canvasDate;
    private int width;
    private int height;
    private String key;
    private String canvasName;
    private String creatorName;
    private String creatorID;
    private String canvasThumbnail;
    private String canvasParentGroupID;
    private ArrayList<String> canvasParticipantsID;
    private ArrayList<String> canvasParticipantsName;

    public Canvas() {
    }

    public Canvas(String canvasTime, String canvasDate, int width, int height, String key, String canvasName, String creatorName, String creatorID, String canvasThumbnail, String canvasParentGroupID, ArrayList<String> canvasParticipantsID, ArrayList<String> canvasParticipantsName) {
        this.canvasTime = canvasTime;
        this.canvasDate = canvasDate;
        this.width = width;
        this.height = height;
        this.key = key;
        this.canvasName = canvasName;
        this.creatorName = creatorName;
        this.creatorID = creatorID;
        this.canvasThumbnail = canvasThumbnail;
        this.canvasParentGroupID = canvasParentGroupID;
        this.canvasParticipantsID = canvasParticipantsID;
        this.canvasParticipantsName = canvasParticipantsName;
    }

    public String getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(String creatorID) {
        this.creatorID = creatorID;
    }

    public String getCanvasParentGroupID() {
        return canvasParentGroupID;
    }

    public void setCanvasParentGroupID(String canvasParentGroupID) {
        this.canvasParentGroupID = canvasParentGroupID;
    }

    public String getCanvasThumbnail() {
        return canvasThumbnail;
    }

    public void setCanvasThumbnail(String canvasThumbnail) {
        this.canvasThumbnail = canvasThumbnail;
    }

    public String getCanvasName() {
        return canvasName;
    }

    public void setCanvasName(String canvasName) {
        this.canvasName = canvasName;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public ArrayList<String> getCanvasParticipantsID() {
        return canvasParticipantsID;
    }

    public void setCanvasParticipantsID(ArrayList<String> canvasParticipantsID) {
        this.canvasParticipantsID = canvasParticipantsID;
    }

    public ArrayList<String> getCanvasParticipantsName() {
        return canvasParticipantsName;
    }

    public void setCanvasParticipantsName(ArrayList<String> canvasParticipantsName) {
        this.canvasParticipantsName = canvasParticipantsName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCanvasTime() {
        return canvasTime;
    }

    public void setCanvasTime(String canvasTime) {
        this.canvasTime = canvasTime;
    }

    public String getCanvasDate() {
        return canvasDate;
    }

    public void setCanvasDate(String canvasDate) {
        this.canvasDate = canvasDate;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
