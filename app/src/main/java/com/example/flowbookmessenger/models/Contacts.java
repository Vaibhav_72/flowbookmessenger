package com.example.flowbookmessenger.models;

public class Contacts {
    private String userid;
    private String name;
    private String search;

    public Contacts(String userid, String name, String search) {
        this.userid = userid;
        this.name = name;
        this.search = search;
    }

    public Contacts() {
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
