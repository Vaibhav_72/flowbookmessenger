package com.example.flowbookmessenger.models;

import com.example.flowbookmessenger.models.CanvasPoint;

import java.util.ArrayList;
import java.util.List;

public class CanvasSegment {

    private List<CanvasPoint> points = new ArrayList<>();
    private int color;
    private float width;

    public CanvasSegment() {
    }

    public CanvasSegment(int color, float width) {
        this.color = color;
        this.width = width;
    }

    public void addPoint(int x, int y){
        CanvasPoint p = new CanvasPoint(x,y);
        points.add(p);
    }

    public List<CanvasPoint> getPoints() {
        return points;
    }

    public void setPoints(List<CanvasPoint> points) {
        this.points = points;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }
}
