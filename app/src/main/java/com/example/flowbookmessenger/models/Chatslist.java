package com.example.flowbookmessenger.models;

public class Chatslist {
    public String id;
    public String lastMsgDateTime;


    public Chatslist(String id, String lastMsgDateTime) {
        this.id = id;
        this.lastMsgDateTime = lastMsgDateTime;
    }

    public Chatslist() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastMsgDateTime() {
        return lastMsgDateTime;
    }

    public void setLastMsgDateTime(String lastMsgDateTime) {
        this.lastMsgDateTime = lastMsgDateTime;
    }
}
