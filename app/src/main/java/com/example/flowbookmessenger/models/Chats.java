package com.example.flowbookmessenger.models;

public class Chats {

    private String sender;
    private String receiver;
    private String message;
    private String m_type;
    private String message_Time;
    private String message_Date;
    private String message_DateTime;
    private String name;
    private String message_status;
    private String message_key;

    public Chats(String sender, String receiver, String message, String m_type, String message_Time, String message_Date, String message_DateTime, String name, String message_status, String message_key) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.m_type = m_type;
        this.message_Time = message_Time;
        this.message_Date = message_Date;
        this.message_DateTime = message_DateTime;
        this.name = name;
        this.message_status = message_status;
        this.message_key = message_key;
    }

    public Chats() {
    }

    public String getMessage_DateTime() {
        return message_DateTime;
    }

    public void setMessage_DateTime(String message_DateTime) {
        this.message_DateTime = message_DateTime;
    }

    public String getMessage_key() {
        return message_key;
    }

    public void setMessage_key(String message_key) {
        this.message_key = message_key;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getM_type() {
        return m_type;
    }

    public void setM_type(String m_type) {
        this.m_type = m_type;
    }

    public String getMessage_Time() {
        return message_Time;
    }

    public void setMessage_Time(String message_Time) {
        this.message_Time = message_Time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage_status() {
        return message_status;
    }

    public void setMessage_status(String message_status) {
        this.message_status = message_status;
    }

    public String getMessage_Date() {
        return message_Date;
    }

    public void setMessage_Date(String message_Date) {
        this.message_Date = message_Date;
    }
}
