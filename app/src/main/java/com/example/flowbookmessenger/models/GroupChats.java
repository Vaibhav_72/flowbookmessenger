package com.example.flowbookmessenger.models;

import java.util.ArrayList;

public class GroupChats {

    private String sender;
    private String group_ID;
    private String group_name;
    private String group_message;
    private String name;
    private String m_type;
    private String group_message_Date;
    private String group_message_Time;
    private String group_message_DateTime;
    private String group_message_status;
    private String group_message_key;
    private ArrayList<String> receivers_ID;
    private ArrayList<String> readBy;

    public GroupChats(String sender, String group_ID, String group_name, String group_message, String name, String m_type, String group_message_Date, String group_message_Time, String group_message_DateTime, String group_message_status, String group_message_key, ArrayList<String> receivers_ID, ArrayList<String> readBy) {
        this.sender = sender;
        this.group_ID = group_ID;
        this.group_name = group_name;
        this.group_message = group_message;
        this.name = name;
        this.m_type = m_type;
        this.group_message_Date = group_message_Date;
        this.group_message_Time = group_message_Time;
        this.group_message_DateTime = group_message_DateTime;
        this.group_message_status = group_message_status;
        this.group_message_key = group_message_key;
        this.receivers_ID = receivers_ID;
        this.readBy = readBy;
    }

    public GroupChats() {
    }

    public String getGroup_message_DateTime() {
        return group_message_DateTime;
    }

    public void setGroup_message_DateTime(String group_message_DateTime) {
        this.group_message_DateTime = group_message_DateTime;
    }

    public String getGroup_message_key() {
        return group_message_key;
    }

    public void setGroup_message_key(String group_message_key) {
        this.group_message_key = group_message_key;
    }

    public ArrayList<String> getReadBy() {
        return readBy;
    }

    public void setReadBy(ArrayList<String> readBy) {
        this.readBy = readBy;
    }

    public ArrayList<String> getReceivers_ID() {
        return receivers_ID;
    }

    public void setReceivers_ID(ArrayList<String> receivers_ID) {
        this.receivers_ID = receivers_ID;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getGroup_ID() {
        return group_ID;
    }

    public void setGroup_ID(String group_ID) {
        this.group_ID = group_ID;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_message() {
        return group_message;
    }

    public void setGroup_message(String group_message) {
        this.group_message = group_message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getM_type() {
        return m_type;
    }

    public void setM_type(String m_type) {
        this.m_type = m_type;
    }

    public String getGroup_message_Time() {
        return group_message_Time;
    }

    public void setGroup_message_Time(String group_message_Time) {
        this.group_message_Time = group_message_Time;
    }

    public String getGroup_message_status() {
        return group_message_status;
    }

    public void setGroup_message_status(String group_message_status) {
        this.group_message_status = group_message_status;
    }

    public String getGroup_message_Date() {
        return group_message_Date;
    }

    public void setGroup_message_Date(String group_message_Date) {
        this.group_message_Date = group_message_Date;
    }
}
