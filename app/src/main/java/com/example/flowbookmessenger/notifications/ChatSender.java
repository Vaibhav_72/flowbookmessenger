package com.example.flowbookmessenger.notifications;

public class ChatSender {
    public ChatData data;
    public String to;

    public ChatSender(ChatData data, String to) {
        this.data = data;
        this.to = to;
    }
}
