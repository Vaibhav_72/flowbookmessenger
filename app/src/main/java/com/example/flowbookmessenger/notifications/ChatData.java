package com.example.flowbookmessenger.notifications;

public class ChatData {

    private String user;
    private int icon;
    private String body;
    private String title;
    private String sented;
    private String notificationType;
    private String grpID;

    public ChatData() {
    }

    public ChatData(String user, int icon, String body, String title, String sented, String notificationType) {
        this.user = user;
        this.icon = icon;
        this.body = body;
        this.title = title;
        this.sented = sented;
        this.notificationType = notificationType;
    }

    public ChatData(String user, int icon, String body, String title, String sented, String notificationType, String grpID) {
        this.user = user;
        this.icon = icon;
        this.body = body;
        this.title = title;
        this.sented = sented;
        this.notificationType = notificationType;
        this.grpID = grpID;
    }

    public String getGrpID() {
        return grpID;
    }

    public void setGrpID(String grpID) {
        this.grpID = grpID;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSented() {
        return sented;
    }

    public void setSented(String sented) {
        this.sented = sented;
    }
}
