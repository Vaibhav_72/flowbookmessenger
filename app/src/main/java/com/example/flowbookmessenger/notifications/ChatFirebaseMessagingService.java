package com.example.flowbookmessenger.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.flowbookmessenger.activities.CanvasDrawActivity;
import com.example.flowbookmessenger.activities.ChatActivity;
import com.example.flowbookmessenger.activities.GroupChatActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class ChatFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String sented = remoteMessage.getData().get("sented");
        String user = remoteMessage.getData().get("user");

        SharedPreferences preferences = getSharedPreferences("PREFS", MODE_PRIVATE);
        String currentUser = preferences.getString("currentuser", "none");


        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser != null && sented.equals(firebaseUser.getUid())){
            if(!currentUser.equals(user)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    sendOreoNotification(remoteMessage);
                } else {
                    sendNotification(remoteMessage);
                }
            }
        }
    }

    private void sendOreoNotification(RemoteMessage remoteMessage) {
        String notificationType = remoteMessage.getData().get("notificationType");

        if (notificationType.equals("Chat")){
            String user = remoteMessage.getData().get("user");
            String icon = remoteMessage.getData().get("icon");
            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");

            RemoteMessage.Notification notification = remoteMessage.getNotification();
            int j = Integer.parseInt(user.replaceAll("[\\D]", ""));
            Intent intent = new Intent(this, ChatActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("userid",user);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,j,intent,PendingIntent.FLAG_ONE_SHOT);

            Uri defaultNotificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            OreoNotifications oreoNotifications = new OreoNotifications(this);
            Notification.Builder builder = oreoNotifications.getOreoNotification(title,body,pendingIntent,defaultNotificationSound,icon);

        /*int i = 0;
        if(j > 0){
            i = j;
        }*/
            oreoNotifications.getManager().notify((int) System.currentTimeMillis(),builder.build());
        }else if(notificationType.equals("GroupChat")){
            //String user = remoteMessage.getData().get("user");
            String icon = remoteMessage.getData().get("icon");
            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");
            String grpID = remoteMessage.getData().get("grpID");

            RemoteMessage.Notification notification = remoteMessage.getNotification();
            int j = Integer.parseInt(grpID.replaceAll("[\\D]", ""));
            Intent intent = new Intent(this, GroupChatActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("groupID",grpID);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,j,intent,PendingIntent.FLAG_ONE_SHOT);

            Uri defaultNotificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            OreoNotifications oreoNotifications = new OreoNotifications(this);
            Notification.Builder builder = oreoNotifications.getOreoNotification(title,body,pendingIntent,defaultNotificationSound,icon);

        /*int i = 0;
        if(j > 0){
            i = j;
        }*/
            oreoNotifications.getManager().notify((int) System.currentTimeMillis(),builder.build());
        }else if(notificationType.equals("Canvas")){
            String icon = remoteMessage.getData().get("icon");
            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");
            String canvasID = remoteMessage.getData().get("grpID");

            RemoteMessage.Notification notification = remoteMessage.getNotification();
            int j = Integer.parseInt(canvasID.replaceAll("[\\D]", ""));
            Intent intent = new Intent(this, CanvasDrawActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("canvasID",canvasID);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,j,intent,PendingIntent.FLAG_ONE_SHOT);

            Uri defaultNotificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            OreoNotifications oreoNotifications = new OreoNotifications(this);
            Notification.Builder builder = oreoNotifications.getOreoNotification(title,body,pendingIntent,defaultNotificationSound,icon);

        /*int i = 0;
        if(j > 0){
            i = j;
        }*/
            oreoNotifications.getManager().notify((int) System.currentTimeMillis(),builder.build());
        }

    }

    private void sendNotification(RemoteMessage remoteMessage) {
        String notificationType = remoteMessage.getData().get("notificationType");

        if(notificationType.equals("Chat")){
            String user = remoteMessage.getData().get("user");
            String icon = remoteMessage.getData().get("icon");
            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");

            RemoteMessage.Notification notification = remoteMessage.getNotification();
            int j = Integer.parseInt(user.replaceAll("[\\D]", ""));
            Intent intent = new Intent(this, ChatActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("userid",user);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,j,intent,PendingIntent.FLAG_ONE_SHOT);

            Uri defaultNotificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this).setSmallIcon(Integer.parseInt(icon))
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setSound(defaultNotificationSound)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /*int i = 0;
        if(j > 0){
            i = j;
        }*/
            notificationManager.notify((int) System.currentTimeMillis(),builder.build());
        }else if(notificationType.equals("GroupChat")){
            //String user = remoteMessage.getData().get("user");
            String icon = remoteMessage.getData().get("icon");
            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");
            String grpID = remoteMessage.getData().get("grpID");

            RemoteMessage.Notification notification = remoteMessage.getNotification();
            int j = Integer.parseInt(grpID.replaceAll("[\\D]", ""));
            Intent intent = new Intent(this, GroupChatActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("groupID",grpID);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,j,intent,PendingIntent.FLAG_ONE_SHOT);

            Uri defaultNotificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this).setSmallIcon(Integer.parseInt(icon))
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setSound(defaultNotificationSound)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /*int i = 0;
        if(j > 0){
            i = j;
        }*/
            notificationManager.notify((int) System.currentTimeMillis(),builder.build());
        }else if(notificationType.equals("Canvas")){
            String icon = remoteMessage.getData().get("icon");
            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");
            String canvasID = remoteMessage.getData().get("grpID");

            RemoteMessage.Notification notification = remoteMessage.getNotification();
            int j = Integer.parseInt(canvasID.replaceAll("[\\D]", ""));
            Intent intent = new Intent(this, CanvasDrawActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("canvasID",canvasID);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,j,intent,PendingIntent.FLAG_ONE_SHOT);

            Uri defaultNotificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this).setSmallIcon(Integer.parseInt(icon))
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setSound(defaultNotificationSound)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /*int i = 0;
        if(j > 0){
            i = j;
        }*/
            notificationManager.notify((int) System.currentTimeMillis(),builder.build());
        }
    }
}
