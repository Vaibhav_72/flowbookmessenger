package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.flowbookmessenger.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class SharePreviewActivity extends AppCompatActivity {

    ArrayList<String> contactID;
    ArrayList<String> contactName;
    ImageView preview_image;
    FloatingActionButton preview_send;
    String message_type,ssPath;
    StorageTask uploadTask;
    Calendar calendar;
    SimpleDateFormat date,time;
    String msgDate,msgTime,finalURL;
    DatabaseReference reference;
    FirebaseUser user;
    BitmapDrawable drawable;
    Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_preview);

        ssPath = getIntent().getStringExtra("path");
        contactID = getIntent().getStringArrayListExtra("contact ID");
        contactName = getIntent().getStringArrayListExtra("contact Name");

        preview_image = findViewById(R.id.preview_image);
        preview_send = findViewById(R.id.preview_send);

        preview_image.setImageURI(Uri.parse(ssPath));

        user = FirebaseAuth.getInstance().getCurrentUser();

        message_type = "gallery";

        drawable = (BitmapDrawable) preview_image.getDrawable();
        bitmap = drawable.getBitmap();

        preview_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (String receiverID : contactID){
                    uploadImage(user.getUid(),receiverID,bitmap,message_type);
                }
                Intent intent = new Intent(SharePreviewActivity.this, ShareScreenActivity.class);
                startActivity(intent);
            }
        });
    }

    public void uploadImage(final String sender, final String receiver, Bitmap b, final String msg_type){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        byte[] data = byteArrayOutputStream.toByteArray();

        final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Share Messages").child("Flowbook " + ssPath);
        UploadTask uploadTask = storageReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(SharePreviewActivity.this, "Message Failed", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                calendar = Calendar.getInstance();
                date = new SimpleDateFormat("dd-MMM-yyyy");
                time = new SimpleDateFormat("hh:mm a");
                msgDate = date.format(calendar.getTime());
                msgTime = time.format(calendar.getTime());

                Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                finalURL = result.toString();

                reference = FirebaseDatabase.getInstance().getReference();

                HashMap<String,Object> message_hashmap = new HashMap<>();

                message_hashmap.put("sender",sender);
                message_hashmap.put("receiver",receiver);
                message_hashmap.put("message",finalURL);
                message_hashmap.put("name","FlowBook " + ssPath);
                message_hashmap.put("m_type",msg_type);
                message_hashmap.put("message_Date",msgDate);
                message_hashmap.put("message_Time", msgTime);
                message_hashmap.put("message_status","sent");

                reference.child("Chats").push().setValue(message_hashmap);

                Toast.makeText(SharePreviewActivity.this, "Message Sent", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
