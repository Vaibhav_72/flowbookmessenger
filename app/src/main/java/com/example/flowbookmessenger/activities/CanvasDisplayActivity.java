package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.flowbookmessenger.adapters.CanvasDisplayAdapter;
import com.example.flowbookmessenger.fragments.APIService;
import com.example.flowbookmessenger.models.Contacts;
import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.models.Canvas;
import com.example.flowbookmessenger.notifications.ChatClient;
import com.example.flowbookmessenger.notifications.ChatData;
import com.example.flowbookmessenger.notifications.ChatResponse;
import com.example.flowbookmessenger.notifications.ChatSender;
import com.example.flowbookmessenger.notifications.ChatToken;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CanvasDisplayActivity extends AppCompatActivity {

    DatabaseReference baseReference;
    DatabaseReference canvasRefrence;
    Toolbar canvas_display_toolbar;
    RecyclerView canvas_display_recycler;
    Calendar calendar;
    SimpleDateFormat date;
    SimpleDateFormat time;
    String canvasDate;
    String canvasTime;
    CanvasDisplayAdapter canvasDisplayAdapter;
    List<Canvas> canvasList;
    ArrayList<String> canvasParticipantsID;
    ArrayList<String> canvasParticipantsName;
    Intent intent;
    FirebaseUser user;
    String userName;
    DatabaseReference userRefrence;
    DatabaseReference reference;
    String canvasName;
    String canvasParentGroupID;
    APIService apiService = null;
    boolean notify = false;
    ArrayList<String> notifyTo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canvas_display);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        apiService = ChatClient.getClient("https://fcm.googleapis.com/").create(APIService.class);

        intent = getIntent();
        canvasParticipantsID = intent.getStringArrayListExtra("canvasParticipantsID");
        canvasParticipantsName = intent.getStringArrayListExtra("canvasParticipantsName");
        canvasParentGroupID = intent.getStringExtra("canvasParentGroupID");

        canvas_display_recycler = findViewById(R.id.canvas_display_recycler);
        canvas_display_recycler.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        canvas_display_recycler.setLayoutManager(linearLayoutManager);

        canvasList = new ArrayList<>();

        user = FirebaseAuth.getInstance().getCurrentUser();

        canvas_display_toolbar = findViewById(R.id.canvas_display_toolbar);
        setSupportActionBar(canvas_display_toolbar);
        getSupportActionBar().setTitle("Available Sessions");

        baseReference = FirebaseDatabase.getInstance().getReference();
        canvasRefrence = FirebaseDatabase.getInstance().getReference().child("Canvas");
        userRefrence = FirebaseDatabase.getInstance().getReference().child("Users");
        userRefrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot s : dataSnapshot.getChildren()){
                    Contacts contacts = s.getValue(Contacts.class);
                    if(user.getUid().equals(contacts.getUserid())){
                        userName = contacts.getName();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        currentUserCanvas();
    }

    private void currentUserCanvas() {

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference canvasRef = FirebaseDatabase.getInstance().getReference("Canvas");

        canvasRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                canvasList.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    Canvas c = d.getValue(Canvas.class);
                    assert c != null;
                    assert user != null;
                    if(c.getCanvasParticipantsID().contains(user.getUid()) && c.getCanvasParentGroupID().equals(canvasParentGroupID)){
                        canvasList.add(c);
                    }
                }
                canvasDisplayAdapter = new CanvasDisplayAdapter(getApplicationContext(),canvasList);
                canvas_display_recycler.setAdapter(canvasDisplayAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void createCanvas(){

        final DatabaseReference newCanvasRefrence = canvasRefrence.push();
        HashMap<String,Object> newCanvas = new HashMap<>();

        calendar = Calendar.getInstance();
        date = new SimpleDateFormat("dd-MMM-yyyy");
        time = new SimpleDateFormat("hh:mm a");
        canvasDate = date.format(calendar.getTime());
        canvasTime = time.format(calendar.getTime());

        newCanvas.put("canvasTime",canvasTime);
        newCanvas.put("canvasDate",canvasDate);

        Point size = new Point();

        getWindowManager().getDefaultDisplay().getSize(size);

        newCanvas.put("width",size.x);
        newCanvas.put("height",size.y);
        newCanvas.put("key",newCanvasRefrence.getKey());
        newCanvas.put("canvasName",canvasName);
        newCanvas.put("creatorName",userName);
        newCanvas.put("creatorID",user.getUid());
        newCanvas.put("canvasThumbnail","");
        newCanvas.put("canvasParentGroupID",canvasParentGroupID);
        newCanvas.put("canvasParticipantsID",canvasParticipantsID);
        newCanvas.put("canvasParticipantsName",canvasParticipantsName);


        notifyTo = new ArrayList<String>();
        notifyTo.addAll(canvasParticipantsID);
        notifyTo.remove(user.getUid());
        newCanvasRefrence.setValue(newCanvas)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                            reference.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                    if (notify){
                                        for (String n : notifyTo){
                                            sendNotification(n,contacts.getName(),newCanvasRefrence.getKey());
                                        }
                                    }
                                    notify = false;
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                            openCanvas(newCanvasRefrence.getKey());
                        }
                    }
                })

        /*newCanvasRefrence.setValue(newCanvas, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if(databaseError != null){
                    throw databaseError.toException();
                }else {
                    openCanvas(newCanvasRefrence.getKey());
                }
            }
        })*/;
    }

    private void openCanvas(String key) {
        Intent intent = new Intent(CanvasDisplayActivity.this, CanvasDrawActivity.class);
        intent.putExtra("canvasID",key);
        //intent.putExtra("canvasName",canvasName);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.canvas_display_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.new_canvas){
            //createCanvas();
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
            bottomSheetDialog.setContentView(R.layout.custom_edittext_dialog);
            bottomSheetDialog.setCanceledOnTouchOutside(true);
            //final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            //View dialog = this.getLayoutInflater().inflate(R.layout.custom_edittext_dialog,null);
            TextView dialog_textView = (TextView) bottomSheetDialog.findViewById(R.id.dialog_textView);
            final EditText dialog_canvas_name = (EditText) bottomSheetDialog.findViewById(R.id.dialog_canvas_name);
            Button dialog_positive_button = (Button) bottomSheetDialog.findViewById(R.id.dialog_positive_button);
            Button dialog_negative_button = (Button) bottomSheetDialog.findViewById(R.id.dialog_negative_button);

            dialog_textView.setText("Create Session");
            dialog_canvas_name.setHint("Enter Session Name");
            dialog_negative_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //alertDialog.dismiss();
                    bottomSheetDialog.dismiss();
                }
            });
            dialog_positive_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canvasName = dialog_canvas_name.getText().toString().trim();
                    if(TextUtils.isEmpty(canvasName)){
                        Toast.makeText(CanvasDisplayActivity.this, "Please enter Session Name", Toast.LENGTH_SHORT).show();
                    }else {
                        notify = true;
                        createCanvas();
                        bottomSheetDialog.dismiss();
                        //alertDialog.dismiss();
                    }
                }
            });
            bottomSheetDialog.show();
            //alertDialog.setView(dialog);
            //alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
    private void sendNotification(final String receiver, final String name, final String canvasID) {
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference().child("Tokens");
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    ChatToken chatToken = snapshot.getValue(ChatToken.class);
                    ChatData chatData = new ChatData(user.getUid(),R.drawable.dubby_challenge,name + " started a live session",canvasName,receiver,"Canvas",canvasID);
                    ChatSender chatSender = new ChatSender(chatData, chatToken.getToken());

                    apiService.sendNotification(chatSender).enqueue(new Callback<ChatResponse>() {
                        @Override
                        public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                            if(response.code() == 200){
                                if(response.body().success != 1){
                                    Toast.makeText(CanvasDisplayActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ChatResponse> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
