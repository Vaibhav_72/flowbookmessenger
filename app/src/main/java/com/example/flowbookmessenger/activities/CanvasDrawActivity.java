package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.adapters.CanvasParticipantsAdapter;
import com.example.flowbookmessenger.models.CanvasSegment;
import com.example.flowbookmessenger.ui.CanvasDrawView;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.google.android.gms.common.util.Base64Utils;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class CanvasDrawActivity extends AppCompatActivity {

    public static final int THUMBNAIL_SIZE = 256;
    private CanvasDrawView canvasDrawView;
    private DatabaseReference baseReference;
    private DatabaseReference canvasDataRefrence;
    private DatabaseReference segmentRefrence;
    private DatabaseReference refrence;
    private ValueEventListener eventListener;
    private String canvasID,canvasName,creatorID;
    private int canvasWidth;
    private int canvasHeight;
    FirebaseUser user;
    private ArrayList<String> canvasParticipantsID;
    private ArrayList<String> canvasParticipantsName;
    View decorView;
    int uiOptions;
    CanvasParticipantsAdapter canvasParticipantsAdapter;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        //setTheme(android.R.style.Widget_Material_ActionBar);
        super.onCreate(savedInstanceState);



        /*decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/

        Intent canvasIntent = getIntent();
        canvasID = canvasIntent.getStringExtra("canvasID");
        refrence = FirebaseDatabase.getInstance().getReference().child("Canvas").child(canvasID);
        refrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                com.example.flowbookmessenger.models.Canvas canvas = dataSnapshot.getValue(com.example.flowbookmessenger.models.Canvas.class);
                assert canvas != null;
                canvasName = canvas.getCanvasName();
                creatorID = canvas.getCreatorID();
                canvasParticipantsID = canvas.getCanvasParticipantsID();
                canvasParticipantsName = canvas.getCanvasParticipantsName();
                getSupportActionBar().setTitle(canvasName);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //canvasName = canvasIntent.getStringExtra("canvasName");
        //canvasDescription = canvasIntent.getStringExtra("groupDescription");
        //canvasParticipantsID = canvasIntent.getStringArrayListExtra("groupParticipantsID");
        //canvasParticipantsName = canvasIntent.getStringArrayListExtra("groupParticipantsName");
        //getSupportActionBar().setTitle(canvasName);
        //Toast.makeText(this, canvasName, Toast.LENGTH_SHORT).show();

        user = FirebaseAuth.getInstance().getCurrentUser();

        baseReference = FirebaseDatabase.getInstance().getReference();
        canvasDataRefrence = baseReference.child("Canvas").child(canvasID);
        segmentRefrence = baseReference.child("canvasSegments").child(canvasID);
        canvasDataRefrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(canvasDrawView != null){
                    ((ViewGroup)canvasDrawView.getParent()).removeView(canvasDrawView);
                    canvasDrawView.cleanUp();
                    canvasDrawView = null;
                }
                Map<String,Object> canvasValues = (Map<String, Object>) dataSnapshot.getValue();
                if(canvasValues != null && canvasValues.get("width") != null && canvasValues.get("height") != null){
                    canvasWidth = ((Long) canvasValues.get("width")).intValue();
                    canvasHeight = ((Long)canvasValues.get("height")).intValue();

                    canvasDrawView = new CanvasDrawView(CanvasDrawActivity.this,baseReference.child("canvasSegments").child(canvasID),canvasWidth,canvasHeight);
                    setContentView(canvasDrawView);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public static String encodeCanvasToBase64(Bitmap canvasImage){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        canvasImage.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        String encodedCanvas = Base64Utils.encode(b);

        return encodedCanvas;
    }

    public static Bitmap decodeCanvasFromBase64(String canvasInput){
        byte[] decodedByte = Base64Utils.decode(canvasInput);
        return BitmapFactory.decodeByteArray(decodedByte,0,decodedByte.length);
    }

    public static void updateThumbnail(int cWidth, int cHeight, DatabaseReference segmentRef, final DatabaseReference canvasDataRef){
        final float scale = Math.min(1.0f * THUMBNAIL_SIZE / cWidth,1.0f * THUMBNAIL_SIZE / cHeight);
        final Bitmap b = Bitmap.createBitmap(Math.round(cWidth * scale),Math.round(cHeight * scale), Bitmap.Config.ARGB_8888);
        final Canvas canvasBuffer = new Canvas(b);

        canvasBuffer.drawRect(0,0,b.getWidth(),b.getHeight(),CanvasDrawView.paintFromColor(Color.WHITE, Paint.Style.FILL_AND_STROKE,2f));

        segmentRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot segmentSnapshot : dataSnapshot.getChildren()){
                    CanvasSegment canvasSegment = segmentSnapshot.getValue(CanvasSegment.class);
                    canvasBuffer.drawPath(CanvasDrawView.getPathForPoints(canvasSegment.getPoints(),scale),CanvasDrawView.paintFromColor(canvasSegment.getColor(),2f));
                }
                String encode = encodeCanvasToBase64(b);
                canvasDataRef.child("canvasThumbnail").setValue(encode, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        updateThumbnail(canvasWidth,canvasHeight,segmentRefrence,canvasDataRefrence);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.canvas_draw_options,menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder menuBuilder = (MenuBuilder) menu;
            menuBuilder.setOptionalIconsVisible(true);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(!(user.getUid().equals(creatorID))){
            invalidateOptionsMenu();
            menu.findItem(R.id.canvas_rename).setVisible(false);
            menu.findItem(R.id.canvas_end).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.canvas_clear){
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(CanvasDrawActivity.this);
            bottomSheetDialog.setContentView(R.layout.custom_eraser_dialog);
            bottomSheetDialog.setCanceledOnTouchOutside(true);
            //final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            //View clearCanvasView = this.getLayoutInflater().inflate(R.layout.custom_eraser_dialog,null);

            TextView eraser_tv = bottomSheetDialog.findViewById(R.id.eraser_tv);
            ImageButton eraser_20_ib = bottomSheetDialog.findViewById(R.id.eraser_20_ib);
            ImageButton eraser_40_ib = bottomSheetDialog.findViewById(R.id.eraser_40_ib);
            ImageButton eraser_60_ib = bottomSheetDialog.findViewById(R.id.eraser_60_ib);
            ImageButton eraser_80_ib = bottomSheetDialog.findViewById(R.id.eraser_80_ib);
            ImageButton eraser_100_ib = bottomSheetDialog.findViewById(R.id.eraser_100_ib);
            Button clear_all = bottomSheetDialog.findViewById(R.id.clear_all);

            eraser_tv.setText("Select Eraser");
            eraser_20_ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canvasDrawView.setStrokeWidth(20f);
                    canvasDrawView.setColor(Color.parseColor("#FFFFFF"));
                    bottomSheetDialog.dismiss();
                    //alertDialog.dismiss();
                }
            });
            eraser_40_ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canvasDrawView.setStrokeWidth(40f);
                    canvasDrawView.setColor(Color.parseColor("#FFFFFF"));
                    bottomSheetDialog.dismiss();
                    //alertDialog.dismiss();
                }
            });
            eraser_60_ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canvasDrawView.setStrokeWidth(60f);
                    canvasDrawView.setColor(Color.parseColor("#FFFFFF"));
                    bottomSheetDialog.dismiss();
                    //alertDialog.dismiss();
                }
            });
            eraser_80_ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canvasDrawView.setStrokeWidth(80f);
                    canvasDrawView.setColor(Color.parseColor("#FFFFFF"));
                    bottomSheetDialog.dismiss();
                    //alertDialog.dismiss();
                }
            });
            eraser_100_ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canvasDrawView.setStrokeWidth(100f);
                    canvasDrawView.setColor(Color.parseColor("#FFFFFF"));
                    bottomSheetDialog.dismiss();
                    //alertDialog.dismiss();
                }
            });
            clear_all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canvasDrawView.cleanUp();
                    segmentRefrence.removeValue(new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            if(databaseError != null){
                                throw databaseError.toException();
                            }
                            canvasDrawView = new CanvasDrawView(CanvasDrawActivity.this,baseReference.child("canvasSegments").child(canvasID),canvasWidth,canvasHeight);
                            setContentView(canvasDrawView);
                        }
                    });
                    bottomSheetDialog.dismiss();
                    //alertDialog.dismiss();
                }
            });
            //alertDialog.setView(clearCanvasView);
            //alertDialog.show();
            bottomSheetDialog.show();
            return true;
        }else if(item.getItemId() == R.id.canvas_path_color){
            /*CanvasPathColorDialog.ColorListener colorListener = new CanvasPathColorDialog.ColorListener() {
                @Override
                public void colorUpdate(int updatedColor) {
                    canvasDrawView.setColor(updatedColor);
                }
            };
            new CanvasPathColorDialog(this, colorListener,0xFF01579B).show();*/

            ColorPickerDialogBuilder.with(CanvasDrawActivity.this)
                    .setTitle("Select Color")
                    .initialColor(0xFF01579B)
                    .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                    .density(12)
                    .setOnColorSelectedListener(new OnColorSelectedListener() {
                        @Override
                        public void onColorSelected(int selectedColor) {

                        }
                    }).setPositiveButton("Select", new ColorPickerClickListener() {
                @Override
                public void onClick(DialogInterface d, int lastSelectedColor, Integer[] allColors) {
                    canvasDrawView.setColor(lastSelectedColor);
                }
            }).setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).build().show();
            return true;
        }else if(item.getItemId() == R.id.canvas_pencil_size){
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(CanvasDrawActivity.this);
            bottomSheetDialog.setContentView(R.layout.custom_pencil_size_dialog);
            bottomSheetDialog.setCanceledOnTouchOutside(true);
            //final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            //View v = this.getLayoutInflater().inflate(R.layout.custom_pencil_size_dialog,null);
            TextView select_size = (TextView) bottomSheetDialog.findViewById(R.id.select_size);
            final SeekBar pencil_size_seekbar = (SeekBar) bottomSheetDialog.findViewById(R.id.pencil_size_seekbar);
            final TextView selected_size = (TextView) bottomSheetDialog.findViewById(R.id.selected_size);
            Button dialog_seekbar_positive_button = (Button) bottomSheetDialog.findViewById(R.id.dialog_seekbar_positive_button);
            Button dialog_seekbar_negative_button = (Button) bottomSheetDialog.findViewById(R.id.dialog_seekbar_negative_button);

            select_size.setText("Select Pencil Size");
            pencil_size_seekbar.setMax(20);
            pencil_size_seekbar.setProgress(10);
            selected_size.setText(String.valueOf(pencil_size_seekbar.getProgress()));
            pencil_size_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    selected_size.setText(String.valueOf(progress));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
            dialog_seekbar_negative_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomSheetDialog.dismiss();
                    //alertDialog.dismiss();
                }
            });
            dialog_seekbar_positive_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(CanvasDrawActivity.this, String.valueOf(pencil_size_seekbar.getProgress()), Toast.LENGTH_SHORT).show();
                    canvasDrawView.setStrokeWidth(Float.parseFloat(String.valueOf(pencil_size_seekbar.getProgress())));
                    bottomSheetDialog.dismiss();
                    //alertDialog.dismiss();
                }
            });
            bottomSheetDialog.show();
            //alertDialog.setView(v);
            //alertDialog.show();
            return true;
        }else if(item.getItemId() == R.id.canvas_rename){
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(CanvasDrawActivity.this);
            bottomSheetDialog.setContentView(R.layout.rename_canvas);
            bottomSheetDialog.setCanceledOnTouchOutside(true);
            TextView rename_textView = bottomSheetDialog.findViewById(R.id.rename_textView);
            final EditText rename_edittext = bottomSheetDialog.findViewById(R.id.rename_edittext);
            Button rename_positive_button = bottomSheetDialog.findViewById(R.id.rename_positive_button);
            Button rename_negative_button = bottomSheetDialog.findViewById(R.id.rename_negative_button);
            rename_textView.setText("Rename Session");
            rename_edittext.setText(canvasName);
            rename_edittext.setSelection(rename_edittext.getText().length());
            rename_negative_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomSheetDialog.dismiss();
                }
            });
            rename_positive_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String canvasNewName = rename_edittext.getText().toString().trim();
                    if(TextUtils.isEmpty(canvasNewName)){
                        Toast.makeText(CanvasDrawActivity.this, "Please Enter Session Name", Toast.LENGTH_SHORT).show();
                    }else {
                        DatabaseReference updateRef = FirebaseDatabase.getInstance().getReference().child("Canvas").child(canvasID);
                        HashMap<String,Object> canvasUpdate = new HashMap<>();
                        canvasUpdate.put("canvasName",canvasNewName);
                        updateRef.updateChildren(canvasUpdate);
                        //getSupportActionBar().setTitle(canvasNewName);
                        bottomSheetDialog.dismiss();
                    }
                }
            });
            bottomSheetDialog.show();
            return true;
        }/*else if(item.getItemId() == R.id.canvas_end){
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(CanvasDrawActivity.this);
            bottomSheetDialog.setContentView(R.layout.delete_canvas_layout);
            bottomSheetDialog.setCanceledOnTouchOutside(true);
            TextView delete_textView = bottomSheetDialog.findViewById(R.id.delete_textView);
            TextView delete_confirmation_textView = bottomSheetDialog.findViewById(R.id.delete_confirmation_textView);
            Button delete_positive_button = bottomSheetDialog.findViewById(R.id.delete_positive_button);
            Button delete_negative_button = bottomSheetDialog.findViewById(R.id.delete_negative_button);
            delete_textView.setText("End Session");
            delete_confirmation_textView.setText("Are you sure you want to end " + canvasName + "?");
            delete_negative_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomSheetDialog.dismiss();
                }
            });
            delete_positive_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canvasDrawView.cleanUp();
                    segmentRefrence.removeValue(new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            if(databaseError != null){
                                throw databaseError.toException();
                            }
                        }
                    });
                    canvasDataRefrence.removeValue(new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            //Intent intent = new Intent(CanvasDrawActivity.this,HomeActivity.class);
                            //startActivity(intent);
                            finish();
                        }
                    });
                }
            });
            bottomSheetDialog.show();
            return true;
        }*/else if(item.getItemId() == R.id.canvas_participants){
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(CanvasDrawActivity.this);
            bottomSheetDialog.setContentView(R.layout.canvas_participants_bottom_sheet);
            bottomSheetDialog.setCanceledOnTouchOutside(true);
            RecyclerView canvas_participants_recyclerview = bottomSheetDialog.findViewById(R.id.canvas_participants_recyclerview);
            ImageButton canvas_add_participant = bottomSheetDialog.findViewById(R.id.canvas_add_participant);
            TextView participants_textView = bottomSheetDialog.findViewById(R.id.participants_textView);
            TextView participants_count = bottomSheetDialog.findViewById(R.id.participants_count);
            participants_textView.setText("Participants");
            participants_count.setText(String.valueOf(canvasParticipantsName.size()));
            if(!(user.getUid().equals(creatorID))){
                canvas_add_participant.setVisibility(View.GONE);
            }
            canvas_participants_recyclerview.setLayoutManager(new LinearLayoutManager(this));
            canvasParticipantsAdapter = new CanvasParticipantsAdapter(this,canvasParticipantsName);
            canvas_participants_recyclerview.setAdapter(canvasParticipantsAdapter);
            canvas_add_participant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CanvasDrawActivity.this,AddCanvasParticipantActivity.class);
                    intent.putExtra("canvasID",canvasID);
                    startActivity(intent);
                    finish();
                }
            });
            bottomSheetDialog.show();
            return true;
        }else if(item.getItemId() == R.id.canvas_screenshot){
            /*Bitmap bitmap;
            canvasDrawView.setDrawingCacheEnabled(true);
            bitmap = Bitmap.createBitmap(canvasDrawView.getDrawingCache());
            canvasDrawView.setDrawingCacheEnabled(false);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,40,byteArrayOutputStream);
            File f = new File(Environment.getExternalStorageState() + File.separator + "canvas.jpeg");
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(f);
                fileOutputStream.write(byteArrayOutputStream.toByteArray());
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat date = new SimpleDateFormat("dd-MMM-yyyy");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm:ss a");
            String ssDate = date.format(calendar.getTime());
            String ssTime = time.format(calendar.getTime());
            Bitmap b = getBitmapFromView(canvasDrawView,canvasHeight,canvasWidth);
            String ssName = "screenshot_" + ssDate + "_" + ssTime + ".png";
            store(b,ssName);
            Toast.makeText(this, "Saved to Internal Storage", Toast.LENGTH_SHORT).show();
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        decorView.setSystemUiVisibility(uiOptions);

        canvasDataRefrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(canvasDrawView != null){
                    ((ViewGroup)canvasDrawView.getParent()).removeView(canvasDrawView);
                    canvasDrawView.cleanUp();
                    canvasDrawView = null;
                }
                Map<String,Object> canvasValues = (Map<String, Object>) dataSnapshot.getValue();
                if(canvasValues != null && canvasValues.get("width") != null && canvasValues.get("height") != null){
                    canvasWidth = ((Long) canvasValues.get("width")).intValue();
                    canvasHeight = ((Long)canvasValues.get("height")).intValue();

                    canvasDrawView = new CanvasDrawView(CanvasDrawActivity.this,baseReference.child("canvasSegments").child(canvasID),canvasWidth,canvasHeight);
                    setContentView(canvasDrawView);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }*/

    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }
    public void store(Bitmap bitmap,String filename){
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/FlowBook Screenshot";
        File dir = new File(dirPath);
        if(!dir.exists()){
            dir.mkdirs();
        }
        File file = new File(dirPath,filename);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG,100,fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
