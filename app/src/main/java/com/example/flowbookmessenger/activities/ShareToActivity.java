package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.adapters.ContactsAdapter;
import com.example.flowbookmessenger.models.Contacts;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ShareToActivity extends AppCompatActivity {

    RecyclerView share_to_activity_recycler_view;
    ContactsAdapter contactsAdapter;
    List<Contacts> contactsList;
    FloatingActionButton share_to_activity_send;
    MaterialToolbar share_to_activity_toolbar;
    Intent intent;
    String ssPath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_to);

        intent = getIntent();
        ssPath = intent.getStringExtra("path");
        /*if(getIntent().hasExtra("byteArray")){
            Bitmap ssBitmap = BitmapFactory.decodeByteArray(getIntent().getByteArrayExtra("byteArray"),0,getIntent().getByteArrayExtra("byteArray").length);
        }*/
        share_to_activity_toolbar = findViewById(R.id.share_to_activity_toolbar);
        setSupportActionBar(share_to_activity_toolbar);
        getSupportActionBar().setTitle("Share to");

        share_to_activity_recycler_view = findViewById(R.id.share_to_activity_recycler_view);
        share_to_activity_recycler_view.setHasFixedSize(true);
        share_to_activity_recycler_view.setLayoutManager(new LinearLayoutManager(this));

        share_to_activity_send = findViewById(R.id.share_to_activity_send);
        share_to_activity_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(contactsAdapter.getSelectedContactsID().size() < 1){
                    Toast.makeText(ShareToActivity.this, "Please select atleast 1 Contact to share with", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(ShareToActivity.this, SharePreviewActivity.class);
                    intent.putExtra("path",ssPath);
                    intent.putStringArrayListExtra("contact ID",contactsAdapter.getSelectedContactsID());
                    intent.putStringArrayListExtra("contact Name",contactsAdapter.getSelectedContactsName());
                    startActivity(intent);
                }
            }
        });

        contactsList = new ArrayList<>();

        availableUsers();
    }

    private void availableUsers() {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contactsList.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    Contacts contacts = d.getValue(Contacts.class);
                    assert contacts != null;
                    assert user != null;
                    if(!user.getUid().equals(contacts.getUserid())) {
                        contactsList.add(contacts);
                    }
                }
                contactsAdapter = new ContactsAdapter(ShareToActivity.this,contactsList,false);
                share_to_activity_recycler_view.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
