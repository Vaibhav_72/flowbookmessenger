package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.flowbookmessenger.adapters.GroupParticipantsAdapter;
import com.example.flowbookmessenger.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class CreateGroupActivity extends AppCompatActivity {

    //Intent selectedContactIntent;
    CircleImageView create_group_group_icon;
    EditText create_group_group_name,create_group_group_description;
    TextView create_group_participants_count;
    FloatingActionButton create_group_create;
    GridView create_group_participants_gridview;
    ArrayList<String> selectionsID;
    ArrayList<String> selectionsName;
    FirebaseUser currentUser;
    FirebaseAuth auth;
    DatabaseReference referenceUserName,reference;
    String currentUserID,currentUserName;
    //String grpKey;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        MaterialToolbar create_group_toolbar = (MaterialToolbar) findViewById(R.id.create_group_toolbar);
        setSupportActionBar(create_group_toolbar);
        getSupportActionBar().setTitle("Create Group");
        getSupportActionBar().setSubtitle("Group Details");

        //selectedContactIntent = getIntent();
        selectionsID = getIntent().getStringArrayListExtra("Selected Contacts ID");
        selectionsName = getIntent().getStringArrayListExtra("Selected Contacts Name");

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        assert currentUser != null;
        currentUserID = currentUser.getUid();
        reference = FirebaseDatabase.getInstance().getReference();
        //Toast.makeText(this, currentUserName, Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, selectionsName + "", Toast.LENGTH_SHORT).show();

        create_group_group_icon = findViewById(R.id.create_group_group_icon);
        create_group_create = findViewById(R.id.create_group_create);
        create_group_group_name = findViewById(R.id.create_group_group_name);
        create_group_group_description = findViewById(R.id.create_group_group_description);
        create_group_participants_count = findViewById(R.id.create_group_participants_count);

        create_group_participants_count.setText(String.valueOf(selectionsID.size()));

        create_group_participants_gridview = findViewById(R.id.create_group_participants_gridview);

        GroupParticipantsAdapter groupParticipantsAdapter = new GroupParticipantsAdapter(this,selectionsName);
        create_group_participants_gridview.setAdapter(groupParticipantsAdapter);

        create_group_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String group_name = create_group_group_name.getText().toString().trim();
                final String group_description = create_group_group_description.getText().toString().trim();
                if(TextUtils.isEmpty(group_name)){
                    Toast.makeText(CreateGroupActivity.this, "Please Enter a Group Name", Toast.LENGTH_SHORT).show();
                }else {
                    referenceUserName = FirebaseDatabase.getInstance().getReference("Users").child(currentUserID).child("name");
                    referenceUserName.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            currentUserName = dataSnapshot.getValue(String.class);
                            selectionsID.add(currentUserID);
                            selectionsName.add(currentUserName);

                            reference = FirebaseDatabase.getInstance().getReference().child("Groups");
                            DatabaseReference newGroupRef = reference.push();

                            final HashMap<String,Object> group_hashmap = new HashMap<>();
                            group_hashmap.put("groupName",group_name);
                            group_hashmap.put("groupDescription",group_description);
                            group_hashmap.put("groupID",(newGroupRef.getKey()));
                            group_hashmap.put("groupAdminID",currentUserID);
                            group_hashmap.put("groupAdminName",currentUserName);
                            group_hashmap.put("groupParticipantsCount",selectionsID.size());
                            group_hashmap.put("groupParticipantsID",selectionsID);
                            group_hashmap.put("groupParticipantsName",selectionsName);
                            group_hashmap.put("search",group_name.toLowerCase());

                            newGroupRef.setValue(group_hashmap);

                            Intent intent = new Intent(CreateGroupActivity.this, GroupChatActivity.class);
                            intent.putExtra("groupID",(newGroupRef.getKey()));
                            /*intent.putExtra("groupName",group_name);
                            intent.putExtra("groupDescription",group_description);
                            intent.putExtra("groupAdminID",currentUserID);
                            intent.putExtra("groupAdminName",currentUserName);
                            intent.putExtra("groupParticipantsCount",selectionsID.size());
                            intent.putStringArrayListExtra("groupParticipantsID",selectionsID);
                            intent.putStringArrayListExtra("groupParticipantsName",selectionsName);*/
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    //Toast.makeText(CreateGroupActivity.this, selectionsName + "", Toast.LENGTH_SHORT).show();
                    //selectionsID.add(currentUserID);

                }
            }
        });
    }
}
