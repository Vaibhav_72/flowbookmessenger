package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.EditText;

import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.fragments.ChatsFragment;
import com.example.flowbookmessenger.fragments.ContactsFragment;
import com.example.flowbookmessenger.fragments.GroupsFragment;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class HomeActivity extends AppCompatActivity {

    TabLayout home_tabs;
    ViewPager home_view_pager;
    AppBarLayout home_app_bar;
    FirebaseAuth auth;
    EditText home_search;
    FirebaseUser user;
    DatabaseReference reference,rootRef;
    MaterialToolbar home_toolbar;
    Calendar calendar;
    SimpleDateFormat date,time;
    String currentDate,currentTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }


        rootRef = FirebaseDatabase.getInstance().getReference();

        home_toolbar = (MaterialToolbar) findViewById(R.id.home_toolbar);
        setSupportActionBar(home_toolbar);
        getSupportActionBar().setTitle("FlowBook Messenger");

        home_app_bar = (AppBarLayout) findViewById(R.id.home_app_bar);

        home_view_pager = (ViewPager) findViewById(R.id.home_view_pager);

        home_search = (EditText) findViewById(R.id.home_search);

        home_tabs = (TabLayout) findViewById(R.id.home_tabs);

        /*home_tabs.getTabAt(0).setIcon(R.drawable.tabs_chats);
        home_tabs.getTabAt(1).setIcon(R.drawable.tabs_calls);
        home_tabs.getTabAt(2).setIcon(R.drawable.tabs_groups);
        home_tabs.getTabAt(3).setIcon(R.drawable.tabs_contacts);*/

        auth = FirebaseAuth.getInstance();

        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());


        TabViewPagerAdapter tabViewPagerAdapter = new TabViewPagerAdapter(getSupportFragmentManager());
        tabViewPagerAdapter.addFragment(new ChatsFragment(),"Chats");
        tabViewPagerAdapter.addFragment(new GroupsFragment(),"Groups");
        tabViewPagerAdapter.addFragment(new ContactsFragment(),"Contacts");

        home_view_pager.setAdapter(tabViewPagerAdapter);

        home_tabs.setupWithViewPager(home_view_pager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null){
            userStatus("online");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null){
            userStatus("offline");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null){
            userStatus("offline");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.home_options_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);

        if(item.getItemId() == R.id.home_menu_group){
            Intent intent = new Intent(HomeActivity.this, ContactsActivity.class);
            startActivity(intent);
        }/*else if(item.getItemId() == R.id.home_menu_important){

        }else if(item.getItemId() == R.id.home_menu_settings){
            Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
            startActivity(intent);
        }*/else if(item.getItemId() == R.id.home_menu_logout){
            userStatus("offline");
            auth.signOut();
            Intent intent = new Intent(HomeActivity.this, WelcomeActivity.class);
            startActivity(intent);
            finish();
        }
        else if(item.getItemId() == R.id.home_menu_search){
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) item.getActionView();
            if(null != searchView){
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                searchView.setIconifiedByDefault(false);
            }
            //searchView.setQueryHint("Search");
            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    PagerAdapter pagerAdapter = (PagerAdapter) home_view_pager.getAdapter();
                    for (int i = 0; i < pagerAdapter.getCount(); i++){
                        Fragment viewPagerFragment = (Fragment) home_view_pager.getAdapter().instantiateItem(home_view_pager,i);
                        if(viewPagerFragment != null && viewPagerFragment.isAdded()){
                            if(viewPagerFragment instanceof ChatsFragment){
                                ChatsFragment chatsFragment = (ChatsFragment) viewPagerFragment;
                                if(chatsFragment != null){
                                    //chatsFragment.beginSearch(newText.toLowerCase());
                                }
                            }else if(viewPagerFragment instanceof GroupsFragment){
                                GroupsFragment groupsFragment = (GroupsFragment) viewPagerFragment;
                                if(groupsFragment != null){
                                    groupsFragment.beginSearch(newText.toLowerCase());
                                }
                            }else if(viewPagerFragment instanceof ContactsFragment){
                                ContactsFragment contactsFragment = (ContactsFragment) viewPagerFragment;
                                if(contactsFragment != null){
                                    contactsFragment.beginSearch(newText.toLowerCase());
                                }
                            }
                        }
                    }
                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        return true;
    }

    class TabViewPagerAdapter extends FragmentPagerAdapter{

        private ArrayList<Fragment> fragments;
        private ArrayList<String> fragments_title;

        public TabViewPagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
            this.fragments = new ArrayList<>();
            this.fragments_title = new ArrayList<>();
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public void addFragment(Fragment fragment,String title){
            fragments.add(fragment);
            fragments_title.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return fragments_title.get(position);
        }
    }

    private void userStatus(String status){
        calendar = Calendar.getInstance();
        date = new SimpleDateFormat("dd-MMM-yyyy");
        time = new SimpleDateFormat("hh:mm a");
        currentDate = date.format(calendar.getTime());
        currentTime = time.format(calendar.getTime());

        HashMap<String,Object> user_hashmap = new HashMap<>();

        user_hashmap.put("time",currentTime);
        user_hashmap.put("date",currentDate);
        user_hashmap.put("status",status);

        rootRef.child("Users").child(user.getUid()).child("UserStatus").updateChildren(user_hashmap);
    }
}
