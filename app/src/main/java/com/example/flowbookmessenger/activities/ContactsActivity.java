package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.flowbookmessenger.adapters.ContactsAdapter;
import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.models.Contacts;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ContactsActivity extends AppCompatActivity {

    RecyclerView contacts_activity_recycler_view;
    ContactsAdapter contactsAdapter;
    List<Contacts> contactsList;
    FloatingActionButton contact_activity_create_grp_btn;
    MaterialToolbar contacts_activity_toolbar;
    //ImageButton contacts_activity_toolbar_seacrh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        contacts_activity_toolbar = (MaterialToolbar) findViewById(R.id.contacts_activity_toolbar);
        setSupportActionBar(contacts_activity_toolbar);
        getSupportActionBar().setTitle("Select Contacts");
        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        contacts_activity_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactsActivity.this,HomeActivity.class);
                startActivity(intent);
            }
        });*/

        contacts_activity_recycler_view = (RecyclerView) findViewById(R.id.contacts_activity_recycler_view);
        contacts_activity_recycler_view.setHasFixedSize(true);
        contacts_activity_recycler_view.setLayoutManager(new LinearLayoutManager(this));

        contactsList = new ArrayList<>();

        contact_activity_create_grp_btn = (FloatingActionButton) findViewById(R.id.contact_activity_create_grp_btn);

        contact_activity_create_grp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(contactsAdapter.getSelectedContactsID().size() < 2){
                    Toast.makeText(ContactsActivity.this, "Please Select atleast 2 contacts to create a group", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(ContactsActivity.this, CreateGroupActivity.class);
                    intent.putStringArrayListExtra("Selected Contacts ID",contactsAdapter.getSelectedContactsID());
                    intent.putStringArrayListExtra("Selected Contacts Name",contactsAdapter.getSelectedContactsName());
                    startActivity(intent);
                }
            }
        });
        availableUsers();
    }

    private void availableUsers() {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contactsList.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    Contacts contacts = d.getValue(Contacts.class);
                    assert contacts != null;
                    assert user != null;
                    if(!user.getUid().equals(contacts.getUserid())) {
                        contactsList.add(contacts);
                    }
                }
                contactsAdapter = new ContactsAdapter(ContactsActivity.this,contactsList,false);
                contacts_activity_recycler_view.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contacts_activity_menu,menu);
        MenuItem item = menu.findItem(R.id.contacts_activity_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setQueryHint("Search Contacts");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
