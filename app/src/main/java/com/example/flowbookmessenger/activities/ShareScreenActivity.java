package com.example.flowbookmessenger.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.flowbookmessenger.R;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ShareScreenActivity extends AppCompatActivity {

    ScrollView share_scrollview;
    RelativeLayout share_relative;
    ImageView share_imageview,share_imageview2;
    TextView share_title,share_description;
    Button share_button,share_chat_section;
    Calendar calendar;
    SimpleDateFormat date,time;
    String ssDate,ssTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_screen);

        share_scrollview = findViewById(R.id.share_scrollview);
        share_relative = findViewById(R.id.share_relative);
        share_imageview = findViewById(R.id.share_imageview);
        share_imageview2 = findViewById(R.id.share_imageview2);
        share_title = findViewById(R.id.share_title);
        share_description = findViewById(R.id.share_description);
        share_button = findViewById(R.id.share_button);
        share_chat_section = findViewById(R.id.share_chat_section);

        //final View rootView = getWindow().getDecorView().findViewById(android.R.id.content);

        share_chat_section.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShareScreenActivity.this, WelcomeActivity.class);
                startActivity(intent);
            }
        });

        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                date = new SimpleDateFormat("dd-MMM-yyyy");
                time = new SimpleDateFormat("hh:mm:ss a");
                ssDate = date.format(calendar.getTime());
                ssTime = time.format(calendar.getTime());
                //String screenshotPath = null;
                Bitmap b = getBitmapFromView(share_scrollview,share_scrollview.getChildAt(0).getHeight(),share_scrollview.getChildAt(0).getWidth());
                String ssName = "screenshot_" + ssDate + "_" + ssTime + ".png";
                store(b,ssName);
                String ssPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/FlowBook Screenshot/" + ssName;
                //Toast.makeText(ShareScreenActivity.this, ssPath, Toast.LENGTH_SHORT).show();
                Intent shareIntent = new Intent(ShareScreenActivity.this, ShareToActivity.class);
                shareIntent.putExtra("path",ssPath);
                startActivity(shareIntent);
                //if(b != null){
                //    screenshotPath = MediaStore.Images.Media.insertImage(getContentResolver(),b,"flowbook_" + ssDate + "_" + ssTime,null);
                //}
                //Intent shareIntent = new Intent(ShareScreenActivity.this,ShareToActivity.class);
                //BitmapHelper.getInstance().setBitmap(b);
                //ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                //b.compress(Bitmap.CompressFormat.JPEG,50,byteArrayOutputStream);
                //shareIntent.putExtra("byteArray",byteArrayOutputStream.toByteArray());
                //startActivity(shareIntent);
                //share(screenshotPath);
            }
        });
    }
    private Bitmap getBitmapFromView(View view, int height, int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }
    public void store(Bitmap bitmap,String filename){
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/FlowBook Screenshot";
        File dir = new File(dirPath);
        if(!dir.exists()){
            dir.mkdirs();
        }
        File file = new File(dirPath,filename);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG,100,fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}