package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.adapters.ContactsAdapter;
import com.example.flowbookmessenger.models.Canvas;
import com.example.flowbookmessenger.models.Contacts;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddCanvasParticipantActivity extends AppCompatActivity {

    MaterialToolbar add_canvas_participant_activity_toolbar;
    RecyclerView add_canvas_participant_recycler_view;
    FloatingActionButton add_canvas_participant_activity_add_btn;
    ContactsAdapter contactsAdapter;
    List<Contacts> canvasParticipants;
    String canvasID;
    String newParticipantName;
    DatabaseReference canvasRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_canvas_participant);

        Toast.makeText(this, "Please make sure the user being added is a part of this group!!!", Toast.LENGTH_LONG).show();

        add_canvas_participant_activity_toolbar = findViewById(R.id.add_canvas_participant_activity_toolbar);
        add_canvas_participant_recycler_view = findViewById(R.id.add_canvas_participant_recycler_view);
        add_canvas_participant_activity_add_btn = findViewById(R.id.add_canvas_participant_activity_add_btn);

        setSupportActionBar(add_canvas_participant_activity_toolbar);
        getSupportActionBar().setTitle("Add Participant");

        canvasID = getIntent().getStringExtra("canvasID");

        add_canvas_participant_recycler_view.setHasFixedSize(true);
        add_canvas_participant_recycler_view.setLayoutManager(new LinearLayoutManager(this));

        canvasParticipants = new ArrayList<>();

        add_canvas_participant_activity_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(contactsAdapter.getSelectedContactsID().size() < 1){
                    Toast.makeText(AddCanvasParticipantActivity.this, "Please select atleast 1 contact", Toast.LENGTH_SHORT).show();
                }else {
                    final ArrayList<String> newParticipantID = contactsAdapter.getSelectedContactsID();
                    canvasRef = FirebaseDatabase.getInstance().getReference().child("Canvas").child(canvasID);
                    canvasRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Canvas canvas = dataSnapshot.getValue(Canvas.class);
                            final ArrayList<String> existingID = canvas.getCanvasParticipantsID();
                            final ArrayList<String> existingName = canvas.getCanvasParticipantsName();
                            for (String newID : newParticipantID){
                                if(existingID.contains(newID)){

                                }else {
                                    existingID.add(newID);
                                    DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(newID).child("name");
                                    userRef.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            newParticipantName = dataSnapshot.getValue(String.class);
                                            existingName.add(newParticipantName);
                                            HashMap<String,Object> canvas_hashmap = new HashMap<>();
                                            canvas_hashmap.put("canvasParticipantsID",existingID);
                                            canvas_hashmap.put("canvasParticipantsName",existingName);
                                            canvasRef.updateChildren(canvas_hashmap);
                                            Toast.makeText(AddCanvasParticipantActivity.this, newParticipantName + " Added", Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    Intent intent = new Intent(AddCanvasParticipantActivity.this,WelcomeActivity.class);
                    startActivity(intent);
                }
            }
        });
        availableUsers();
    }

    private void availableUsers() {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                canvasParticipants.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    Contacts contacts = d.getValue(Contacts.class);
                    assert contacts != null;
                    assert user != null;
                    if(!user.getUid().equals(contacts.getUserid())) {
                        canvasParticipants.add(contacts);
                    }
                }
                contactsAdapter = new ContactsAdapter(AddCanvasParticipantActivity.this,canvasParticipants,false);
                add_canvas_participant_recycler_view.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
