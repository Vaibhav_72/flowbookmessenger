package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.flowbookmessenger.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    Button signin_email_btn,signin_phone_btn,signin_signin_btn,signin_send_otp_btn,signin_verify_otp_btn;
    EditText signin_email,signin_password,signin_mobile,signin_otp;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        MaterialToolbar signin_toolbar = (MaterialToolbar) findViewById(R.id.signin_toolbar);
        setSupportActionBar(signin_toolbar);
        getSupportActionBar().setTitle("Login");

        signin_email = (EditText) findViewById(R.id.signin_email);
        signin_password = (EditText) findViewById(R.id.signin_password);
        signin_mobile = (EditText) findViewById(R.id.signin_mobile);
        signin_otp = (EditText) findViewById(R.id.signin_otp);

        signin_email_btn = (Button) findViewById(R.id.signin_email_btn);
        signin_phone_btn = (Button) findViewById(R.id.signin_phone_btn);
        signin_signin_btn = (Button) findViewById(R.id.signin_signin_btn);
        signin_send_otp_btn = (Button) findViewById(R.id.signin_send_otp_btn);
        signin_verify_otp_btn = (Button) findViewById(R.id.signin_verify_otp_btn);

        signin_email.setVisibility(View.GONE);
        signin_password.setVisibility(View.GONE);
        signin_mobile.setVisibility(View.GONE);
        signin_otp.setVisibility(View.GONE);
        signin_signin_btn.setVisibility(View.GONE);
        signin_send_otp_btn.setVisibility(View.GONE);
        signin_verify_otp_btn.setVisibility(View.GONE);

        auth = FirebaseAuth.getInstance();

        signin_email_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signin_email.setVisibility(View.VISIBLE);
                signin_password.setVisibility(View.VISIBLE);
                signin_signin_btn.setVisibility(View.VISIBLE);
                signin_mobile.setVisibility(View.GONE);
                signin_otp.setVisibility(View.GONE);
                signin_send_otp_btn.setVisibility(View.GONE);
                signin_verify_otp_btn.setVisibility(View.GONE);
                signin_signin_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String signin_email_txt = signin_email.getText().toString().trim();
                        String signin_password_txt = signin_password.getText().toString().trim();
                        if(TextUtils.isEmpty(signin_email_txt) || TextUtils.isEmpty(signin_password_txt)){
                            Toast.makeText(LoginActivity.this, "All Fields are Mandatory!!!", Toast.LENGTH_SHORT).show();
                        }else {
                            auth.signInWithEmailAndPassword(signin_email_txt,signin_password_txt).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful()){
                                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }else {
                                        Toast.makeText(LoginActivity.this, "Authentication failed!!! Please Verify your details and Try again", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });

 /*       signin_phone_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signin_mobile.setVisibility(View.VISIBLE);
                signin_send_otp_btn.setVisibility(View.VISIBLE);
                signin_email.setVisibility(View.GONE);
                signin_password.setVisibility(View.GONE);
                signin_signin_btn.setVisibility(View.GONE);

                signin_send_otp_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mobile_txt = "+91" + signin_mobile.getText().toString().trim();
                        signin_mobile.setVisibility(View.GONE);
                        signin_send_otp_btn.setVisibility(View.GONE);
                        if(mobile_txt.length() != 13){
                            Toast.makeText(LoginActivity.this, "Please enter a valid mobile number", Toast.LENGTH_SHORT).show();
                        }else {
                            signin_otp.setVisibility(View.VISIBLE);
                            signin_verify_otp_btn.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }); */
    }
}
