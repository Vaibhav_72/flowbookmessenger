package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.adapters.ContactsAdapter;
import com.example.flowbookmessenger.models.Contacts;
import com.example.flowbookmessenger.models.Groups;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddParticipantActivity extends AppCompatActivity {

    MaterialToolbar add_participant_activity_toolbar;
    RecyclerView add_participant_recycler_view;
    FloatingActionButton add_participant_activity_add_btn;
    ContactsAdapter contactsAdapter;
    List<Contacts> participantsList;
    String groupID;
    String newParticipantName;
    DatabaseReference groupRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_participant);

        add_participant_activity_toolbar = findViewById(R.id.add_participant_activity_toolbar);
        add_participant_recycler_view = findViewById(R.id.add_participant_recycler_view);
        add_participant_activity_add_btn = findViewById(R.id.add_participant_activity_add_btn);

        setSupportActionBar(add_participant_activity_toolbar);
        getSupportActionBar().setTitle("Add Participant");

        groupID = getIntent().getStringExtra("groupID");


        add_participant_recycler_view.setHasFixedSize(true);
        add_participant_recycler_view.setLayoutManager(new LinearLayoutManager(this));

        participantsList = new ArrayList<>();

        add_participant_activity_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(contactsAdapter.getSelectedContactsID().size() < 1){
                    Toast.makeText(AddParticipantActivity.this, "Please select atleast 1 contact", Toast.LENGTH_SHORT).show();
                }else {
                    final ArrayList<String> newParticipantID = contactsAdapter.getSelectedContactsID();
                    //ArrayList<String> newParticipantsName = contactsAdapter.getSelectedContactsName();
                    groupRef = FirebaseDatabase.getInstance().getReference().child("Groups").child(groupID);
                    groupRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Groups g = dataSnapshot.getValue(Groups.class);
                            final ArrayList<String> existingID = g.getGroupParticipantsID();
                            final ArrayList<String> existingName = g.getGroupParticipantsName();
                            for (String newID : newParticipantID){
                                if(existingID.contains(newID)){

                                }else {
                                    existingID.add(newID);
                                    DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(newID).child("name");
                                    userRef.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            newParticipantName = dataSnapshot.getValue(String.class);
                                            existingName.add(newParticipantName);
                                            HashMap<String,Object> group_hashmap = new HashMap<>();
                                            group_hashmap.put("groupParticipantsID",existingID);
                                            group_hashmap.put("groupParticipantsName",existingName);
                                            group_hashmap.put("groupParticipantsCount",existingID.size());
                                            groupRef.updateChildren(group_hashmap);
                                            Toast.makeText(AddParticipantActivity.this, newParticipantName + " Added", Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    Intent intent = new Intent(AddParticipantActivity.this,WelcomeActivity.class);
                    startActivity(intent);
                }
            }
        });
        availableUsers();
    }

    private void availableUsers() {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                participantsList.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    Contacts contacts = d.getValue(Contacts.class);
                    assert contacts != null;
                    assert user != null;
                    if(!user.getUid().equals(contacts.getUserid())) {
                        participantsList.add(contacts);
                    }
                }
                contactsAdapter = new ContactsAdapter(AddParticipantActivity.this,participantsList,false);
                add_participant_recycler_view.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
