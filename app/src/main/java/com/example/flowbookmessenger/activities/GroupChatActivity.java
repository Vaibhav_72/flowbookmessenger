package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.flowbookmessenger.fragments.APIService;
import com.example.flowbookmessenger.models.Contacts;
import com.example.flowbookmessenger.models.GroupChats;
import com.example.flowbookmessenger.adapters.GroupChatsAdapter;
import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.models.Groups;
import com.example.flowbookmessenger.notifications.ChatClient;
import com.example.flowbookmessenger.notifications.ChatData;
import com.example.flowbookmessenger.notifications.ChatResponse;
import com.example.flowbookmessenger.notifications.ChatSender;
import com.example.flowbookmessenger.notifications.ChatToken;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupChatActivity extends AppCompatActivity {

    Intent intent;
    Toolbar group_chat_widget_tool_bar;
    TextView group_chat_tool_bar_group_name,group_chat_tool_bar_group_details;
    EditText group_chat_message_et;
    ImageButton group_chat_send_btn,group_chat_attach_btn,group_attachment_document_btn, group_attachment_video_btn,group_attachment_gallery_btn,group_attachment_audio_btn;
    LinearLayout group_chat_attach_options;
    Button group_chat_blank_btn;
    ImageView group_chat_tool_bar_back;
    CircleImageView group_chat_tool_bar_group_icon;
    RecyclerView group_chat_message_recycler;
    String group_message_type,finalURL;
    String grpName,grpDescription,grpID,grpAdminID,grpAdminName;
    int grpParticipantsCount;
    ArrayList<String> grpParticipantsID,grpParticipantsName;
    FirebaseUser user;
    String currentUserName;
    DatabaseReference userRefrence;
    DatabaseReference reference;
    GroupChatsAdapter groupChatsAdapter;
    List<GroupChats> groupChatsList;

    static final int GROUP_DOCUMENT_CODE = 1;
    static final int GROUP_GALLERY_CODE = 2;
    static final int GROUP_VIDEO_CODE = 3;
    static final int GROUP_AUDIO_CODE = 4;

    private Uri groupGalleryURI,groupVideoURI,groupDocumentURI,groupAudioUri;
    private StorageTask uploadTask;

    String grpMsgTime,grpMsgDate,canvasDate,canvasTime;
    Calendar calendar;
    SimpleDateFormat date;
    SimpleDateFormat time;
    SimpleDateFormat dateTime;
    String grpMsgDateTime;

    //String grpMsgTime;
    ValueEventListener grpMessageStatusListener;

    ArrayList<String> receiverID,receiverName;
    ArrayList<String> readBy;

    APIService apiService = null;
    boolean notify = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        apiService = ChatClient.getClient("https://fcm.googleapis.com/").create(APIService.class);

        intent = getIntent();
        grpID = intent.getStringExtra("groupID");

        /*grpName = intent.getStringExtra("groupName");
        grpDescription = intent.getStringExtra("groupDescription");
        grpAdminID = intent.getStringExtra("groupAdminID");
        grpAdminName = intent.getStringExtra("groupAdminName");
        grpParticipantsCount = intent.getIntExtra("groupParticipantsCount",3);
        //grpParticipantsCount = intent.getStringExtra("groupParticipantsCount");
        grpParticipantsID = intent.getStringArrayListExtra("groupParticipantsID");
        grpParticipantsName = intent.getStringArrayListExtra("groupParticipantsName");*/
        //String grpKey = intent.getStringExtra("groupkey");


        group_chat_widget_tool_bar = (Toolbar) findViewById(R.id.group_chat_widget_tool_bar);
        group_chat_tool_bar_group_name = (TextView) findViewById(R.id.group_chat_tool_bar_group_name);
        group_chat_tool_bar_group_details = (TextView) findViewById(R.id.group_chat_tool_bar_group_details);
        group_chat_message_et = (EditText) findViewById(R.id.group_chat_message_et);
        group_chat_send_btn = (ImageButton) findViewById(R.id.group_chat_send_btn);
        group_chat_attach_btn = (ImageButton) findViewById(R.id.group_chat_attach_btn);
        group_attachment_document_btn = (ImageButton) findViewById(R.id.group_attachment_document_btn);
        group_attachment_video_btn = (ImageButton) findViewById(R.id.group_attachment_video_btn);
        group_attachment_gallery_btn = (ImageButton) findViewById(R.id.group_attachment_gallery_btn);
        group_attachment_audio_btn = (ImageButton) findViewById(R.id.group_attachment_audio_btn);
        group_chat_attach_options = (LinearLayout) findViewById(R.id.group_chat_attach_options);
        group_chat_tool_bar_back = (ImageView) findViewById(R.id.group_chat_tool_bar_back);
        group_chat_message_recycler = (RecyclerView) findViewById(R.id.group_chat_message_recycler);
        group_chat_tool_bar_group_icon = (CircleImageView) findViewById(R.id.group_chat_tool_bar_group_icon);
        group_chat_blank_btn = (Button) findViewById(R.id.group_chat_blank_btn);

        setSupportActionBar(group_chat_widget_tool_bar);
        getSupportActionBar().setTitle("");
        //getSupportActionBar().setSubtitle(grpDescription);
        group_chat_widget_tool_bar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailIntent = new Intent(GroupChatActivity.this, GroupChatDetailActivity.class);
                detailIntent.putExtra("groupID",grpID);
                detailIntent.putExtra("groupName",grpName);
                detailIntent.putStringArrayListExtra("groupParticipantsID",grpParticipantsID);
                detailIntent.putStringArrayListExtra("groupParticipantsName",grpParticipantsName);
                detailIntent.putExtra("groupAdminID",grpAdminID);
                //detailIntent.putExtra("participantCount",grpParticipantsCount);
                startActivity(detailIntent);
            }
        });

        //group_chat_tool_bar_group_name.setText(grpName);
        //group_chat_tool_bar_group_details.setText(grpDescription);
        //group_chat_tool_bar_group_icon.setImageResource(R.drawable.flowbook_logo);

        user = FirebaseAuth.getInstance().getCurrentUser();
        userRefrence = FirebaseDatabase.getInstance().getReference().child("Users").child(user.getUid()).child("name");
        userRefrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                currentUserName = dataSnapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //Toast.makeText(this, grpID, Toast.LENGTH_SHORT).show();

        //Toast.makeText(this,user.getDisplayName(), Toast.LENGTH_SHORT).show();


        readBy = new ArrayList<String>();

        group_chat_tool_bar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(GroupChatActivity.this, HomeActivity.class);
                startActivity(backIntent);
                finish();
            }
        });

        group_chat_message_recycler.setNestedScrollingEnabled(false);
        group_chat_message_recycler.setHasFixedSize(false);
        LinearLayoutManager groupMessageLinearLayoutManager = new LinearLayoutManager(getApplicationContext());
        groupMessageLinearLayoutManager.setStackFromEnd(true);
        group_chat_message_recycler.setLayoutManager(groupMessageLinearLayoutManager);

        group_chat_attach_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                group_chat_blank_btn.setVisibility(View.VISIBLE);
                if(group_chat_attach_options.getVisibility() == View.VISIBLE){
                    group_chat_attach_options.setVisibility(View.GONE);
                }else {
                    group_chat_attach_options.setVisibility(View.VISIBLE);
                }
            }
        });

        group_chat_blank_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                group_chat_attach_options.setVisibility(View.GONE);
                group_chat_blank_btn.setVisibility(View.GONE);
            }
        });

        group_chat_message_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String groupMessage = group_chat_message_et.getText().toString().trim();
                if(!groupMessage.equals("")){
                    group_chat_send_btn.setVisibility(View.VISIBLE);
                    group_chat_attach_btn.setVisibility(View.GONE);
                    group_chat_send_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            group_message_type = "text";
                            receiverID = new ArrayList<String>();
                            receiverID.addAll(grpParticipantsID);
                            receiverID.remove(user.getUid());
                            notify = true;
                            send_grp_message(user.getUid(),grpID,grpName,groupMessage,group_message_type,receiverID);
                            group_chat_message_et.setText("");
                        }
                    });
                }else {
                    group_chat_send_btn.setVisibility(View.GONE);
                    group_chat_attach_btn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*group_chat_send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String groupMessage = group_chat_message_et.getText().toString().trim();
                if(!groupMessage.equals("")){
                    group_message_type = "text";
                    receiverID = new ArrayList<String>();
                    receiverID.addAll(grpParticipantsID);
                    receiverID.remove(user.getUid());
                    send_grp_message(user.getUid(),grpID,grpName,groupMessage,group_message_type,receiverID);
                }else {
                    Toast.makeText(GroupChatActivity.this, "Please enter a message", Toast.LENGTH_SHORT).show();
                }
                group_chat_message_et.setText("");
            }
        });*/

        reference = FirebaseDatabase.getInstance().getReference().child("Groups").child(grpID);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Groups groups = dataSnapshot.getValue(Groups.class);
                assert groups != null;
                group_chat_tool_bar_group_name.setText(groups.getGroupName());
                group_chat_tool_bar_group_icon.setImageResource(R.drawable.flowbook_logo);
                group_chat_tool_bar_group_details.setText(groups.getGroupDescription());
                grpName = groups.getGroupName();
                grpDescription = groups.getGroupDescription();
                grpAdminID = groups.getGroupAdminID();
                grpAdminName = groups.getGroupAdminName();
                grpParticipantsCount = groups.getGroupParticipantsCount();
                grpParticipantsID = groups.getGroupParticipantsID();
                grpParticipantsName = groups.getGroupParticipantsName();
                read_grp_messages(user.getUid(),grpID);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        groupMessageStatus(grpID);

        group_attachment_document_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                group_chat_attach_options.setVisibility(View.GONE);
                group_message_type = "document";
                notify = true;
                Intent groupDocumentIntent = new Intent();
                groupDocumentIntent.setAction(Intent.ACTION_GET_CONTENT);
                groupDocumentIntent.setType("application/*");
                startActivityForResult(Intent.createChooser(groupDocumentIntent,"Select Document"),GROUP_DOCUMENT_CODE);
            }
        });



        group_attachment_gallery_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                group_chat_attach_options.setVisibility(View.GONE);
                group_message_type = "gallery";
                notify = true;
                Intent groupGalleryIntent = new Intent();
                groupGalleryIntent.setType("image/*");
                //galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
                groupGalleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(groupGalleryIntent,"Select Picture"),GROUP_GALLERY_CODE);
            }
        });

        group_attachment_audio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                group_chat_attach_options.setVisibility(View.GONE);
                group_message_type = "audio";
                notify = true;
                Intent groupAudioIntent = new Intent();
                groupAudioIntent.setType("audio/*");
                groupAudioIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(groupAudioIntent,"Select Audio"),GROUP_AUDIO_CODE);
            }
        });

        group_attachment_video_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                group_chat_attach_options.setVisibility(View.GONE);
                group_message_type = "video";
                notify = true;
                Intent videoIntent = new Intent();
                videoIntent.setType("video/*");
                videoIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(videoIntent,"Select Video"),GROUP_VIDEO_CODE);
            }
        });
    }

    private void groupMessageStatus(final String groupID){
        reference = FirebaseDatabase.getInstance().getReference("Group Chats");
        grpMessageStatusListener = reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot s : dataSnapshot.getChildren()){
                    GroupChats gc = s.getValue(GroupChats.class);
                    if(gc.getReceivers_ID().contains(user.getUid()) && gc.getGroup_ID().equals(groupID)){
                        if(gc.getReadBy().contains(user.getUid())){

                        }else {
                            readBy = gc.getReadBy();
                            if(readBy.contains(gc.getSender())) {
                                readBy.remove(gc.getSender());
                            }
                            readBy.add(user.getUid());
                            //gc.getReadBy().add(user.getUid());
                            HashMap<String,Object> group_message_hashmap = new HashMap<>();
                            group_message_hashmap.put("readBy",readBy);
                            s.getRef().updateChildren(group_message_hashmap);
                        }
                    }
                    if(gc.getReadBy().size() == gc.getReceivers_ID().size()){
                        HashMap<String,Object> group_message_hashmap = new HashMap<>();
                        group_message_hashmap.put("group_message_status","seen");
                        s.getRef().updateChildren(group_message_hashmap);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void send_grp_message(String sender, String grp_ID, String grp_name, final String grp_message, String grp_msg_type, final ArrayList<String> receiversID) {

        //grpMsgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());

        calendar = Calendar.getInstance();
        date = new SimpleDateFormat("dd-MMM-yyyy");
        time = new SimpleDateFormat("hh:mm a");
        dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        grpMsgDate = date.format(calendar.getTime());
        grpMsgTime = time.format(calendar.getTime());
        grpMsgDateTime = dateTime.format(calendar.getTime());

        reference = FirebaseDatabase.getInstance().getReference().child("Group Chats");
        DatabaseReference newGroupChatMessageRef = reference.push();

        readBy = new ArrayList<String>();
        readBy.add(user.getUid());

        HashMap<String,Object> group_message_hashmap = new HashMap<>();

        group_message_hashmap.put("sender",sender);
        group_message_hashmap.put("group_ID",grp_ID);
        group_message_hashmap.put("receivers_ID",receiversID);
        group_message_hashmap.put("group_name",grp_name);
        group_message_hashmap.put("group_message",grp_message);
        group_message_hashmap.put("m_type",grp_msg_type);
        group_message_hashmap.put("group_message_Date",grpMsgDate);
        group_message_hashmap.put("group_message_Time",grpMsgTime);
        group_message_hashmap.put("group_message_DateTime",grpMsgDateTime);
        group_message_hashmap.put("group_message_status","sent");
        group_message_hashmap.put("group_message_key",newGroupChatMessageRef.getKey());
        group_message_hashmap.put("readBy",readBy);

        newGroupChatMessageRef.setValue(group_message_hashmap);

        //reference.child("Group Chats").push().setValue(group_message_hashmap);


        reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Contacts contacts = dataSnapshot.getValue(Contacts.class);
                if(notify) {
                    for (String r : receiversID){
                        sendNotification(r, contacts.getName(), grp_message);
                    }
                }
                notify = false;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendNotification(final String receiver, final String name, final String msg) {
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference().child("Tokens");
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    ChatToken chatToken = snapshot.getValue(ChatToken.class);
                    ChatData chatData = new ChatData(user.getUid(),R.drawable.dubby_challenge,name + ": " + msg,grpName,receiver,"GroupChat",grpID);
                    ChatSender chatSender = new ChatSender(chatData, chatToken.getToken());

                    apiService.sendNotification(chatSender).enqueue(new Callback<ChatResponse>() {
                        @Override
                        public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                            if(response.code() == 200){
                                if(response.body().success != 1){
                                    Toast.makeText(GroupChatActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ChatResponse> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GROUP_GALLERY_CODE && resultCode == RESULT_OK && data != null && data.getData() != null){
            groupGalleryURI = data.getData();
            if (group_message_type.equals("gallery")){
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Group Gallery Messages").child("FlowGroup " + groupGalleryURI.getLastPathSegment());

                uploadTask = storageReference.putFile(groupGalleryURI);

                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {
                        if(!task.isSuccessful()){
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){

                            //grpMsgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());
                            calendar = Calendar.getInstance();
                            date = new SimpleDateFormat("dd-MMM-yyyy");
                            time = new SimpleDateFormat("hh:mm a");
                            dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                            grpMsgDate = date.format(calendar.getTime());
                            grpMsgTime = time.format(calendar.getTime());
                            grpMsgDateTime = dateTime.format(calendar.getTime());

                            receiverID = new ArrayList<String>();
                            receiverID.addAll(grpParticipantsID);
                            receiverID.remove(user.getUid());

                            Uri downloadURL = (Uri) task.getResult();
                            finalURL = downloadURL.toString();

                            reference = FirebaseDatabase.getInstance().getReference().child("Group Chats");
                            DatabaseReference newGroupChatMessageRef = reference.push();

                            readBy = new ArrayList<String>();
                            readBy.add(user.getUid());

                            HashMap<String,Object> group_message_hashmap = new HashMap<>();

                            group_message_hashmap.put("sender",user.getUid());
                            group_message_hashmap.put("group_ID",grpID);
                            group_message_hashmap.put("receivers_ID",receiverID);
                            group_message_hashmap.put("group_name",grpName);
                            group_message_hashmap.put("group_message",finalURL);
                            group_message_hashmap.put("name","FlowGroup " + groupGalleryURI.getLastPathSegment());
                            group_message_hashmap.put("m_type",group_message_type);
                            group_message_hashmap.put("group_message_Date",grpMsgDate);
                            group_message_hashmap.put("group_message_Time",grpMsgTime);
                            group_message_hashmap.put("group_message_DateTime",grpMsgDateTime);
                            group_message_hashmap.put("group_message_status","sent");
                            group_message_hashmap.put("group_message_key",newGroupChatMessageRef.getKey());
                            group_message_hashmap.put("readBy",readBy);

                            newGroupChatMessageRef.setValue(group_message_hashmap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            for (String r : receiverID){
                                                                sendNotification(r, contacts.getName(), "Photo");
                                                            }
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });

                            //reference.child("Group Chats").push().setValue(group_message_hashmap);
                        }
                    }
                });
            }
        }else if(requestCode == GROUP_DOCUMENT_CODE && resultCode == RESULT_OK && data != null && data.getData() != null){
            groupDocumentURI = data.getData();
            if(group_message_type.equals("document")){
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Group Document Messages").child("FlowGroup "+ groupDocumentURI.getLastPathSegment());

                uploadTask = storageReference.putFile(groupDocumentURI);

                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {
                        if(!task.isSuccessful()){
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){

                            //grpMsgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());
                            calendar = Calendar.getInstance();
                            date = new SimpleDateFormat("dd-MMM-yyyy");
                            time = new SimpleDateFormat("hh:mm a");
                            dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                            grpMsgDate = date.format(calendar.getTime());
                            grpMsgTime = time.format(calendar.getTime());
                            grpMsgDateTime = dateTime.format(calendar.getTime());

                            receiverID = new ArrayList<String>();
                            receiverID.addAll(grpParticipantsID);
                            receiverID.remove(user.getUid());

                            Uri downloadURL = (Uri) task.getResult();
                            finalURL = downloadURL.toString();

                            reference = FirebaseDatabase.getInstance().getReference().child("Group Chats");
                            DatabaseReference newGroupChatMessageRef = reference.push();

                            readBy = new ArrayList<String>();
                            readBy.add(user.getUid());

                            HashMap<String,Object> group_message_hashmap = new HashMap<>();

                            group_message_hashmap.put("sender",user.getUid());
                            group_message_hashmap.put("group_ID",grpID);
                            group_message_hashmap.put("receivers_ID",receiverID);
                            group_message_hashmap.put("group_name",grpName);
                            group_message_hashmap.put("group_message",finalURL);
                            group_message_hashmap.put("name","FlowGroup " + groupDocumentURI.getLastPathSegment());
                            group_message_hashmap.put("m_type",group_message_type);
                            group_message_hashmap.put("group_message_Date",grpMsgDate);
                            group_message_hashmap.put("group_message_Time",grpMsgTime);
                            group_message_hashmap.put("group_message_DateTime",grpMsgDateTime);
                            group_message_hashmap.put("group_message_status","sent");
                            group_message_hashmap.put("group_message_key",newGroupChatMessageRef.getKey());
                            group_message_hashmap.put("readBy",readBy);

                            newGroupChatMessageRef.setValue(group_message_hashmap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            for (String r : receiverID){
                                                                sendNotification(r, contacts.getName(), "Document");
                                                            }
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });

                            //reference.child("Group Chats").push().setValue(group_message_hashmap);
                        }
                    }
                });
            }
        }else if(requestCode == GROUP_AUDIO_CODE && resultCode == RESULT_OK && data != null && data.getData() != null){
            groupAudioUri = data.getData();
            if(group_message_type.equals("audio")){
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Group Audio Messages").child("FlowGroup " + groupAudioUri.getLastPathSegment());

                uploadTask = storageReference.putFile(groupAudioUri);
                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {
                        if(!task.isSuccessful()){
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){

                            //grpMsgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());
                            calendar = Calendar.getInstance();
                            date = new SimpleDateFormat("dd-MMM-yyyy");
                            time = new SimpleDateFormat("hh:mm a");
                            dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                            grpMsgDate = date.format(calendar.getTime());
                            grpMsgTime = time.format(calendar.getTime());
                            grpMsgDateTime = dateTime.format(calendar.getTime());

                            receiverID = new ArrayList<String>();
                            receiverID.addAll(grpParticipantsID);
                            receiverID.remove(user.getUid());

                            Uri downloadURL = (Uri) task.getResult();
                            finalURL = downloadURL.toString();

                            reference = FirebaseDatabase.getInstance().getReference().child("Group Chats");
                            DatabaseReference newGroupChatMessageRef = reference.push();

                            readBy = new ArrayList<String>();
                            readBy.add(user.getUid());

                            HashMap<String,Object> group_message_hashmap = new HashMap<>();

                            group_message_hashmap.put("sender",user.getUid());
                            group_message_hashmap.put("group_ID",grpID);
                            group_message_hashmap.put("receivers_ID",receiverID);
                            group_message_hashmap.put("group_name",grpName);
                            group_message_hashmap.put("group_message",finalURL);
                            group_message_hashmap.put("name","FlowGroup " + groupAudioUri.getLastPathSegment());
                            group_message_hashmap.put("m_type",group_message_type);
                            group_message_hashmap.put("group_message_Date",grpMsgDate);
                            group_message_hashmap.put("group_message_Time",grpMsgTime);
                            group_message_hashmap.put("group_message_DateTime",grpMsgDateTime);
                            group_message_hashmap.put("group_message_status","sent");
                            group_message_hashmap.put("group_message_key",newGroupChatMessageRef.getKey());
                            group_message_hashmap.put("readBy",readBy);

                            newGroupChatMessageRef.setValue(group_message_hashmap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            for (String r : receiverID){
                                                                sendNotification(r, contacts.getName(), "Audio");
                                                            }
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });

                            //reference.child("Group Chats").push().setValue(group_message_hashmap);
                        }
                    }
                });
            }
        }else if(requestCode == GROUP_VIDEO_CODE && resultCode == RESULT_OK && data != null && data.getData() != null){
            groupVideoURI = data.getData();
            if(group_message_type.equals("video")){
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Group Video Messages").child("FlowGroup " + groupVideoURI.getLastPathSegment());

                uploadTask = storageReference.putFile(groupVideoURI);
                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {
                        if(!task.isSuccessful()){
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){

                            //grpMsgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());
                            calendar = Calendar.getInstance();
                            date = new SimpleDateFormat("dd-MMM-yyyy");
                            time = new SimpleDateFormat("hh:mm a");
                            dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                            grpMsgDate = date.format(calendar.getTime());
                            grpMsgTime = time.format(calendar.getTime());
                            grpMsgDateTime = dateTime.format(calendar.getTime());

                            receiverID = new ArrayList<String>();
                            receiverID.addAll(grpParticipantsID);
                            receiverID.remove(user.getUid());

                            Uri downloadURL = (Uri) task.getResult();
                            finalURL = downloadURL.toString();

                            reference = FirebaseDatabase.getInstance().getReference().child("Group Chats");
                            DatabaseReference newGroupChatMessageRef = reference.push();

                            readBy = new ArrayList<String>();
                            readBy.add(user.getUid());

                            HashMap<String,Object> group_message_hashmap = new HashMap<>();

                            group_message_hashmap.put("sender",user.getUid());
                            group_message_hashmap.put("group_ID",grpID);
                            group_message_hashmap.put("receivers_ID",receiverID);
                            group_message_hashmap.put("group_name",grpName);
                            group_message_hashmap.put("group_message",finalURL);
                            group_message_hashmap.put("name","FlowGroup " + groupVideoURI.getLastPathSegment());
                            group_message_hashmap.put("m_type",group_message_type);
                            group_message_hashmap.put("group_message_Date",grpMsgDate);
                            group_message_hashmap.put("group_message_Time",grpMsgTime);
                            group_message_hashmap.put("group_message_DateTime",grpMsgDateTime);
                            group_message_hashmap.put("group_message_status","sent");
                            group_message_hashmap.put("group_message_key",newGroupChatMessageRef.getKey());
                            group_message_hashmap.put("readBy",readBy);

                            newGroupChatMessageRef.setValue(group_message_hashmap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            for (String r : receiverID){
                                                                sendNotification(r, contacts.getName(), "Video");
                                                            }
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });

                            //reference.child("Group Chats").push().setValue(group_message_hashmap);
                        }
                    }
                });
            }
        }
    }

    /*private void grpMessageStatus(final String groupID){
        reference = FirebaseDatabase.getInstance().getReference("Group Chats");
        grpMessageStatusListener = reference.addValueEventListener(new ValueEventListener() {
            @Overridex
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot s : dataSnapshot.getChildren()){
                    GroupChats gc = s.getValue(GroupChats.class);
                    if(!(gc.getSender().equals(user.getUid())) && gc.getGroup_ID().equals(groupID)){
                        HashMap<String,Object> group_message_hashmap = new HashMap<>();
                        group_message_hashmap.put("group_message_status","seen");
                        s.getRef().updateChildren(group_message_hashmap);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }*/

    /*private void read_grp_message(String currentUserID,String groupID){
        groupChatsList = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference("Group Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                groupChatsList.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    GroupChats groupChat = d.getValue(GroupChats.class);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        })
    }*/

    private void read_grp_messages(String currentUserID, final String groupID){

        groupChatsList = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference().child("Group Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                groupChatsList.clear();
                for (DataSnapshot s : dataSnapshot.getChildren()){
                    GroupChats groupChats = s.getValue(GroupChats.class);
                    if(groupChats.getGroup_ID().equals(groupID)){
                        groupChatsList.add(groupChats);
                    }
                    groupChatsAdapter = new GroupChatsAdapter(GroupChatActivity.this,groupChatsList);
                    group_chat_message_recycler.setAdapter(groupChatsAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.group_chat_activity_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.group_chat_activity_canvas){

            Intent intent = new Intent(GroupChatActivity.this, CanvasDisplayActivity.class);
            intent.putExtra("canvasParticipantsID",grpParticipantsID);
            intent.putExtra("canvasParticipantsName",grpParticipantsName);
            intent.putExtra("canvasParentGroupID",grpID);
            startActivity(intent);

            /*DatabaseReference canvasRef = FirebaseDatabase.getInstance().getReference().child("Canvas");
            final DatabaseReference newCanvasRef = canvasRef.push();
            Map<String,Object> newCanvasValues = new HashMap<>();
            newCanvasValues.put("canvasTime", ServerValue.TIMESTAMP);
            android.graphics.Point size = new android.graphics.Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            newCanvasValues.put("width",size.x);
            newCanvasValues.put("height",size.y);
            newCanvasRef.setValue(newCanvasValues, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    if(databaseError != null){
                        throw databaseError.toException();
                    }else {
                        Intent intent = new Intent(GroupChatActivity.this,CanvasDrawActivity.class);
                        intent.putExtra("canvasID",newCanvasRef.getKey());
                        startActivity(intent);
                    }
                }
            });*/

            /*calendar = Calendar.getInstance();
            date = new SimpleDateFormat("dd-MMM-yyyy");
            time = new SimpleDateFormat("hh:mm a");
            canvasDate = date.format(calendar.getTime());
            canvasTime = time.format(calendar.getTime());

            reference = FirebaseDatabase.getInstance().getReference();

            HashMap<String,Object> canvas_hashmap = new HashMap<>();

            canvas_hashmap.put("canvasName",grpName);
            canvas_hashmap.put("canvasID",grpID);
            canvas_hashmap.put("canvasDescription",grpDescription);
            canvas_hashmap.put("canvasAdminID",grpAdminID);
            canvas_hashmap.put("canvasAdminName",grpAdminName);
            canvas_hashmap.put("canvasParticipantsID",grpParticipantsID);
            canvas_hashmap.put("canvasParticipantsName",grpParticipantsName);
            canvas_hashmap.put("count",grpParticipantsCount);
            canvas_hashmap.put("canvasDate",canvasDate);
            canvas_hashmap.put("canvasTime",canvasTime);

            reference.child("Canvas").push().setValue(canvas_hashmap);

            Intent canvasIntent = new Intent(GroupChatActivity.this,CanvasDrawActivity.class);
            //canvasIntent.putExtra("groupName",grpName);
            //canvasIntent.putExtra("groupDescription",grpDescription);
            //canvasIntent.putExtra("groupID",grpID);
            //canvasIntent.putStringArrayListExtra("groupParticipantsID",grpParticipantsID);
            //canvasIntent.putStringArrayListExtra("groupParticipantsName",grpParticipantsName);
            startActivity(canvasIntent);*/
        }/*else if(item.getItemId() == R.id.group_chat_activity_exit){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Exit");
            builder.setMessage("Are you sure you want to exit the group?");
            builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    grpParticipantsID.remove(user.getUid());
                    grpParticipantsName.remove(currentUserName);
                    DatabaseReference groupRefrence = FirebaseDatabase.getInstance().getReference().child("Groups");
                    HashMap<String,Object> group_hashmap = new HashMap<>();
                    group_hashmap.put("groupParticipantsID",grpParticipantsID);
                    group_hashmap.put("groupParticipantsName",grpParticipantsName);
                    group_hashmap.put("groupParticipantsCount",grpParticipantsID.size());
                    groupRefrence.child(grpID).updateChildren(group_hashmap);
                    Intent intent = new Intent(GroupChatActivity.this,HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        reference.removeEventListener(grpMessageStatusListener);
    }
}
