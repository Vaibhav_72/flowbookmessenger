package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.adapters.ContactsAdapter;
import com.example.flowbookmessenger.models.Contacts;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForwardActivity extends AppCompatActivity {

    MaterialToolbar forward_activity_toolbar;
    RecyclerView forward_activity_recycler_view;
    FloatingActionButton forward_activity_send;
    ContactsAdapter contactsAdapter;
    List<Contacts> contacts;
    String message,message_type,name;
    Calendar calendar;
    SimpleDateFormat date;
    SimpleDateFormat time;
    SimpleDateFormat dateTime;
    String msgDate,msgTime,msgDateTime;
    DatabaseReference reference;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forward);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        message_type = getIntent().getStringExtra("message_type");
        message = getIntent().getStringExtra("message");
        name = getIntent().getStringExtra("name");

        user = FirebaseAuth.getInstance().getCurrentUser();

        forward_activity_toolbar = (MaterialToolbar) findViewById(R.id.forward_activity_toolbar);
        setSupportActionBar(forward_activity_toolbar);
        getSupportActionBar().setTitle("Forward To...");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        forward_activity_recycler_view = (RecyclerView) findViewById(R.id.forward_activity_recycler_view);
        forward_activity_recycler_view.setHasFixedSize(true);
        forward_activity_recycler_view.setLayoutManager(new LinearLayoutManager(this));

        forward_activity_send = (FloatingActionButton) findViewById(R.id.forward_activity_send);
        forward_activity_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contactsAdapter.getSelectedContactsID().size() < 1){
                    Toast.makeText(ForwardActivity.this, "No Contacts selected", Toast.LENGTH_SHORT).show();
                }else {
                    if(message_type.equals("text")){
                        for (String forwardReceiverID : contactsAdapter.getSelectedContactsID()){
                            forwardMessage(user.getUid(),forwardReceiverID);
                        }
                    }else {
                        for (String forwardReceiverID : contactsAdapter.getSelectedContactsID()){
                            forwardMediaMessage(user.getUid(),forwardReceiverID);
                        }
                    }
                    Toast.makeText(ForwardActivity.this, "Message sent!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ForwardActivity.this,HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        contacts = new ArrayList<>();
        availableUsers();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.forward_activity_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.forward_menu_search){
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) item.getActionView();
            if(null != searchView){
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                searchView.setIconifiedByDefault(false);
            }
            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    beginSearch(newText.toLowerCase());
                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        return true;
    }

    private void forwardMediaMessage(String sender, final String receiver) {
        calendar = Calendar.getInstance();
        date = new SimpleDateFormat("dd-MMM-yyyy");
        time = new SimpleDateFormat("hh:mm a");
        dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        msgDate = date.format(calendar.getTime());
        msgTime = time.format(calendar.getTime());
        msgDateTime = dateTime.format(calendar.getTime());

        String senderRefrence = "Chats/" +  sender + "/" + receiver;
        String receiverRefrence = "Chats/" + receiver + "/" + sender;

        DatabaseReference msgReference = FirebaseDatabase.getInstance().getReference().child("Chats").child(sender).child(receiver);
        DatabaseReference newChatMessageRef = msgReference.push();

        HashMap<String,Object> message_hashmap = new HashMap<>();

        message_hashmap.put("sender",sender);
        message_hashmap.put("receiver",receiver);
        message_hashmap.put("message",message);
        message_hashmap.put("name",name);
        message_hashmap.put("m_type",message_type);
        message_hashmap.put("message_Date",msgDate);
        message_hashmap.put("message_Time", msgTime);
        message_hashmap.put("message_DateTime",msgDateTime);
        message_hashmap.put("message_status","sent");
        message_hashmap.put("message_key",newChatMessageRef.getKey());

        Map messageStorageMap = new HashMap();
        messageStorageMap.put(senderRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);
        messageStorageMap.put(receiverRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);

        //newChatMessageRef.setValue(message_hashmap);

        FirebaseDatabase.getInstance().getReference().updateChildren(messageStorageMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

            }
        });

        final DatabaseReference chatref = FirebaseDatabase.getInstance().getReference("Chatslist")
                .child(user.getUid()).child(receiver);

        HashMap<String,Object> chatsListMapSender = new HashMap<>();
        chatsListMapSender.put("id",receiver);
        chatsListMapSender.put("lastMsgDateTime",msgDateTime);
        chatref.updateChildren(chatsListMapSender);

        /*chatref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    chatref.child("id").setValue(receiver);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

        final DatabaseReference chatrefReceiver = FirebaseDatabase.getInstance().getReference("Chatslist")
                .child(receiver).child(user.getUid());

        HashMap<String,Object> chatsListMapReceiver = new HashMap<>();
        chatsListMapReceiver.put("id",user.getUid());
        chatsListMapReceiver.put("lastMsgDateTime",msgDateTime);
        chatrefReceiver.updateChildren(chatsListMapReceiver);

        /*chatrefReceiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    chatrefReceiver.child("id").setValue(user.getUid());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/
    }

    private void forwardMessage(String sender, final String receiver) {
        calendar = Calendar.getInstance();
        date = new SimpleDateFormat("dd-MMM-yyyy");
        time = new SimpleDateFormat("hh:mm a");
        dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        msgDate = date.format(calendar.getTime());
        msgTime = time.format(calendar.getTime());
        msgDateTime = dateTime.format(calendar.getTime());

        String senderRefrence = "Chats/" +  sender + "/" + receiver;
        String receiverRefrence = "Chats/" + receiver + "/" + sender;

        DatabaseReference msgReference = FirebaseDatabase.getInstance().getReference().child("Chats").child(sender).child(receiver);
        DatabaseReference newChatMessageRef = msgReference.push();

        HashMap<String,Object> message_hashmap = new HashMap<>();

        message_hashmap.put("sender",sender);
        message_hashmap.put("receiver",receiver);
        message_hashmap.put("message",message);
        message_hashmap.put("m_type",message_type);
        message_hashmap.put("message_Date",msgDate);
        message_hashmap.put("message_Time", msgTime);
        message_hashmap.put("message_DateTime",msgDateTime);
        message_hashmap.put("message_status","sent");
        message_hashmap.put("message_key",newChatMessageRef.getKey());

        Map messageStorageMap = new HashMap();
        messageStorageMap.put(senderRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);
        messageStorageMap.put(receiverRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);

        //newChatMessageRef.setValue(message_hashmap);

        FirebaseDatabase.getInstance().getReference().updateChildren(messageStorageMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

            }
        });

        final DatabaseReference chatref = FirebaseDatabase.getInstance().getReference("Chatslist")
                .child(user.getUid()).child(receiver);

        HashMap<String,Object> chatsListMapSender = new HashMap<>();
        chatsListMapSender.put("id",receiver);
        chatsListMapSender.put("lastMsgDateTime",msgDateTime);
        chatref.updateChildren(chatsListMapSender);

        /*chatref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    chatref.child("id").setValue(receiver);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

        final DatabaseReference chatrefReceiver = FirebaseDatabase.getInstance().getReference("Chatslist")
                .child(receiver).child(user.getUid());

        HashMap<String,Object> chatsListMapReceiver = new HashMap<>();
        chatsListMapReceiver.put("id",user.getUid());
        chatsListMapReceiver.put("lastMsgDateTime",msgDateTime);
        chatrefReceiver.updateChildren(chatsListMapReceiver);

        /*chatrefReceiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    chatrefReceiver.child("id").setValue(user.getUid());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/
    }

    private void availableUsers() {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contacts.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    Contacts contacts = d.getValue(Contacts.class);
                    assert contacts != null;
                    assert user != null;
                    if(!user.getUid().equals(contacts.getUserid())) {
                        ForwardActivity.this.contacts.add(contacts);
                    }
                }
                contactsAdapter = new ContactsAdapter(ForwardActivity.this, contacts,false);
                forward_activity_recycler_view.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void beginSearch(String s){
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Query query = FirebaseDatabase.getInstance().getReference("Users").orderByChild("search")
                .startAt(s)
                .endAt(s + "\uf8ff");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contacts.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Contacts c = snapshot.getValue(Contacts.class);
                    assert c != null;
                    assert firebaseUser != null;
                    if(!c.getUserid().equals(firebaseUser.getUid())){
                        contacts.add(c);
                    }
                }
                contactsAdapter = new ContactsAdapter(getBaseContext(),contacts,false);
                forward_activity_recycler_view.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
