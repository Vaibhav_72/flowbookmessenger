package com.example.flowbookmessenger.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.example.flowbookmessenger.R;

public class FullScreenVideoActivity extends AppCompatActivity {


    public VideoView video_message_full_screen;
    //public ImageButton video_exit_full_screen_btn;
    public MediaController videoMediaController;
    Intent intent;
    View decorView;
    int uiOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_video);

        /*Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }*/

        decorView = getWindow().getDecorView();
       //uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        video_message_full_screen = (VideoView) findViewById(R.id.video_message_full_screen);
        //video_exit_full_screen_btn = (ImageButton) findViewById(R.id.video_exit_full_screen_btn);

        /*DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) video_message_full_screen.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        video_message_full_screen.setLayoutParams(params);*/

        intent = getIntent();
        String videoUrl = intent.getStringExtra("url");

        videoMediaController = new MediaController(this);
        video_message_full_screen.setMediaController(videoMediaController);
        videoMediaController.setAnchorView(video_message_full_screen);
        video_message_full_screen.setVideoURI(Uri.parse(videoUrl));
        video_message_full_screen.start();
        video_message_full_screen.seekTo(1);

        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);



        /*video_exit_full_screen_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) video_message_full_screen.getLayoutParams();
                params.width = (int)(300*metrics.density);
                params.height = (int)(250*metrics.density);
                params.leftMargin = 30;
                video_message_full_screen.setLayoutParams(params);
            }
        });*/
    }
}
