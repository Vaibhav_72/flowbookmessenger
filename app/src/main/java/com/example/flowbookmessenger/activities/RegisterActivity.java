package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.flowbookmessenger.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class RegisterActivity extends AppCompatActivity {

    EditText register_name,register_email, register_password, register_mobile, register_otp;
    Button register_email_btn, register_phone_btn, register_register_btn, register_send_otp_btn, register_verify_otp_btn;
    FirebaseAuth auth;
    DatabaseReference reference;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    String verification;
    PhoneAuthProvider.ForceResendingToken token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        MaterialToolbar register_toolbar = (MaterialToolbar) findViewById(R.id.register_toolbar);
        setSupportActionBar(register_toolbar);
        getSupportActionBar().setTitle("Register");

        register_name = (EditText) findViewById(R.id.register_name);
        register_email = (EditText) findViewById(R.id.register_email);
        register_mobile = (EditText) findViewById(R.id.register_mobile);
        register_password = (EditText) findViewById(R.id.register_password);
        register_otp = (EditText) findViewById(R.id.register_otp);
        register_email_btn = (Button) findViewById(R.id.register_email_btn);
        register_phone_btn = (Button) findViewById(R.id.register_phone_btn);
        register_send_otp_btn = (Button) findViewById(R.id.register_send_otp_btn);
        register_verify_otp_btn = (Button) findViewById(R.id.register_verify_otp_btn);
        register_register_btn = (Button) findViewById(R.id.register_register_btn);

        register_name.setVisibility(View.GONE);
        register_email.setVisibility(View.GONE);
        register_password.setVisibility(View.GONE);
        register_mobile.setVisibility(View.GONE);
        register_otp.setVisibility(View.GONE);
        register_send_otp_btn.setVisibility(View.GONE);
        register_verify_otp_btn.setVisibility(View.GONE);
        register_register_btn.setVisibility(View.GONE);

        auth = FirebaseAuth.getInstance();

        register_email_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register_name.setVisibility(View.VISIBLE);
                register_email.setVisibility(View.VISIBLE);
                register_password.setVisibility(View.VISIBLE);
                register_register_btn.setVisibility(View.VISIBLE);
                register_mobile.setVisibility(View.GONE);
                register_otp.setVisibility(View.GONE);
                register_send_otp_btn.setVisibility(View.GONE);
                register_verify_otp_btn.setVisibility(View.GONE);
                register_register_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String name_txt = register_name.getText().toString().trim();
                        String email_txt = register_email.getText().toString().trim();
                        String password_txt = register_password.getText().toString().trim();
                        if (TextUtils.isEmpty(email_txt) || TextUtils.isEmpty(password_txt) || TextUtils.isEmpty(name_txt)) {
                            Toast.makeText(RegisterActivity.this, "All fields are mandatory!!!", Toast.LENGTH_SHORT).show();
                        } else if (password_txt.length() < 6) {
                            Toast.makeText(RegisterActivity.this, "Password must be of atleast 6 characters", Toast.LENGTH_SHORT).show();
                        } else {
                            register(name_txt,email_txt,password_txt);
                        }
                    }
                });
            }
        });

        register_phone_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register_name.setVisibility(View.VISIBLE);
                register_mobile.setVisibility(View.VISIBLE);
                register_send_otp_btn.setVisibility(View.VISIBLE);
                register_email.setVisibility(View.GONE);
                register_password.setVisibility(View.GONE);
                register_register_btn.setVisibility(View.GONE);
                register_send_otp_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mobile_number_txt = "+91" + register_mobile.getText().toString().trim();
                        register_send_otp_btn.setVisibility(View.GONE);
                        register_mobile.setVisibility(View.GONE);
                        register_name.setVisibility(View.GONE);
                        if (mobile_number_txt.length() != 13){
                            Toast.makeText(RegisterActivity.this, "Please enter a Valid Mobile Number", Toast.LENGTH_SHORT).show();
                        } else {
                            register_otp.setVisibility(View.VISIBLE);
                            register_verify_otp_btn.setVisibility(View.VISIBLE);
                            PhoneAuthProvider.getInstance().verifyPhoneNumber(mobile_number_txt,120, TimeUnit.SECONDS,RegisterActivity.this,callbacks);
                        }
                    }
                });
            }
        });

        register_verify_otp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register_send_otp_btn.setVisibility(View.GONE);
                register_mobile.setVisibility(View.GONE);
                String otp_txt = register_otp.getText().toString().trim();
                if(TextUtils.isEmpty(otp_txt)){
                    Toast.makeText(RegisterActivity.this, "Please Enter OTP Received", Toast.LENGTH_SHORT).show();
                }else {
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verification, otp_txt);
                    signInWithPhoneAuthCredential(credential);
                    register_otp.setText("");
                }
            }
        });
        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Toast.makeText(RegisterActivity.this, "Authentication Failed" + e.getMessage(), Toast.LENGTH_LONG).show();
                register_send_otp_btn.setVisibility(View.VISIBLE);
                register_mobile.setVisibility(View.VISIBLE);
                register_verify_otp_btn.setVisibility(View.GONE);
                register_otp.setVisibility(View.GONE);
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                verification = s;
                token = forceResendingToken;
                Toast.makeText(RegisterActivity.this, "OTP Sent", Toast.LENGTH_SHORT).show();
                register_send_otp_btn.setVisibility(View.GONE);
                register_mobile.setVisibility(View.GONE);
                super.onCodeSent(s, forceResendingToken);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(RegisterActivity.this, "Verification Successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                            startActivity(intent);
                        } else {
                            String error = task.getException().toString();
                            Toast.makeText(RegisterActivity.this, "Error :" +error , Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void register(final String name, final String email, final String password) {
        auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser user = auth.getCurrentUser();
                    assert user != null;
                    String userid = user.getUid();
                    Toast.makeText(RegisterActivity.this, "Registration Successful!!!", Toast.LENGTH_SHORT).show();

                    reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);

                    HashMap<String,String> h = new HashMap<>();
                    h.put("userid",userid);
                    h.put("name",name);
                    h.put("email",email);
                    h.put("Password",password);
                    h.put("search",name.toLowerCase());

                    reference.setValue(h).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Intent intent = new Intent(RegisterActivity.this,HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }else {
                    Toast.makeText(RegisterActivity.this, "Invalid Format", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}


