package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.flowbookmessenger.models.Chats;
import com.example.flowbookmessenger.adapters.ChatsAdapter;
import com.example.flowbookmessenger.models.Contacts;
import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.fragments.APIService;
import com.example.flowbookmessenger.notifications.ChatClient;
import com.example.flowbookmessenger.notifications.ChatData;
import com.example.flowbookmessenger.notifications.ChatResponse;
import com.example.flowbookmessenger.notifications.ChatSender;
import com.example.flowbookmessenger.notifications.ChatToken;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {

    CircleImageView profile_image;
    TextView user_name,user_details;
    EditText chat_message_et;
    ImageButton chat_send_btn,chat_attach_btn,attachment_document_btn, attachment_video_btn,attachment_gallery_btn,attachment_audio_btn;
    //ImageButton attachment_location_btn,attachment_contact_btn,chat_camera_btn;
    Toolbar chat_widget_tool_bar;
    LinearLayout chat_attach_options;
    Button chat_blank_btn;
    ImageView chat_tool_bar_back;
    Intent intent;
    FirebaseUser user;
    DatabaseReference reference;
    DatabaseReference msgReference;
    ChatsAdapter chatsAdapter;
    List<Chats> chatsList = new ArrayList<>();
    List<String> chatKey = new ArrayList<>();
    RecyclerView chat_message_recycler;
    String  id;  //Receiver
    private String message_type,finalURL;
    private Uri galleryURI,videoURI,documentURI,audioUri,cameraURI;
    private StorageTask uploadTask;

    static final int DOCUMENT_CODE = 1;
    static final int GALLERY_CODE = 2;
    static final int VIDEO_CODE = 3;
    static final int AUDIO_CODE = 4;
    static final int CAMERA_CODE = 5;

    String msgTime,msgDate;
    Calendar calendar;
    SimpleDateFormat date;
    SimpleDateFormat time;
    SimpleDateFormat dateTime;
    String msgDateTime;

    String cameraPhotoPath;

    ValueEventListener messageStatusListener;

    ProgressDialog progressDialog;

    APIService apiService = null;

    boolean notify = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        apiService = ChatClient.getClient("https://fcm.googleapis.com/").create(APIService.class);

        profile_image = (CircleImageView) findViewById(R.id.chat_tool_bar_profile_icon);
        user_name = (TextView) findViewById(R.id.chat_tool_bar_name);
        user_details = (TextView) findViewById(R.id.chat_tool_bar_user_details);
        chat_message_et = (EditText) findViewById(R.id.chat_message_et);
        chat_send_btn = (ImageButton) findViewById(R.id.chat_send_btn);
        chat_attach_btn = (ImageButton) findViewById(R.id.chat_attach_btn);
        //chat_camera_btn = (ImageButton) findViewById(R.id.chat_camera_btn);
        chat_tool_bar_back = (ImageView) findViewById(R.id.chat_tool_bar_back);
        chat_widget_tool_bar = (Toolbar) findViewById(R.id.chat_widget_tool_bar);

        chat_attach_options = (LinearLayout) findViewById(R.id.chat_attach_options);
        chat_blank_btn = (Button) findViewById(R.id.chat_blank_btn);

        attachment_document_btn = (ImageButton) findViewById(R.id.attachment_document_btn);
        attachment_video_btn = (ImageButton) findViewById(R.id.attachment_video_btn);
        attachment_gallery_btn = (ImageButton) findViewById(R.id.attachment_gallery_btn);
        attachment_audio_btn = (ImageButton) findViewById(R.id.attachment_audio_btn);
        //attachment_location_btn = (ImageButton) findViewById(R.id.attachment_location_btn);
        //attachment_contact_btn = (ImageButton) findViewById(R.id.attachment_contact_btn);

        setSupportActionBar(chat_widget_tool_bar);
        getSupportActionBar().setTitle("");

        chatsAdapter = new ChatsAdapter(ChatActivity.this,chatsList);

        chat_message_recycler = findViewById(R.id.chat_message_recycler);
        chat_message_recycler.setNestedScrollingEnabled(false);
        chat_message_recycler.setHasFixedSize(false);
        LinearLayoutManager messageLinearLayoutManager = new LinearLayoutManager(getApplicationContext());
        messageLinearLayoutManager.setStackFromEnd(true);
        chat_message_recycler.setLayoutManager(messageLinearLayoutManager);
        chat_message_recycler.setAdapter(chatsAdapter);

        intent = getIntent();
        id = intent.getStringExtra("userid");

        user = FirebaseAuth.getInstance().getCurrentUser();

        getMessages();

        //progressDialog = new ProgressDialog(this);

        chat_attach_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_blank_btn.setVisibility(View.VISIBLE);
                if(chat_attach_options.getVisibility() == View.VISIBLE){
                    chat_attach_options.setVisibility(View.GONE);
                }else {
                    chat_attach_options.setVisibility(View.VISIBLE);
                }
            }
        });

        chat_blank_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_attach_options.setVisibility(View.GONE);
                chat_blank_btn.setVisibility(View.GONE);
            }
        });

        chat_message_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String message = chat_message_et.getText().toString().trim();
                if(!message.equals("")){
                    chat_send_btn.setVisibility(View.VISIBLE);
                    chat_attach_btn.setVisibility(View.GONE);
                    chat_send_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            message_type = "text";
                            notify = true;
                            send_message(user.getUid(),id,message,message_type);
                            chat_message_et.setText("");
                        }
                    });
                }else {
                    chat_send_btn.setVisibility(View.GONE);
                    chat_attach_btn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*chat_send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notify = true;
                String message = chat_message_et.getText().toString().trim();
                if(!message.equals("")){
                    message_type = "text";
                    send_message(user.getUid(),id,message,message_type);
                }else {
                    Toast.makeText(ChatActivity.this, "Please enter a message", Toast.LENGTH_SHORT).show();
                }
                chat_message_et.setText("");
            }
        });*/

        reference = FirebaseDatabase.getInstance().getReference("Users").child(id);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Contacts contact = dataSnapshot.getValue(Contacts.class);
                user_name.setText(contact.getName());
                //user_details.setText(contact.getUserid());
                profile_image.setImageResource(R.drawable.student_icon);

                //read_message(user.getUid(),id);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        messageStatus(id);

        chat_tool_bar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        attachment_document_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_attach_options.setVisibility(View.GONE);
                message_type = "document";
                notify = true;
                Intent documentIntent = new Intent();
                documentIntent.setAction(Intent.ACTION_GET_CONTENT);
                documentIntent.setType("application/*");
                startActivityForResult(Intent.createChooser(documentIntent,"Select Document"),DOCUMENT_CODE);
            }
        });



        attachment_gallery_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_attach_options.setVisibility(View.GONE);
                message_type = "gallery";
                notify = true;
                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                //galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(galleryIntent,"Select Picture"), GALLERY_CODE);
            }
        });

        attachment_audio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_attach_options.setVisibility(View.GONE);
                message_type = "audio";
                notify = true;
                Intent audioIntent = new Intent();
                audioIntent.setType("audio/*");
                audioIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(audioIntent,"Select Audio"),AUDIO_CODE);
            }
        });

        attachment_video_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_attach_options.setVisibility(View.GONE);
                message_type = "video";
                notify = true;
                Intent videoIntent = new Intent();
                videoIntent.setType("video/*");
                videoIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(videoIntent,"Select Video"),VIDEO_CODE);
            }
        });

        /*chat_camera_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message_type = "camera";
                notify = true;
                dispatchTakePictureIntent();
            }
        });*/


       /* attachment_location_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //chat_attach_options.setVisibility(View.GONE);
            }
        }); */

       /* attachment_contact_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_attach_options.setVisibility(View.GONE);
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent,1);
            }
        }); */

        lastSeen();
    }

    private void getMessages() {
        FirebaseDatabase.getInstance().getReference().child("Chats").child(user.getUid()).child(id).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Chats chats = dataSnapshot.getValue(Chats.class);
                chatsList.add(chats);
                String key = dataSnapshot.getKey();
                chatKey.add(key);
                chatsAdapter.notifyDataSetChanged();
                chat_message_recycler.scrollToPosition(chatsList.size() - 1);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Chats chats = dataSnapshot.getValue(Chats.class);
                String key = dataSnapshot.getKey();
                int index = chatKey.indexOf(key);
                chatsList.set(index,chats);
                chatsAdapter.notifyDataSetChanged();
                chat_message_recycler.scrollToPosition(chatsList.size() - 1);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent,CAMERA_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        cameraPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_CODE && resultCode == RESULT_OK && data != null && data.getData() != null){
            galleryURI = data.getData();
            if(message_type.equals("gallery")){
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Gallery Messages").child("Flowbook " + galleryURI.getLastPathSegment());

                uploadTask = storageReference.putFile(galleryURI);

                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {

                        if(!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if(task.isSuccessful()){
                            //msgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());

                            calendar = Calendar.getInstance();
                            date = new SimpleDateFormat("dd-MMM-yyyy");
                            time = new SimpleDateFormat("hh:mm a");
                            dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                            msgDate = date.format(calendar.getTime());
                            msgTime = time.format(calendar.getTime());
                            msgDateTime = dateTime.format(calendar.getTime());

                            String senderRefrence = "Chats/" +  user.getUid() + "/" + id;
                            String receiverRefrence = "Chats/" + id + "/" + user.getUid();

                            Uri downloadURL = task.getResult();
                            finalURL = downloadURL.toString();

                            msgReference = FirebaseDatabase.getInstance().getReference().child("Chats").child(user.getUid()).child(id);
                            DatabaseReference newChatMessageRef = msgReference.push();

                            HashMap<String,Object> message_hashmap = new HashMap<>();

                            message_hashmap.put("sender",user.getUid());
                            message_hashmap.put("receiver",id);
                            message_hashmap.put("message",finalURL);
                            message_hashmap.put("name","FlowBook " + galleryURI.getLastPathSegment());
                            message_hashmap.put("m_type",message_type);
                            message_hashmap.put("message_Date",msgDate);
                            message_hashmap.put("message_Time", msgTime);
                            message_hashmap.put("message_DateTime",msgDateTime);
                            message_hashmap.put("message_status","sent");
                            message_hashmap.put("message_key",newChatMessageRef.getKey());

                            Map messageStorageMap = new HashMap();
                            messageStorageMap.put(senderRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);
                            messageStorageMap.put(receiverRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);

                            FirebaseDatabase.getInstance().getReference().updateChildren(messageStorageMap)
                                    .addOnCompleteListener(new OnCompleteListener() {
                                        @Override
                                        public void onComplete(@NonNull Task task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            sendNotification(id, contacts.getName(), "Photo");
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });

                            /*newChatMessageRef.setValue(message_hashmap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            sendNotification(id, contacts.getName(), "Photo");
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });*/

                            //reference.child("Chats").push().setValue(message_hashmap);

                            final DatabaseReference chatref = FirebaseDatabase.getInstance().getReference("Chatslist")
                                    .child(user.getUid()).child(id);

                            HashMap<String,Object> chatsListMapSender = new HashMap<>();
                            chatsListMapSender.put("id",id);
                            chatsListMapSender.put("lastMsgDateTime",msgDateTime);
                            chatref.updateChildren(chatsListMapSender);

                            /*chatref.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists()){
                                        chatref.child("id").setValue(id);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });*/

                            final DatabaseReference chatrefReceiver = FirebaseDatabase.getInstance().getReference("Chatslist")
                                    .child(id).child(user.getUid());

                            HashMap<String,Object> chatsListMapReceiver = new HashMap<>();
                            chatsListMapReceiver.put("id",user.getUid());
                            chatsListMapReceiver.put("lastMsgDateTime",msgDateTime);
                            chatrefReceiver.updateChildren(chatsListMapReceiver);

                            /*chatrefReceiver.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists()){
                                        chatrefReceiver.child("id").setValue(user.getUid());
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });*/
                        }
                    }
                });
            }
        }else if(requestCode == DOCUMENT_CODE && resultCode == RESULT_OK && data != null && data.getData() != null){
            documentURI = data.getData();
            if(message_type.equals("document")){
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Document Messages").child("FlowBook " + documentURI.getLastPathSegment());

                uploadTask = storageReference.putFile(documentURI);

                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {

                        if(!task.isSuccessful()){
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if(task.isSuccessful()){
                            //msgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());

                            calendar = Calendar.getInstance();
                            date = new SimpleDateFormat("dd-MMM-yyyy");
                            time = new SimpleDateFormat("hh:mm a");
                            dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                            msgDate = date.format(calendar.getTime());
                            msgTime = time.format(calendar.getTime());
                            msgDateTime = dateTime.format(calendar.getTime());

                            String senderRefrence = "Chats/" +  user.getUid() + "/" + id;
                            String receiverRefrence = "Chats/" + id + "/" + user.getUid();

                            Uri downloadURL = task.getResult();
                            finalURL = downloadURL.toString();

                            msgReference = FirebaseDatabase.getInstance().getReference().child("Chats").child(user.getUid()).child(id);
                            DatabaseReference newChatMessageRef = msgReference.push();

                            HashMap<String,Object> message_hashmap = new HashMap<>();

                            message_hashmap.put("sender",user.getUid());
                            message_hashmap.put("receiver",id);
                            message_hashmap.put("message",finalURL);
                            message_hashmap.put("name","FlowBook " + documentURI.getLastPathSegment());
                            message_hashmap.put("m_type",message_type);
                            message_hashmap.put("message_Date",msgDate);
                            message_hashmap.put("message_Time", msgTime);
                            message_hashmap.put("message_DateTime",msgDateTime);
                            message_hashmap.put("message_status","sent");
                            message_hashmap.put("message_key",newChatMessageRef.getKey());

                            Map messageStorageMap = new HashMap();
                            messageStorageMap.put(senderRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);
                            messageStorageMap.put(receiverRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);

                            FirebaseDatabase.getInstance().getReference().updateChildren(messageStorageMap)
                                    .addOnCompleteListener(new OnCompleteListener() {
                                        @Override
                                        public void onComplete(@NonNull Task task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            sendNotification(id, contacts.getName(), "Document");
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });

                            /*newChatMessageRef.setValue(message_hashmap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            sendNotification(id, contacts.getName(), "Document");
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });*/

                            //reference.child("Chats").push().setValue(message_hashmap);

                            final DatabaseReference chatref = FirebaseDatabase.getInstance().getReference("Chatslist")
                                    .child(user.getUid()).child(id);

                            HashMap<String,Object> chatsListMapSender = new HashMap<>();
                            chatsListMapSender.put("id",id);
                            chatsListMapSender.put("lastMsgDateTime",msgDateTime);
                            chatref.updateChildren(chatsListMapSender);

                            /*chatref.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists()){
                                        chatref.child("id").setValue(id);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });*/

                            final DatabaseReference chatrefReceiver = FirebaseDatabase.getInstance().getReference("Chatslist")
                                    .child(id).child(user.getUid());

                            HashMap<String,Object> chatsListMapReceiver = new HashMap<>();
                            chatsListMapReceiver.put("id",user.getUid());
                            chatsListMapReceiver.put("lastMsgDateTime",msgDateTime);
                            chatrefReceiver.updateChildren(chatsListMapReceiver);

                            /*chatrefReceiver.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists()){
                                        chatrefReceiver.child("id").setValue(user.getUid());
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });*/
                        }
                    }
                });
            }
        }else if(requestCode == AUDIO_CODE && resultCode == RESULT_OK && data != null && data.getData() != null){
            audioUri = data.getData();

            if(message_type.equals("audio")){
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Audio Messages").child("FlowBook " + audioUri.getLastPathSegment());

                uploadTask = storageReference.putFile(audioUri);

                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {

                        if(!task.isSuccessful()){
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){
                            //msgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());

                            calendar = Calendar.getInstance();
                            date = new SimpleDateFormat("dd-MMM-yyyy");
                            time = new SimpleDateFormat("hh:mm a");
                            dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                            msgDate = date.format(calendar.getTime());
                            msgTime = time.format(calendar.getTime());
                            msgDateTime = dateTime.format(calendar.getTime());

                            String senderRefrence = "Chats/" +  user.getUid() + "/" + id;
                            String receiverRefrence = "Chats/" + id + "/" + user.getUid();

                            Uri downloadURL = (Uri) task.getResult();
                            finalURL = downloadURL.toString();

                            msgReference = FirebaseDatabase.getInstance().getReference().child("Chats").child(user.getUid()).child(id);
                            DatabaseReference newChatMessageRef = msgReference.push();

                            HashMap<String,Object> message_hashmap = new HashMap<>();

                            message_hashmap.put("sender",user.getUid());
                            message_hashmap.put("receiver",id);
                            message_hashmap.put("message",finalURL);
                            message_hashmap.put("name","FlowBook " + audioUri.getLastPathSegment());
                            message_hashmap.put("m_type",message_type);
                            message_hashmap.put("message_Date",msgDate);
                            message_hashmap.put("message_Time", msgTime);
                            message_hashmap.put("message_DateTime",msgDateTime);
                            message_hashmap.put("message_status","sent");
                            message_hashmap.put("message_key",newChatMessageRef.getKey());

                            Map messageStorageMap = new HashMap();
                            messageStorageMap.put(senderRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);
                            messageStorageMap.put(receiverRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);

                            FirebaseDatabase.getInstance().getReference().updateChildren(messageStorageMap)
                                    .addOnCompleteListener(new OnCompleteListener() {
                                        @Override
                                        public void onComplete(@NonNull Task task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            sendNotification(id, contacts.getName(), "Audio");
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });

                            /*newChatMessageRef.setValue(message_hashmap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            sendNotification(id, contacts.getName(), "Audio");
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });*/

                            //reference.child("Chats").push().setValue(message_hashmap);

                            final DatabaseReference chatref = FirebaseDatabase.getInstance().getReference("Chatslist")
                                    .child(user.getUid()).child(id);

                            HashMap<String,Object> chatsListMapSender = new HashMap<>();
                            chatsListMapSender.put("id",id);
                            chatsListMapSender.put("lastMsgDateTime",msgDateTime);
                            chatref.updateChildren(chatsListMapSender);

                            /*chatref.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists()){
                                        chatref.child("id").setValue(id);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });*/

                            final DatabaseReference chatrefReceiver = FirebaseDatabase.getInstance().getReference("Chatslist")
                                    .child(id).child(user.getUid());

                            HashMap<String,Object> chatsListMapReceiver = new HashMap<>();
                            chatsListMapReceiver.put("id",user.getUid());
                            chatsListMapReceiver.put("lastMsgDateTime",msgDateTime);
                            chatrefReceiver.updateChildren(chatsListMapReceiver);

                            /*chatrefReceiver.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists()){
                                        chatrefReceiver.child("id").setValue(user.getUid());
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });*/
                        }
                    }
                });
            }
        }else if(requestCode == VIDEO_CODE && resultCode == RESULT_OK && data != null && data.getData() != null){
            videoURI = data.getData();

            if(message_type.equals("video")){
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Video Messages").child("FlowBook " + videoURI.getLastPathSegment());

                uploadTask = storageReference.putFile(videoURI);

                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {
                        if(!task.isSuccessful()){
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){
                            //msgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());

                            calendar = Calendar.getInstance();
                            date = new SimpleDateFormat("dd-MMM-yyyy");
                            time = new SimpleDateFormat("hh:mm a");
                            dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                            msgDate = date.format(calendar.getTime());
                            msgTime = time.format(calendar.getTime());
                            msgDateTime = dateTime.format(calendar.getTime());

                            String senderRefrence = "Chats/" +  user.getUid() + "/" + id;
                            String receiverRefrence = "Chats/" + id + "/" + user.getUid();

                            Uri downloadURL = (Uri) task.getResult();
                            finalURL = downloadURL.toString();

                            msgReference = FirebaseDatabase.getInstance().getReference().child("Chats").child(user.getUid()).child(id);
                            DatabaseReference newChatMessageRef = msgReference.push();

                            HashMap<String,Object> message_hashmap = new HashMap<>();

                            message_hashmap.put("sender",user.getUid());
                            message_hashmap.put("receiver",id);
                            message_hashmap.put("message",finalURL);
                            message_hashmap.put("name","FlowBook " + videoURI.getLastPathSegment());
                            message_hashmap.put("m_type",message_type);
                            message_hashmap.put("message_Date",msgDate);
                            message_hashmap.put("message_Time", msgTime);
                            message_hashmap.put("message_DateTime",msgDateTime);
                            message_hashmap.put("message_status","sent");
                            message_hashmap.put("message_key",newChatMessageRef.getKey());

                            Map messageStorageMap = new HashMap();
                            messageStorageMap.put(senderRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);
                            messageStorageMap.put(receiverRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);

                            FirebaseDatabase.getInstance().getReference().updateChildren(messageStorageMap)
                                    .addOnCompleteListener(new OnCompleteListener() {
                                        @Override
                                        public void onComplete(@NonNull Task task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            sendNotification(id, contacts.getName(), "Video");
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });

                            /*newChatMessageRef.setValue(message_hashmap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                                reference.addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                        if(notify) {
                                                            sendNotification(id, contacts.getName(), "Video");
                                                        }
                                                        notify = false;
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        }
                                    });*/

                            //reference.child("Chats").push().setValue(message_hashmap);

                            final DatabaseReference chatref = FirebaseDatabase.getInstance().getReference("Chatslist")
                                    .child(user.getUid()).child(id);

                            HashMap<String,Object> chatsListMapSender = new HashMap<>();
                            chatsListMapSender.put("id",id);
                            chatsListMapSender.put("lastMsgDateTime",msgDateTime);
                            chatref.updateChildren(chatsListMapSender);

                            /*chatref.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists()){
                                        chatref.child("id").setValue(id);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });*/

                            final DatabaseReference chatrefReceiver = FirebaseDatabase.getInstance().getReference("Chatslist")
                                    .child(id).child(user.getUid());

                            HashMap<String,Object> chatsListMapReceiver = new HashMap<>();
                            chatsListMapReceiver.put("id",user.getUid());
                            chatsListMapReceiver.put("lastMsgDateTime",msgDateTime);
                            chatrefReceiver.updateChildren(chatsListMapReceiver);

                            /*chatrefReceiver.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists()){
                                        chatrefReceiver.child("id").setValue(user.getUid());
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });*/
                        }
                    }
                });
            }
        }else if(requestCode == CAMERA_CODE && resultCode == RESULT_OK){
            File f = new File(cameraPhotoPath);

            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

            uploadCameraImage(f.getName(),contentUri);
        }
    }

    private void uploadCameraImage(final String name, Uri contentUri) {
        if(message_type.equals("camera")){
            final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Camera Messages").child("FlowBook " + name);

            uploadTask = storageReference.putFile(contentUri);

            uploadTask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception {
                    if(!task.isSuccessful()){
                        throw task.getException();
                    }
                    return storageReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful()){
                        //msgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());

                        calendar = Calendar.getInstance();
                        date = new SimpleDateFormat("dd-MMM-yyyy");
                        time = new SimpleDateFormat("hh:mm a");
                        dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                        msgDate = date.format(calendar.getTime());
                        msgTime = time.format(calendar.getTime());
                        msgDateTime = dateTime.format(calendar.getTime());

                        String senderRefrence = "Chats/" +  user.getUid() + "/" + id;
                        String receiverRefrence = "Chats/" + id + "/" + user.getUid();

                        Uri downloadURL = (Uri) task.getResult();
                        finalURL = downloadURL.toString();

                        msgReference = FirebaseDatabase.getInstance().getReference().child("Chats").child(user.getUid()).child(id);
                        DatabaseReference newChatMessageRef = msgReference.push();

                        HashMap<String,Object> message_hashmap = new HashMap<>();

                        message_hashmap.put("sender",user.getUid());
                        message_hashmap.put("receiver",id);
                        message_hashmap.put("message",finalURL);
                        message_hashmap.put("name","FlowBook " + name);
                        message_hashmap.put("m_type",message_type);
                        message_hashmap.put("message_Date",msgDate);
                        message_hashmap.put("message_Time", msgTime);
                        message_hashmap.put("message_DateTime",msgDateTime);
                        message_hashmap.put("message_status","sent");
                        message_hashmap.put("message_key",newChatMessageRef.getKey());

                        Map messageStorageMap = new HashMap();
                        messageStorageMap.put(senderRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);
                        messageStorageMap.put(receiverRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);

                        FirebaseDatabase.getInstance().getReference().updateChildren(messageStorageMap)
                                .addOnCompleteListener(new OnCompleteListener() {
                                    @Override
                                    public void onComplete(@NonNull Task task) {
                                        if(task.isSuccessful()){
                                            reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                            reference.addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                    if(notify) {
                                                        sendNotification(id, contacts.getName(), "Photo");
                                                    }
                                                    notify = false;
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }
                                });

                        /*newChatMessageRef.setValue(message_hashmap)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            reference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
                                            reference.addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    Contacts contacts = dataSnapshot.getValue(Contacts.class);
                                                    if(notify) {
                                                        sendNotification(id, contacts.getName(), "Photo");
                                                    }
                                                    notify = false;
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }
                                });*/

                        //reference.child("Chats").push().setValue(message_hashmap);

                        final DatabaseReference chatref = FirebaseDatabase.getInstance().getReference("Chatslist")
                                .child(user.getUid()).child(id);

                        HashMap<String,Object> chatsListMapSender = new HashMap<>();
                        chatsListMapSender.put("id",id);
                        chatsListMapSender.put("lastMsgDateTime",msgDateTime);
                        chatref.updateChildren(chatsListMapSender);

                        /*chatref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(!dataSnapshot.exists()){
                                    chatref.child("id").setValue(id);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });*/

                        final DatabaseReference chatrefReceiver = FirebaseDatabase.getInstance().getReference("Chatslist")
                                .child(id).child(user.getUid());

                        HashMap<String,Object> chatsListMapReceiver = new HashMap<>();
                        chatsListMapReceiver.put("id",user.getUid());
                        chatsListMapReceiver.put("lastMsgDateTime",msgDateTime);
                        chatrefReceiver.updateChildren(chatsListMapReceiver);

                        /*chatrefReceiver.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(!dataSnapshot.exists()){
                                    chatrefReceiver.child("id").setValue(user.getUid());
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });*/
                    }
                }
            });
        }
    }


    private void messageStatus(final String receiverID){
        reference = FirebaseDatabase.getInstance().getReference("Chats").child(receiverID).child(user.getUid());
        messageStatusListener = reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot s : dataSnapshot.getChildren()){
                    Chats c = s.getValue(Chats.class);
                    if((c.getReceiver().equals(user.getUid())) && (c.getSender().equals(receiverID))){
                        HashMap<String,Object> message_hashmap = new HashMap<>();
                        message_hashmap.put("message_status","seen");
                        String ref = s.getKey();
                        s.getRef().updateChildren(message_hashmap);
                        DatabaseReference msg = FirebaseDatabase.getInstance().getReference().child("Chats").child(user.getUid()).child(receiverID).child(ref);
                        msg.updateChildren(message_hashmap);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void send_message(String sender, final String receiver, final String message, String msg_type){

        //msgTime = (Calendar.getInstance().getTime().getHours() + ":" + Calendar.getInstance().getTime().getMinutes());

        calendar = Calendar.getInstance();
        date = new SimpleDateFormat("dd-MMM-yyyy");
        time = new SimpleDateFormat("hh:mm a");
        dateTime = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        msgDate = date.format(calendar.getTime());
        msgTime = time.format(calendar.getTime());
        msgDateTime = dateTime.format(calendar.getTime());

        String senderRefrence = "Chats/" +  sender + "/" + receiver;
        String receiverRefrence = "Chats/" + receiver + "/" + sender;

        msgReference = FirebaseDatabase.getInstance().getReference().child("Chats").child(sender).child(receiver);
        DatabaseReference newChatMessageRef = msgReference.push();

        HashMap<String,Object> message_hashmap = new HashMap<>();

        message_hashmap.put("sender",sender);
        message_hashmap.put("receiver",receiver);
        message_hashmap.put("message",message);
        message_hashmap.put("m_type",msg_type);
        message_hashmap.put("message_Date",msgDate);
        message_hashmap.put("message_Time", msgTime);
        message_hashmap.put("message_DateTime",msgDateTime);
        message_hashmap.put("message_status","sent");
        message_hashmap.put("message_key",newChatMessageRef.getKey());

        Map messageStorageMap = new HashMap();
        messageStorageMap.put(senderRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);
        messageStorageMap.put(receiverRefrence + "/" + newChatMessageRef.getKey(),message_hashmap);

        FirebaseDatabase.getInstance().getReference().updateChildren(messageStorageMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

            }
        });

        //newChatMessageRef.setValue(message_hashmap);

        //reference.child("Chats").push().setValue(message_hashmap);

        final DatabaseReference chatref = FirebaseDatabase.getInstance().getReference("Chatslist")
                .child(user.getUid()).child(id);

        HashMap<String,Object> chatsListMapSender = new HashMap<>();
        chatsListMapSender.put("id",id);
        chatsListMapSender.put("lastMsgDateTime",msgDateTime);
        chatref.updateChildren(chatsListMapSender);

        /*chatref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    chatref.child("id").setValue(id);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

        final DatabaseReference chatrefReceiver = FirebaseDatabase.getInstance().getReference("Chatslist")
                .child(id).child(user.getUid());

        HashMap<String,Object> chatsListMapReceiver = new HashMap<>();
        chatsListMapReceiver.put("id",user.getUid());
        chatsListMapReceiver.put("lastMsgDateTime",msgDateTime);
        chatrefReceiver.updateChildren(chatsListMapReceiver);

        /*chatrefReceiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    chatrefReceiver.child("id").setValue(user.getUid());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

        DatabaseReference notifyReference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
        notifyReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Contacts contacts = dataSnapshot.getValue(Contacts.class);
                if(notify) {
                    sendNotification(receiver, contacts.getName(), message);
                }
                notify = false;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendNotification(String receiver, final String name, final String msg) {
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference().child("Tokens");
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    ChatToken chatToken = snapshot.getValue(ChatToken.class);
                    ChatData chatData = new ChatData(user.getUid(),R.drawable.dubby_challenge,name + ": " + msg,"New Message",id,"Chat");
                    ChatSender chatSender = new ChatSender(chatData, chatToken.getToken());

                    apiService.sendNotification(chatSender).enqueue(new Callback<ChatResponse>() {
                        @Override
                        public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                            if(response.code() == 200){
                                if(response.body().success != 1){
                                    Toast.makeText(ChatActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ChatResponse> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        //getMenuInflater().inflate(R.menu.chat_options_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);

        /*switch (item.getItemId()){
            case R.id.chat_menu_video :
                break;
            case R.id.chat_menu_voice:
                break;
        }*/
        return true;
    }

    /*private void read_message(final String id, final String userid){
        chatsList = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chatsList.clear();
                for (DataSnapshot s : dataSnapshot.getChildren()){
                    Chats chat = s.getValue(Chats.class);
                    if((userid.equals(chat.getReceiver()) && id.equals(chat.getSender())) ||
                            (id.equals(chat.getReceiver()) && userid.equals(chat.getSender()))) {
                        chatsList.add(chat);
                    }
                    chatsAdapter = new ChatsAdapter(ChatActivity.this,chatsList);
                    chat_message_recycler.setAdapter(chatsAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }*/

    private void lastSeen(){
        reference = FirebaseDatabase.getInstance().getReference();
        reference.child("Users").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child("UserStatus").hasChild("status")){
                    String status = dataSnapshot.child("UserStatus").child("status").getValue().toString();
                    String date = dataSnapshot.child("UserStatus").child("date").getValue().toString();
                    String time = dataSnapshot.child("UserStatus").child("time").getValue().toString();

                    if(status.equals("online")){
                        user_details.setText("online");
                    }else if(status.equals("offline")){
                        user_details.setText("Last Seen " + date + " at " + time);
                    }
                }else {
                    user_details.setText("offline");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void currentUser(String userid){
        SharedPreferences.Editor editor = getSharedPreferences("PREFS", MODE_PRIVATE).edit();
        editor.putString("currentuser", userid);
        editor.apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
        msgReference = FirebaseDatabase.getInstance().getReference().child("Chats").child(user.getUid()).child(id);
        msgReference.removeEventListener(messageStatusListener);
        currentUser("none");
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentUser(id);
    }
}