package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.example.flowbookmessenger.BuildConfig;
import com.example.flowbookmessenger.R;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

public class WelcomeActivity extends AppCompatActivity {

    Button welcome_register,welcome_sign_in;
    FirebaseUser user;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onStart() {
        super.onStart();

        user = FirebaseAuth.getInstance().getCurrentUser();

        if(user != null){
            Intent intent = new Intent(WelcomeActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        MaterialToolbar welcome_toolbar = (MaterialToolbar) findViewById(R.id.welcome_toolbar);
        setSupportActionBar(welcome_toolbar);
        getSupportActionBar().setTitle("FlowBook Messenger - Welcome");

        welcome_register = (Button) findViewById(R.id.welcome_register_btn);
        welcome_sign_in = (Button) findViewById(R.id.welcome_signin_btn);

        welcome_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        welcome_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        /*try {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                Log.w("instanceId", "getInstanceId failed", task.getException());
                                return;
                            }

                            try {
                                String token = task.getResult().getToken();
                                Log.d("token", token);
                                //Toast.makeText(WelcomeActivity.this, token, Toast.LENGTH_SHORT).show();
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

        } catch (Exception e) {

        }
        try {
            if(AppUtils.isNetworkAvailable(this)) {
                mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (intent != null && intent.getAction() != null) {
                            if (Constants.REGISTRATION_COMPLETE.equals(intent.getAction())) {
                                FirebaseMessaging.getInstance().subscribeToTopic(Constants.TOPIC_GLOBAL);
                                if (BuildConfig.DEBUG) {
                                    FirebaseMessaging.getInstance().subscribeToTopic(Constants.TOPIC_DEVELOPMENT);
                                } else {
                                    FirebaseMessaging.getInstance().subscribeToTopic(Constants.TOPIC_PRODUCTION);
                                }
                            } else if (Constants.PUSH_NOTIFICATION.equals(intent.getAction())) {
                                Bundle bundle = null;
                                if(intent.getExtras() != null) {
                                    bundle = intent.getExtras();
                                }
                                Intent i = new Intent(getApplicationContext(), HandleNotificationsActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                if (bundle != null) {
                                    i.putExtras(bundle);
                                }
                                startActivity(i);
                            }
                        }
                    }
                };
            }
        }catch (Exception ignored){

        }*/

    }

    /*@Override
    protected void onResume() {
        super.onResume();
        if(AppUtils.isNetworkAvailable(this)) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Constants.REGISTRATION_COMPLETE));
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Constants.PUSH_NOTIFICATION));
            NotificationUtils.clearNotifications(getApplicationContext());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if(mRegistrationBroadcastReceiver != null) {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
            }
        } catch (Exception ignored){
        }
    }*/
}
