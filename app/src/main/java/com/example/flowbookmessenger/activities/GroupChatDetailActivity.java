package com.example.flowbookmessenger.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.flowbookmessenger.adapters.GroupChatDetailAdapter;
import com.example.flowbookmessenger.R;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class GroupChatDetailActivity extends AppCompatActivity {

    AppBarLayout group_chat_detail_appbar;
    CollapsingToolbarLayout group_chat_detail_collapsing_toolbar;
    MaterialToolbar group_chat_detail_material_toolbar;
    RecyclerView group_chat_detail_recycler_view;
    //TextView group_chat_detail_count;
    ArrayList<String> grpParticipantsID,grpParticipantsName;
    String grpName,grpID,grpAdminID;
    //int participantsCount;
    GroupChatDetailAdapter groupChatDetailAdapter;
    TextView group_chat_detail_exit;
    TextView group_chat_detail_add_participant;
    FirebaseUser user;
    String currentUserName;
    DatabaseReference userRefrence;
    //CircleImageView group_chat_detail_material_toolbar_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat_detail);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        group_chat_detail_appbar = findViewById(R.id.group_chat_detail_appbar);
        group_chat_detail_collapsing_toolbar = findViewById(R.id.group_chat_detail_collapsing_toolbar);
        group_chat_detail_material_toolbar = findViewById(R.id.group_chat_detail_material_toolbar);
        group_chat_detail_recycler_view = findViewById(R.id.group_chat_detail_recycler_view);
        group_chat_detail_exit = findViewById(R.id.group_chat_detail_exit);
        group_chat_detail_add_participant = findViewById(R.id.group_chat_detail_add_participant);
        //group_chat_detail_count = findViewById(R.id.group_chat_detail_count);
        //group_chat_detail_material_toolbar_icon = findViewById(R.id.group_chat_detail_material_toolbar_icon);

        group_chat_detail_add_participant.setVisibility(View.GONE);

        grpName = getIntent().getStringExtra("groupName");
        grpID = getIntent().getStringExtra("groupID");
        //participantsCount = getIntent().getIntExtra("participantCount",3);
        grpParticipantsID = getIntent().getStringArrayListExtra("groupParticipantsID");
        grpParticipantsName = getIntent().getStringArrayListExtra("groupParticipantsName");
        grpAdminID = getIntent().getStringExtra("groupAdminID");


        user = FirebaseAuth.getInstance().getCurrentUser();
        userRefrence = FirebaseDatabase.getInstance().getReference().child("Users").child(user.getUid()).child("name");
        userRefrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                currentUserName = dataSnapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if(user.getUid().equals(grpAdminID)){
            group_chat_detail_add_participant.setVisibility(View.VISIBLE);
            group_chat_detail_add_participant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GroupChatDetailActivity.this,AddParticipantActivity.class);
                    intent.putExtra("groupID",grpID);
                    startActivity(intent);
                }
            });
        }


        group_chat_detail_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Exit");
                builder.setMessage("Are you sure you want to exit the group?");
                builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        grpParticipantsID.remove(user.getUid());
                        grpParticipantsName.remove(currentUserName);
                        DatabaseReference groupRefrence = FirebaseDatabase.getInstance().getReference().child("Groups");
                        HashMap<String,Object> group_hashmap = new HashMap<>();
                        group_hashmap.put("groupParticipantsID",grpParticipantsID);
                        group_hashmap.put("groupParticipantsName",grpParticipantsName);
                        group_hashmap.put("groupParticipantsCount",grpParticipantsID.size());
                        groupRefrence.child(grpID).updateChildren(group_hashmap);
                        Intent intent = new Intent(GroupChatDetailActivity.this,HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        group_chat_detail_recycler_view.setLayoutManager(new LinearLayoutManager(this));

        group_chat_detail_collapsing_toolbar.setTitle(grpName);
        group_chat_detail_material_toolbar.setTitle(grpName);
        //group_chat_detail_count.setText(participantsCount + " Participants");
        //group_chat_detail_material_toolbar_icon.setImageResource(R.drawable.flowbook_logo);

        groupChatDetailAdapter = new GroupChatDetailAdapter(this,grpParticipantsName);
        group_chat_detail_recycler_view.setAdapter(groupChatDetailAdapter);
    }
}
