package com.example.flowbookmessenger.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.flowbookmessenger.R;
import com.google.android.material.appbar.MaterialToolbar;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity {

    CircleImageView settings_profile_image;
    EditText settings_name,settings_class;
    Button settings_update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(R.drawable.app_toolbar_gradient);
        }

        MaterialToolbar settings_toolbar = (MaterialToolbar) findViewById(R.id.settings_toolbar);
        setSupportActionBar(settings_toolbar);
        getSupportActionBar().setTitle("Profile");

        settings_name = (EditText) findViewById(R.id.settings_profile_name);
        settings_class = (EditText) findViewById(R.id.settings_profile_class);
        settings_profile_image = (CircleImageView) findViewById(R.id.settings_profile_image);
        settings_update = (Button) findViewById(R.id.settings_update_btn);
    }
}
