package com.example.flowbookmessenger.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;

public class CanvasPathColorDialog extends Dialog {

    public interface ColorListener{
        void colorUpdate(int updatedColor);
    }

    private ColorListener colorListener;
    private int defaultColor;

    public CanvasPathColorDialog(@NonNull Context context, ColorListener colorListener, int defaultColor) {
        super(context);
        this.colorListener = colorListener;
        this.defaultColor = defaultColor;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ColorListener c = new ColorListener() {
            @Override
            public void colorUpdate(int updatedColor) {
                colorListener.colorUpdate(updatedColor);
                dismiss();
            }
        };

        setContentView(new ColorSelectorView(getContext(),c,defaultColor));
        setTitle("Select Color");
    }

    private class ColorSelectorView extends View {

        private Paint paint;
        private Paint centerPaint;
        private int colorOptions[];
        private ColorListener listener;

        public ColorSelectorView(Context context, ColorListener colorListener1, int defaultColor) {
            super(context);
            listener = colorListener1;
            colorOptions = new int[]{
                    0xFFFF0000, 0xFFFF00FF, 0xFF0000FF, 0xFF00FFFF, 0xFF00FF00,
                    0xFFFFFF00, 0xFFFF0000,0xFF01579B
            };

            Shader shader = new SweepGradient(0,0,colorOptions,null);

            paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setShader(shader);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(32);

            centerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            centerPaint.setColor(defaultColor);
            centerPaint.setStrokeWidth(5);
        }

        private boolean trackingCenter;
        private boolean highlightCenter;

        private static final int CENTER_X = 100;
        private static final int CENTER_Y = 100;
        private static final int CENTER_RADIUS = 32;
        private static final float PI = 3.1415927f;

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(CENTER_X * 2, CENTER_Y * 2);
        }

        @Override
        protected void onDraw(Canvas canvas) {

            float r = CENTER_X - paint.getStrokeWidth() * 0.5f;

            canvas.translate(CENTER_X,CENTER_X);
            canvas.drawOval(new RectF(-r,-r,r,r),paint);
            canvas.drawCircle(0,0,CENTER_RADIUS,centerPaint);

            if(trackingCenter){
                int c = centerPaint.getColor();
                centerPaint.setStyle(Paint.Style.STROKE);

                if(highlightCenter){
                    centerPaint.setAlpha(0xFF);
                }else {
                    centerPaint.setAlpha(0x80);
                }

                canvas.drawCircle(0,0,CENTER_RADIUS + centerPaint.getStrokeWidth(),centerPaint);

                centerPaint.setStyle(Paint.Style.FILL);
                centerPaint.setColor(c);
            }
        }

        private int floatToByte(float x){
            int n = Math.round(x);
            return n;
        }

        private int pinToByte(int n){
            if(n < 0){
                n = 0;
            }else if(n > 255){
                n = 255;
            }
            return n;
        }

        private int ave(int s,int d,float p){
            return s + Math.round(p * (d - s));
        }

        private int interColor(int colors[],float unit){
            if(unit <= 0){
                return colors[0];
            }else if(unit >= 1){
                return colors[colors.length - 1];
            }

            float p = unit * (colors.length - 1);
            int i = (int) p;
            p -= i;
            int c0 = colors[i];
            int c1= colors[i+1];
            int a = ave(Color.alpha(c0),Color.alpha(c1),p);
            int r = ave(Color.red(c0),Color.red(c1),p);
            int g = ave(Color.green(c0),Color.green(c1),p);
            int b = ave(Color.blue(c0),Color.blue(c1),p);

            return Color.argb(a,r,g,b);
        }

        private int rotateColor(int color,float radian){
            float degree = radian * 180 / PI;
            int r = Color.red(color);
            int g = Color.green(color);
            int b = Color.blue(color);

            ColorMatrix colorMatrix = new ColorMatrix();
            ColorMatrix transposeColorMatrix = new ColorMatrix();

            colorMatrix.setRGB2YUV();
            transposeColorMatrix.setRotate(0,degree);
            colorMatrix.postConcat(transposeColorMatrix);
            transposeColorMatrix.setYUV2RGB();
            colorMatrix.postConcat(transposeColorMatrix);

            final float[] a = colorMatrix.getArray();

            int ir = floatToByte(a[0] * r + a[1] * g + a[2] * b);
            int ig = floatToByte(a[5] * r + a[6] * g + a[7] * b);
            int ib = floatToByte(a[10] * r + a[11] * g + a[12] * b);

            return Color.argb(Color.alpha(color),pinToByte(ir),pinToByte(ig),pinToByte(ib));
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {

            float x = event.getX() - CENTER_X;
            float y = event.getY() - CENTER_Y;
            boolean inCenter = Math.sqrt(x * x + y * y) <= CENTER_RADIUS;

            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN :
                    trackingCenter = inCenter;
                    if(inCenter){
                        highlightCenter = true;
                        invalidate();
                        break;
                    }
                case MotionEvent.ACTION_MOVE :
                    if(trackingCenter){
                        if(highlightCenter != inCenter){
                            highlightCenter = inCenter;
                            invalidate();
                        }
                    }else {
                        float angle = (float) Math.atan2(y,x);
                        float unit = angle / (2 * PI);
                        if(unit < 0){
                            unit += 1;
                        }
                        centerPaint.setColor(interColor(colorOptions,unit));
                        invalidate();
                    }
                    break;
                case MotionEvent.ACTION_UP :
                    if(trackingCenter){
                        if(inCenter){
                            listener.colorUpdate(centerPaint.getColor());
                        }
                        trackingCenter = false;
                        invalidate();
                    }
                    break;
            }
            return true;
        }
    }
}
