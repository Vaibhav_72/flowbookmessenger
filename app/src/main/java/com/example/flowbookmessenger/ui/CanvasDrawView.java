package com.example.flowbookmessenger.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.flowbookmessenger.models.CanvasPoint;
import com.example.flowbookmessenger.models.CanvasSegment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CanvasDrawView extends View {
    public static final int PIXEL_SIZE = 8;

    private Paint paint;
    private int lastX;
    private int lastY;
    private Canvas buffer;
    private Bitmap bitmap;
    private Paint bitmapPaint;
    private DatabaseReference firebaseRef;
    private ChildEventListener childEventListener;
    private int currentColor = 0xFF01579B;
    private float strokeWidth = 10f;
    private Path path;
    private Set<String> outstandingSegments;
    private CanvasSegment currentSegment;
    private float scale = 1.0f;
    private int canvasWidth;
    private int canvasHeight;
    Context context;

    public CanvasDrawView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CanvasDrawView(Context context, DatabaseReference ref){
        this(context,ref,1.0f);
    }
    public CanvasDrawView(Context context,DatabaseReference ref,int width,int height){
        this(context,ref);
        this.setBackgroundColor(Color.DKGRAY);
        canvasWidth = width;
        canvasHeight = height;
    }

    public CanvasDrawView(Context context,DatabaseReference ref,float scale){
        super(context);

        outstandingSegments = new HashSet<String>();
        path = new Path();
        this.firebaseRef = ref;
        this.scale = scale;

        childEventListener = ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String name = dataSnapshot.getKey();
                if(!outstandingSegments.contains(name)){
                    CanvasSegment canvasSegment = dataSnapshot.getValue(CanvasSegment.class);
                    drawSegment(canvasSegment,paintFromColor(canvasSegment.getColor(),canvasSegment.getWidth()));
                    invalidate();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(0xFF01579B);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10f);

        bitmapPaint = new Paint(Paint.DITHER_FLAG);
    }

    public void cleanUp(){
        firebaseRef.removeEventListener(childEventListener);
    }

    public void setColor(int color){
        currentColor = color;
        paint.setColor(color);
    }

    public void setStrokeWidth(float width){
        strokeWidth = width;
        paint.setStrokeWidth(width);
    }

    public void clear(){
        bitmap = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        buffer = new Canvas(bitmap);
        currentSegment = null;
        outstandingSegments.clear();
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        scale = Math.min(1.0f * w / canvasWidth,1.0f * h / canvasHeight);

        bitmap = Bitmap.createBitmap((int) Math.ceil(w), (int) Math.ceil(h), Bitmap.Config.ARGB_8888);
        buffer = new Canvas(bitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.DKGRAY);
        canvas.drawRect(0,0,bitmap.getWidth(),bitmap.getHeight(),paintFromColor(Color.WHITE, Paint.Style.FILL_AND_STROKE,strokeWidth));

        canvas.drawBitmap(bitmap,0,0,bitmapPaint);

        canvas.drawPath(path,paint);
    }

    public static Paint paintFromColor(int color,float width){
        return paintFromColor(color, Paint.Style.STROKE,width);
    }

    public static Paint paintFromColor(int color,Paint.Style style,float width){
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setDither(true);
        p.setColor(color);
        p.setStyle(style);
        p.setStrokeWidth(width);
        return p;
    }

    public static Path getPathForPoints(List<CanvasPoint> points, double scale){
        Path path = new Path();
        scale = scale * PIXEL_SIZE;
        CanvasPoint current = points.get(0);
        path.moveTo(Math.round(scale * current.x),Math.round(scale * current.y));
        CanvasPoint next = null;
        for (int i = 1;i < points.size();++i){
            next = points.get(i);
            path.quadTo(Math.round(scale * current.x),
                    Math.round(scale * current.y),
                    Math.round(scale * (next.x + current.x) / 2),
                    Math.round(scale * (next.y + current.y) / 2));
            current = next;
        }
        if(next != null){
            path.lineTo(Math.round(scale * next.x),Math.round(scale * next.y));
        }
        return path;
    }

    private void drawSegment(CanvasSegment segment,Paint paint){
        if(buffer != null){
            buffer.drawPath(getPathForPoints(segment.getPoints(),scale),paint);
        }
    }

    private void onTouchStart(float x, float y) {
        path.reset();
        path.moveTo(x, y);
        currentSegment = new CanvasSegment(currentColor,strokeWidth);
        lastX = (int) x / PIXEL_SIZE;
        lastY = (int) y / PIXEL_SIZE;
        currentSegment.addPoint(lastX, lastY);
    }

    private void onTouchMove(float x, float y) {

        int x1 = (int) x / PIXEL_SIZE;
        int y1 = (int) y / PIXEL_SIZE;

        float dx = Math.abs(x1 - lastX);
        float dy = Math.abs(y1 - lastY);
        if (dx >= 1 || dy >= 1) {
            path.quadTo(lastX * PIXEL_SIZE, lastY * PIXEL_SIZE, ((x1 + lastX) * PIXEL_SIZE) / 2, ((y1 + lastY) * PIXEL_SIZE) / 2);
            lastX = x1;
            lastY = y1;
            currentSegment.addPoint(lastX, lastY);
        }
    }

    private void onTouchEnd() {
        path.lineTo(lastX * PIXEL_SIZE, lastY * PIXEL_SIZE);
        buffer.drawPath(path, paint);
        path.reset();
        DatabaseReference segmentRef = firebaseRef.push();
        final String segmentName = segmentRef.getKey();
        outstandingSegments.add(segmentName);

        CanvasSegment segment = new CanvasSegment(currentSegment.getColor(),currentSegment.getWidth());
        for (CanvasPoint point: currentSegment.getPoints()) {
            segment.addPoint((int)Math.round(point.x / scale), (int)Math.round(point.y / scale));
        }
        segmentRef.setValue(segment, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if(databaseError != null){
                    throw databaseError.toException();
                }
                outstandingSegments.remove(segmentName);
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN :
                onTouchStart(x,y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE :
                onTouchMove(x,y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP :
                onTouchEnd();
                invalidate();
                break;
        }
        return true;
    }
}
