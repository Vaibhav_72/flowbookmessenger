package com.example.flowbookmessenger.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.flowbookmessenger.activities.CreateGroupActivity;
import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.adapters.ContactsAdapter;
import com.example.flowbookmessenger.models.Contacts;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment {

    private RecyclerView contacts_recycler;
    ContactsAdapter contactsAdapter;
    List<Contacts> contactsList;

    FloatingActionButton contact_create_grp_btn;

    public ContactsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        contacts_recycler = view.findViewById(R.id.contacts_recycler_view);
        contacts_recycler.setHasFixedSize(true);
        contacts_recycler.setLayoutManager(new LinearLayoutManager(getContext()));

        contactsList = new ArrayList<>();

        contact_create_grp_btn = view.findViewById(R.id.contact_create_grp_btn);
        contact_create_grp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(contactsAdapter.getSelectedContactsID().size() < 2){
                    Toast.makeText(getContext(), "Please Select atleast 2 contacts to create a group", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(getContext(), CreateGroupActivity.class);
                    intent.putStringArrayListExtra("Selected Contacts ID",contactsAdapter.getSelectedContactsID());
                    intent.putStringArrayListExtra("Selected Contacts Name",contactsAdapter.getSelectedContactsName());
                    //intent.putParcelableArrayListExtra(Contacts.TYPE,contactsAdapter.getSelectedContacts());
                    getContext().startActivity(intent);
                }
            }
        });

        availableUsers();

        return view;
    }

    private void availableUsers() {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contactsList.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    Contacts contacts = d.getValue(Contacts.class);
                    assert contacts != null;
                    assert user != null;
                    if(!user.getUid().equals(contacts.getUserid())) {
                        contactsList.add(contacts);
                    }
                }
                contactsAdapter = new ContactsAdapter(getContext(),contactsList,false);
                contacts_recycler.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void beginSearch(String s){
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Query query = FirebaseDatabase.getInstance().getReference("Users").orderByChild("search")
                .startAt(s)
                .endAt(s + "\uf8ff");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contactsList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Contacts c = snapshot.getValue(Contacts.class);
                    assert c != null;
                    assert firebaseUser != null;
                    if(!c.getUserid().equals(firebaseUser.getUid())){
                        contactsList.add(c);
                    }
                }
                contactsAdapter = new ContactsAdapter(getContext(),contactsList,false);
                contacts_recycler.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
