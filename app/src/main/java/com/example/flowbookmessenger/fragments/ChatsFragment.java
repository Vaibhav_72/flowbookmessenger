package com.example.flowbookmessenger.fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.flowbookmessenger.activities.ChatActivity;
import com.example.flowbookmessenger.models.Chats;
import com.example.flowbookmessenger.models.Chatslist;
import com.example.flowbookmessenger.models.Contacts;
import com.example.flowbookmessenger.adapters.ContactsAdapter;
import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.notifications.ChatToken;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsFragment extends Fragment {

    RecyclerView chats_fragment_recyclerview;
    DatabaseReference chatslistRefrence;
    DatabaseReference chatsRefrence;
    DatabaseReference usersRefrence;
    FirebaseUser user;
    //DatabaseReference dbrefrence;
    //ContactsAdapter contactsAdapter;

    //List<Contacts> contacts;
    //List<Chatslist> chatslist;

    public ChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chats, container, false);
        chats_fragment_recyclerview = v.findViewById(R.id.chats_fragment_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        chats_fragment_recyclerview.setHasFixedSize(true);
        chats_fragment_recyclerview.setLayoutManager(linearLayoutManager);

        user = FirebaseAuth.getInstance().getCurrentUser();

        chatslistRefrence = FirebaseDatabase.getInstance().getReference().child("Chatslist").child(user.getUid());
        chatslistRefrence.keepSynced(true);

        usersRefrence = FirebaseDatabase.getInstance().getReference().child("Users");
        usersRefrence.keepSynced(true);

        chatsRefrence = FirebaseDatabase.getInstance().getReference().child("Chats").child(user.getUid());

        /*chatslist = new ArrayList<>();

        dbrefrence = FirebaseDatabase.getInstance().getReference("Chatslist").child(user.getUid());
        dbrefrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chatslist.clear();
                for (DataSnapshot s : dataSnapshot.getChildren()){
                    Chatslist c = s.getValue(Chatslist.class);
                    chatslist.add(c);
                }
                recentChats();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

        updateToken(FirebaseInstanceId.getInstance().getToken());

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        Query chatlistQuery = chatslistRefrence.orderByChild("lastMsgDateTime");
        FirebaseRecyclerAdapter<Chatslist,ChatlistViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Chatslist, ChatlistViewHolder>(
                Chatslist.class,
                R.layout.contacts_row_layout,
                ChatlistViewHolder.class,
                chatlistQuery) {

            @Override
            protected void populateViewHolder(final ChatlistViewHolder chatlistViewHolder, Chatslist chatslist, int i) {
                final String chatListUserID = getRef(i).getKey();
                assert chatListUserID != null;
                Query lastMsgQuery = chatsRefrence.child(chatListUserID).limitToLast(1);
                lastMsgQuery.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        //String data = dataSnapshot.child("Chats").getValue().toString();
                        Chats chats = dataSnapshot.getValue(Chats.class);
                        chatlistViewHolder.setLastMsg(chats);

                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                usersRefrence.child(chatListUserID).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String name = dataSnapshot.child("name").getValue().toString();
                        chatlistViewHolder.setName(name);

                        chatlistViewHolder.v.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent chatIntent = new Intent(getContext(), ChatActivity.class);
                                chatIntent.putExtra("userid",chatListUserID);
                                startActivity(chatIntent);
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        };
        chats_fragment_recyclerview.setAdapter(firebaseRecyclerAdapter);
    }

    public static class ChatlistViewHolder extends RecyclerView.ViewHolder{

        View v;

        public ChatlistViewHolder(@NonNull View itemView) {
            super(itemView);
            v = itemView;
        }

        @SuppressLint("ResourceAsColor")
        public void setLastMsg(Chats c) {

            Calendar calendar = Calendar.getInstance();
            final SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
            final String currentDate = format.format(calendar.getTime());

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            ImageView chat_last_message_status = v.findViewById(R.id.chat_last_message_status);
            ImageView chat_last_message_iv = v.findViewById(R.id.chat_last_message_iv);
            TextView chat_last_message = v.findViewById(R.id.chat_last_message);
            TextView chat_last_message_time = v.findViewById(R.id.chat_last_message_time);


            try {
                if(format.parse(c.getMessage_Date()).equals(format.parse(currentDate))){
                    chat_last_message_time.setText(c.getMessage_Time());
                }else {
                    chat_last_message_time.setText(c.getMessage_Date());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(c.getReceiver().equals(user.getUid()) && c.getMessage_status().equals("sent")){
                chat_last_message_status.setVisibility(View.GONE);
                chat_last_message.setTextColor(R.color.greenNotify);
                chat_last_message_time.setTextColor(R.color.greenNotify);
                chat_last_message.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                chat_last_message_time.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                if(c.getM_type().equals("text")){
                    chat_last_message_iv.setImageResource(R.drawable.text_sent);
                    chat_last_message.setText(c.getMessage());
                }else if(c.getM_type().equals("document")){
                    chat_last_message_iv.setImageResource(R.drawable.document_sent);
                    chat_last_message.setText("Document");
                }else if(c.getM_type().equals("gallery")){
                    chat_last_message_iv.setImageResource(R.drawable.gallery_sent);
                    chat_last_message.setText("Photo");
                }else if(c.getM_type().equals("audio")){
                    chat_last_message_iv.setImageResource(R.drawable.audio_sent);
                    chat_last_message.setText("Audio");
                }else if(c.getM_type().equals("video")){
                    chat_last_message_iv.setImageResource(R.drawable.video_sent);
                    chat_last_message.setText("Video");
                }else if(c.getM_type().equals("deleted")){
                    chat_last_message_iv.setImageResource(R.drawable.deleted_sent);
                    chat_last_message.setText("Message deleted");
                }
            }else if(c.getReceiver().equals(user.getUid()) && c.getMessage_status().equals("seen")){
                chat_last_message_status.setVisibility(View.GONE);
                if(c.getM_type().equals("text")){
                    chat_last_message_iv.setImageResource(R.drawable.text);
                    chat_last_message.setText(c.getMessage());
                }else if(c.getM_type().equals("document")){
                    chat_last_message_iv.setImageResource(R.drawable.document);
                    chat_last_message.setText("Document");
                }else if(c.getM_type().equals("gallery")){
                    chat_last_message_iv.setImageResource(R.drawable.gallery);
                    chat_last_message.setText("Photo");
                }else if(c.getM_type().equals("audio")){
                    chat_last_message_iv.setImageResource(R.drawable.audio);
                    chat_last_message.setText("Audio");
                }else if(c.getM_type().equals("video")){
                    chat_last_message_iv.setImageResource(R.drawable.video);
                    chat_last_message.setText("Video");
                }else if(c.getM_type().equals("deleted")){
                    chat_last_message_iv.setImageResource(R.drawable.deleted);
                    chat_last_message.setText("Message deleted");
                }
            }else if(c.getSender().equals(user.getUid())){
                chat_last_message_status.setVisibility(View.VISIBLE);
                if(c.getMessage_status().equals("sent")){
                    chat_last_message_status.setImageResource(R.drawable.sent_row);
                }else if(c.getMessage_status().equals("seen")){
                    chat_last_message_status.setImageResource(R.drawable.seen_row);
                }
                if(c.getM_type().equals("text")){
                    chat_last_message_iv.setImageResource(R.drawable.text);
                    chat_last_message.setText(c.getMessage());
                }else if(c.getM_type().equals("document")){
                    chat_last_message_iv.setImageResource(R.drawable.document);
                    chat_last_message.setText("Document");
                }else if(c.getM_type().equals("gallery")){
                    chat_last_message_iv.setImageResource(R.drawable.gallery);
                    chat_last_message.setText("Photo");
                }else if(c.getM_type().equals("audio")){
                    chat_last_message_iv.setImageResource(R.drawable.audio);
                    chat_last_message.setText("Audio");
                }else if(c.getM_type().equals("video")){
                    chat_last_message_iv.setImageResource(R.drawable.video);
                    chat_last_message.setText("Video");
                }else if(c.getM_type().equals("deleted")){
                    chat_last_message_status.setVisibility(View.GONE);
                    chat_last_message_iv.setImageResource(R.drawable.deleted);
                    chat_last_message.setText("Message deleted");
                }
            }
        }

        public void setName(String n){
            TextView contact_row_name = v.findViewById(R.id.contact_row_name);
            CircleImageView contact_row_icon = v.findViewById(R.id.contact_row_icon);

            contact_row_name.setText(n);
            contact_row_icon.setImageResource(R.drawable.student_icon);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //recentChats();
    }

    private void updateToken(String token){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Tokens");
        ChatToken token1 = new ChatToken(token);
        reference.child(user.getUid()).setValue(token1);
    }

    /*private void recentChats() {
        contacts = new ArrayList<>();
        dbrefrence = FirebaseDatabase.getInstance().getReference("Users");
        dbrefrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contacts.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Contacts contact = ds.getValue(Contacts.class);
                    for (Chatslist cl : chatslist){
                        if(cl.getId().equals(contact.getUserid())){
                            contacts.add(contact);
                        }
                    }
                }
                contactsAdapter = new ContactsAdapter(getContext(),contacts,true);
                chats_fragment_recyclerview.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }*/

    /*public void beginSearch(String s){
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Query query = FirebaseDatabase.getInstance().getReference("Users").orderByChild("search")
                .startAt(s)
                .endAt(s + "\uf8ff");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contacts.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Contacts c = snapshot.getValue(Contacts.class);
                    assert c != null;
                    assert firebaseUser != null;
                    if(!c.getUserid().equals(firebaseUser.getUid())){
                        contacts.add(c);
                    }
                }
                contactsAdapter = new ContactsAdapter(getContext(),contacts,false);
                chats_fragment_recyclerview.setAdapter(contactsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }*/
}
