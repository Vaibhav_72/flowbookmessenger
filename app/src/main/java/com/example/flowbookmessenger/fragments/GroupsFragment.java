package com.example.flowbookmessenger.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.adapters.GroupsAdapter;
import com.example.flowbookmessenger.models.Groups;
import com.example.flowbookmessenger.notifications.ChatToken;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupsFragment extends Fragment {
    
    private RecyclerView groups_recycler;
    GroupsAdapter groupsAdapter;
    List<Groups> groupsList;
    FirebaseUser user;


    public GroupsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_groups, container, false);
        groups_recycler = view.findViewById(R.id.groups_recycler_view);
        groups_recycler.setHasFixedSize(true);
        groups_recycler.setLayoutManager(new LinearLayoutManager(getContext()));

        user = FirebaseAuth.getInstance().getCurrentUser();
        
        groupsList = new ArrayList<>();
        
        currentUserGroups();

        updateToken(FirebaseInstanceId.getInstance().getToken());
        
        return view;
    }

    private void updateToken(String token){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Tokens");
        ChatToken token1 = new ChatToken(token);
        reference.child(user.getUid()).setValue(token1);
    }

    private void currentUserGroups() {
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Groups");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                groupsList.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    Groups groups = d.getValue(Groups.class);
                    assert groups != null;
                    assert firebaseUser != null;
                    if(groups.getGroupParticipantsID().contains(firebaseUser.getUid())){
                        groupsList.add(groups);
                    }
                }
                groupsAdapter = new GroupsAdapter(getContext(),groupsList);
                groups_recycler.setAdapter(groupsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void beginSearch(String s){
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Query query = FirebaseDatabase.getInstance().getReference("Groups").orderByChild("search")
                .startAt(s)
                .endAt(s + "\uf8ff");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                groupsList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Groups g = snapshot.getValue(Groups.class);
                    assert g != null;
                    assert firebaseUser != null;
                    if(g.getGroupParticipantsID().contains(firebaseUser.getUid())){
                        groupsList.add(g);
                    }
                }
                groupsAdapter = new GroupsAdapter(getContext(),groupsList);
                groups_recycler.setAdapter(groupsAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
