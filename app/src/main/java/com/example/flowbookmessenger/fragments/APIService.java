package com.example.flowbookmessenger.fragments;

import com.example.flowbookmessenger.notifications.ChatResponse;
import com.example.flowbookmessenger.notifications.ChatSender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAX4sUwIM:APA91bGHFHBCtNT9yGv6_wv6_FQxKRhphO4q9I4AzbiPVWY3_o6mqxtZQ5oXuVZFEe_mBTgPLgr5l6mlVHKcwf5AAdI1DliYCcqtNP9-_xG-wJU8kOqfo533uOxugLnM15-V4taP3nIa"
            }
    )

    @POST("fcm/send")
    Call<ChatResponse> sendNotification(@Body ChatSender body);
}
