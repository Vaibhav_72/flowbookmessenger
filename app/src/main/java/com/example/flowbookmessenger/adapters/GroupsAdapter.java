package com.example.flowbookmessenger.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.activities.GroupChatActivity;
import com.example.flowbookmessenger.models.GroupChats;
import com.example.flowbookmessenger.models.Groups;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.ViewHolder> {

    private Context context;
    private List<Groups> groupsList;
    String grpLastMsg;
    int grpUnreadCount;
    ColorStateList timeTVColor;
    ColorStateList lastMsgTVColor;

    public GroupsAdapter(Context context, List<Groups> groupsList) {
        this.context = context;
        this.groupsList = groupsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.group_row_layout,parent,false);
        return new GroupsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupsAdapter.ViewHolder holder, int position) {
        final Groups groups = groupsList.get(position);
        holder.group_name.setText(groups.getGroupName());
        holder.group_icon.setImageResource(R.drawable.flowbook_logo);
        groupLastMessage(groups.getGroupID(),holder.group_last_message,holder.group_last_message_time,holder.group_last_message_iv,holder.group_last_message_status);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GroupChatActivity.class);
                intent.putExtra("groupID",groups.getGroupID());
                /*intent.putExtra("groupName",groups.getGroupName());
                intent.putExtra("groupDescription",groups.getGroupDescription());
                intent.putExtra("groupAdminID",groups.getGroupAdminID());
                intent.putExtra("groupAdminName",groups.getGroupAdminName());
                intent.putExtra("groupParticipantsCount",groups.getGroupParticipantsCount());
                intent.putStringArrayListExtra("groupParticipantsID",groups.getGroupParticipantsID());
                intent.putStringArrayListExtra("groupParticipantsName",groups.getGroupParticipantsName());*/
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return groupsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView group_name;
        public CircleImageView group_icon;
        public TextView group_last_message,group_last_message_time,group_messages_unread_count;
        public ImageView group_last_message_iv;
        public ImageView group_last_message_status;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            group_name = (TextView) itemView.findViewById(R.id.group_row_name);
            group_icon = (CircleImageView) itemView.findViewById(R.id.group_row_icon);
            group_last_message = (TextView) itemView.findViewById(R.id.group_last_message);
            group_last_message_time = (TextView) itemView.findViewById(R.id.group_last_message_time);
            group_messages_unread_count = (TextView) itemView.findViewById(R.id.group_messages_unread_count);
            group_last_message_iv = (ImageView) itemView.findViewById(R.id.group_last_message_iv);
            group_last_message_status = (ImageView) itemView.findViewById(R.id.group_last_message_status);
            timeTVColor = group_last_message_time.getTextColors();
            lastMsgTVColor = group_last_message.getTextColors();
        }
    }

    private void groupLastMessage(final String groupID, final TextView groupLastMsgTV, final TextView groupLastMsgTime, final ImageView groupLastMsgIV, final ImageView groupLastMsgStatus){
        Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        final String currentDate = format.format(calendar.getTime());
        grpLastMsg = "no msg";
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference grpChatsRef = FirebaseDatabase.getInstance().getReference("Group Chats");
        grpChatsRef.addValueEventListener(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot s : dataSnapshot.getChildren()){
                    GroupChats groupChats = s.getValue(GroupChats.class);
                    if(user != null && groupChats != null){
                        if(groupChats.getGroup_ID().equals(groupID)){
                            try {
                                if(format.parse(groupChats.getGroup_message_Date()).equals(format.parse(currentDate))){
                                    groupLastMsgTime.setText(groupChats.getGroup_message_Time());
                                }else {
                                    groupLastMsgTime.setText(groupChats.getGroup_message_Date());
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            //ColorStateList timeTVColor = groupLastMsgTime.getTextColors();
                            //ColorStateList lastMsgTVColor = groupLastMsgTV.getTextColors();
                            if(groupChats.getSender().equals(user.getUid())){
                                groupLastMsgStatus.setVisibility(View.VISIBLE);
                                if(groupChats.getGroup_message_status().equals("sent")){
                                    groupLastMsgStatus.setImageResource(R.drawable.sent_row);
                                }else if(groupChats.getGroup_message_status().equals("seen")){
                                    groupLastMsgStatus.setImageResource(R.drawable.seen_row);
                                }
                                groupLastMsgTV.setTextColor(lastMsgTVColor);
                                groupLastMsgTime.setTextColor(timeTVColor);
                                groupLastMsgTV.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                                groupLastMsgTime.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                                if(groupChats.getM_type().equals("text")){
                                    groupLastMsgIV.setImageResource(R.drawable.text);
                                    groupLastMsgTV.setText(groupChats.getGroup_message());
                                }else if(groupChats.getM_type().equals("document")){
                                    groupLastMsgIV.setImageResource(R.drawable.document);
                                    groupLastMsgTV.setText("Document");
                                }else if(groupChats.getM_type().equals("gallery")){
                                    groupLastMsgIV.setImageResource(R.drawable.gallery);
                                    groupLastMsgTV.setText("Photo");
                                }else if(groupChats.getM_type().equals("audio")){
                                    groupLastMsgIV.setImageResource(R.drawable.audio);
                                    groupLastMsgTV.setText("Audio");
                                }else if(groupChats.getM_type().equals("video")){
                                    groupLastMsgIV.setImageResource(R.drawable.video);
                                    groupLastMsgTV.setText("Video");
                                }else if(groupChats.getM_type().equals("deleted")){
                                    groupLastMsgStatus.setVisibility(View.GONE);
                                    groupLastMsgIV.setImageResource(R.drawable.deleted);
                                    groupLastMsgTV.setText("Message deleted");
                                }
                            }else if(groupChats.getReadBy().contains(user.getUid())){
                                groupLastMsgStatus.setVisibility(View.GONE);
                                groupLastMsgTV.setTextColor(lastMsgTVColor);
                                groupLastMsgTime.setTextColor(timeTVColor);
                                groupLastMsgTV.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                                groupLastMsgTime.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                                if(groupChats.getM_type().equals("text")){
                                    groupLastMsgIV.setImageResource(R.drawable.text);
                                    groupLastMsgTV.setText(groupChats.getGroup_message());
                                }else if(groupChats.getM_type().equals("document")){
                                    groupLastMsgIV.setImageResource(R.drawable.document);
                                    groupLastMsgTV.setText("Document");
                                }else if(groupChats.getM_type().equals("gallery")){
                                    groupLastMsgIV.setImageResource(R.drawable.gallery);
                                    groupLastMsgTV.setText("Photo");
                                }else if(groupChats.getM_type().equals("audio")){
                                    groupLastMsgIV.setImageResource(R.drawable.audio);
                                    groupLastMsgTV.setText("Audio");
                                }else if(groupChats.getM_type().equals("video")){
                                    groupLastMsgIV.setImageResource(R.drawable.video);
                                    groupLastMsgTV.setText("Video");
                                }else if(groupChats.getM_type().equals("deleted")){
                                    groupLastMsgIV.setImageResource(R.drawable.deleted);
                                    groupLastMsgTV.setText("Message deleted");
                                }
                            }else if(!groupChats.getReadBy().contains(user.getUid())){
                                groupLastMsgStatus.setVisibility(View.GONE);
                                groupLastMsgTV.setTextColor(R.color.greenNotify);
                                groupLastMsgTime.setTextColor(R.color.greenNotify);
                                groupLastMsgTV.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                                groupLastMsgTime.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                                if(groupChats.getM_type().equals("text")){
                                    groupLastMsgIV.setImageResource(R.drawable.text_sent);
                                    groupLastMsgTV.setText(groupChats.getGroup_message());
                                }else if(groupChats.getM_type().equals("document")){
                                    groupLastMsgIV.setImageResource(R.drawable.document_sent);
                                    groupLastMsgTV.setText("Document");
                                }else if(groupChats.getM_type().equals("gallery")){
                                    groupLastMsgIV.setImageResource(R.drawable.gallery_sent);
                                    groupLastMsgTV.setText("Photo");
                                }else if(groupChats.getM_type().equals("audio")){
                                    groupLastMsgIV.setImageResource(R.drawable.audio_sent);
                                    groupLastMsgTV.setText("Audio");
                                }else if(groupChats.getM_type().equals("video")){
                                    groupLastMsgIV.setImageResource(R.drawable.video_sent);
                                    groupLastMsgTV.setText("Video");
                                }else if(groupChats.getM_type().equals("deleted")){
                                    groupLastMsgIV.setImageResource(R.drawable.deleted_sent);
                                    groupLastMsgTV.setText("Message deleted");
                                }
                            }
                        }
                    }
                }
                grpLastMsg = "no msg";
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
