package com.example.flowbookmessenger.adapters;

import android.app.DownloadManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.flowbookmessenger.activities.ForwardActivity;
import com.example.flowbookmessenger.activities.FullScreenImageActivity;
import com.example.flowbookmessenger.activities.FullScreenVideoActivity;
import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.models.Chats;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {

    private Context context;
    private List<Chats> chatsList;
    public static final int MSG_SENT = 0;
    public static final int MSG_RECEIVED = 1;
    //MediaPlayer mediaPlayer;
    //DisplayMetrics metrics;

    FirebaseUser user;

    public ChatsAdapter(Context context, List<Chats> chats_list) {
        this.context = context;
        this.chatsList = chats_list;
    }

    @NonNull
    @Override
    public ChatsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == MSG_SENT){
            View v = LayoutInflater.from(context).inflate(R.layout.chat_send_message,parent,false);
            return new ChatsAdapter.ViewHolder(v);
        }else {
            View v = LayoutInflater.from(context).inflate(R.layout.chat_receive_message,parent,false);
            return new ChatsAdapter.ViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ChatsAdapter.ViewHolder holder, int position) {
        //Chats chatPrevMsg = chatsList.get(position - 1);
        final Chats chat = chatsList.get(position);
        user = FirebaseAuth.getInstance().getCurrentUser();
        holder.text_message_relative.setVisibility(View.GONE);
        holder.image_message_relative.setVisibility(View.GONE);
        holder.document_message_relative.setVisibility(View.GONE);
        holder.audio_message_relative.setVisibility(View.GONE);
        holder.video_message_relative.setVisibility(View.GONE);
        holder.camera_message_relative.setVisibility(View.GONE);
        holder.deleted_message_relative.setVisibility(View.GONE);
        holder.audio_message_controls.setVisibility(View.GONE);
        //holder.chat_message_date.setVisibility(View.GONE);

        /*SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        if(position >= 1){
            Chats previousMessage = chatsList.get(position - 1);
            try {
                if(format.parse(previousMessage.getMessage_Date()).equals(format.parse(chat.getMessage_Date()))){
                    holder.chat_message_date.setVisibility(View.GONE);
                }else {
                    holder.chat_message_date.setText(chat.getMessage_Date());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else {
            holder.chat_message_date.setText(chat.getMessage_Date());
        }*/
        holder.chat_message_date.setText(chat.getMessage_Date());
        if(user.getUid().equals(chat.getSender())){
            if(("text").equals(chat.getM_type())){
                holder.text_message_relative.setVisibility(View.VISIBLE);
                if(chat.getMessage().startsWith("*") && chat.getMessage().endsWith("*")){
                    holder.text_message.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                    holder.text_message.setText(chat.getMessage().substring(1,chat.getMessage().length() - 1));
                }else {
                    holder.text_message.setText(chat.getMessage());
                }
                holder.text_message_time.setText(chat.getMessage_Time());
                if(chat.getMessage_status().equals("seen")){
                    holder.text_message_status.setImageResource(R.drawable.message_seen);
                }else if(chat.getMessage_status().equals("sent")){
                    holder.text_message_status.setImageResource(R.drawable.message_sent);
                }
                holder.text_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(chat.getMessage_key(),chat.getMessage_DateTime(),chat.getM_type(),"","",chat.getMessage(),chat.getSender(),chat.getReceiver());
                        return true;
                    }
                });
            }else if(("gallery").equals(chat.getM_type())){
                holder.image_message_relative.setVisibility(View.VISIBLE);
                Glide.with(context).load(chat.getMessage()).into(holder.image_message);
                holder.image_message_time.setText(chat.getMessage_Time());
                if(chat.getMessage_status().equals("seen")){
                    holder.image_message_status.setImageResource(R.drawable.message_seen);
                }else if(chat.getMessage_status().equals("sent")){
                    holder.image_message_status.setImageResource(R.drawable.message_sent);
                }
                final String imageURL = chat.getMessage();
                final String imageName = chat.getName();
                holder.image_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent fullScreenIntent = new Intent(holder.image_message.getContext(), FullScreenImageActivity.class);
                        fullScreenIntent.putExtra("ImageURL",imageURL);
                        holder.image_message.getContext().startActivity(fullScreenIntent);
                    }
                });
                holder.image_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(chat.getMessage_key(),chat.getMessage_DateTime(),chat.getM_type(),imageName,imageURL,"",chat.getSender(),chat.getReceiver());
                        //downloadMedia(imageName,imageURL);
                        return true;
                    }
                });
            }/*else if(("camera").equals(chat.getM_type())){
            holder.image_message.setVisibility(View.VISIBLE);
            Glide.with(context).load(chat.getMessage()).into(holder.image_message);
        }*/else if(("document").equals(chat.getM_type())){
                holder.document_message_relative.setVisibility(View.VISIBLE);
                holder.document_message_doc_name.setText(chat.getName());
                holder.document_message_time.setText(chat.getMessage_Time());
                if(chat.getMessage_status().equals("seen")){
                    holder.document_message_status.setImageResource(R.drawable.message_seen);
                }else if(chat.getMessage_status().equals("sent")){
                    holder.document_message_status.setImageResource(R.drawable.message_sent);
                }
                final String documentURL = chat.getMessage();
                final String documentName = chat.getName();
                holder.document_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(context, "Document Message", Toast.LENGTH_SHORT).show();
                        Intent documentIntent = new Intent();
                        documentIntent.setAction(Intent.ACTION_VIEW);
                        documentIntent.setDataAndType(Uri.parse(chat.getMessage()),"application/pdf");
                        holder.document_message.getContext().startActivity(documentIntent);
                    }
                });
                holder.document_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(chat.getMessage_key(),chat.getMessage_DateTime(),chat.getM_type(),documentName,documentURL,"",chat.getSender(),chat.getReceiver());
                        //downloadMedia(documentName,documentURL);
                        return true;
                    }
                });
            }else if(("audio").equals(chat.getM_type())){
                holder.audio_message_relative.setVisibility(View.VISIBLE);
                holder.audio_message_time.setText(chat.getMessage_Time());
                if(chat.getMessage_status().equals("seen")){
                    holder.audio_message_status.setImageResource(R.drawable.message_seen);
                }else if(chat.getMessage_status().equals("sent")){
                    holder.audio_message_status.setImageResource(R.drawable.message_sent);
                }
                final String audioURL = chat.getMessage();
                final String audioName = chat.getName();
                holder.audio_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.audio_message_relative.setVisibility(View.GONE);
                        holder.audio_message_controls.setVisibility(View.VISIBLE);
                        final MediaPlayer mediaPlayer = new MediaPlayer();
                        try {
                            mediaPlayer.setDataSource(holder.itemView.getContext(),Uri.parse(chat.getMessage()));
                            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {
                                    holder.audio_message_seekbar.setMax(mp.getDuration());
                                    mp.start();
                                    //holder.audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_pause);
                                    seekBarStatus();
                                }

                                private void seekBarStatus() {
                                    holder.audio_message_seekbar.setProgress(mediaPlayer.getCurrentPosition());
                                    if(mediaPlayer.isPlaying()){
                                        holder.audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_pause);
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                seekBarStatus();
                                            }
                                        };
                                        Handler handler = new Handler();
                                        handler.postDelayed(runnable,2000);
                                    }else if(!mediaPlayer.isPlaying()){
                                        holder.audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_play);
                                        seekBarStatus();
                                    }
                                }
                            });
                            holder.audio_message_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                @Override
                                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                    if(fromUser){
                                        mediaPlayer.seekTo(progress);
                                    }
                                }

                                @Override
                                public void onStartTrackingTouch(SeekBar seekBar) {

                                }

                                @Override
                                public void onStopTrackingTouch(SeekBar seekBar) {

                                }
                            });
                            mediaPlayer.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                holder.audio_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(chat.getMessage_key(),chat.getMessage_DateTime(),chat.getM_type(),audioName,audioURL,"",chat.getSender(),chat.getReceiver());
                        //downloadMedia(audioName,audioURL);
                        return true;
                    }
                });
            }
            else if(("video").equals(chat.getM_type())){
                holder.video_message_relative.setVisibility(View.VISIBLE);
                holder.video_message_time.setText(chat.getMessage_Time());
                if(chat.getMessage_status().equals("seen")){
                    holder.video_message_status.setImageResource(R.drawable.message_seen);
                }else if(chat.getMessage_status().equals("sent")){
                    holder.video_message_status.setImageResource(R.drawable.message_sent);
                }
                holder.videoMediaController = new MediaController(holder.itemView.getContext());
                holder.video_message.setMediaController(holder.videoMediaController);
                holder.videoMediaController.setAnchorView(holder.video_message);
                holder.video_message.setVideoURI(Uri.parse(chat.getMessage()));
                final String videoURL = chat.getMessage();
                final String videoName = chat.getName();
                holder.video_message_full_screen_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(holder.video_message_full_screen_btn.getContext(), FullScreenVideoActivity.class);
                        intent.putExtra("url", videoURL);
                        holder.video_message_full_screen_btn.getContext().startActivity(intent);
                    /*metrics = new DisplayMetrics();
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) holder.video_message.getLayoutParams();
                    params.width = metrics.widthPixels;
                    params.height = metrics.heightPixels;
                    holder.video_message.setLayoutParams(params);*/
                    }
                });
                //holder.video_message.start();
                holder.video_message.seekTo(1);
                holder.video_message.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        //mp.start();
                    }
                });
                holder.video_message_relative.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(chat.getMessage_key(),chat.getMessage_DateTime(),chat.getM_type(),videoName,videoURL,"",chat.getSender(),chat.getReceiver());
                        //downloadMedia(videoName,videoURL);
                        return true;
                    }
                });
            }else if(("camera").equals(chat.getM_type())){
                holder.camera_message_relative.setVisibility(View.VISIBLE);
                Glide.with(context).load(chat.getMessage()).into(holder.camera_message);
                holder.camera_message_time.setText(chat.getMessage_Time());
                if(chat.getMessage_status().equals("seen")){
                    holder.camera_message_status.setImageResource(R.drawable.message_seen);
                }else if(chat.getMessage_status().equals("sent")){
                    holder.camera_message_status.setImageResource(R.drawable.message_sent);
                }
                final String cameraImageURL = chat.getMessage();
                final String cameraImageName = chat.getName();
                holder.camera_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent fullScreenIntent = new Intent(holder.camera_message.getContext(),FullScreenImageActivity.class);
                        fullScreenIntent.putExtra("ImageURL",cameraImageURL);
                        holder.camera_message.getContext().startActivity(fullScreenIntent);
                    }
                });
                holder.camera_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(chat.getMessage_key(),chat.getMessage_DateTime(),chat.getM_type(),cameraImageName,cameraImageURL,"",chat.getSender(),chat.getReceiver());
                        //downloadMedia(cameraImageName,cameraImageURL);
                        return true;
                    }
                });
            }else if(("deleted").equals(chat.getM_type())){
                holder.deleted_message_relative.setVisibility(View.VISIBLE);
                holder.deleted_message.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
                holder.deleted_message.setText("You deleted this message");
                holder.deleted_message_time.setText(chat.getMessage_Time());
            }
        }else if(!(user.getUid().equals(chat.getSender()))){
            if(("text").equals(chat.getM_type())){
                holder.text_message_relative.setVisibility(View.VISIBLE);
                if(chat.getMessage().startsWith("*") && chat.getMessage().endsWith("*")){
                    holder.text_message.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                    holder.text_message.setText(chat.getMessage().substring(1,chat.getMessage().length() - 1));
                }else {
                    holder.text_message.setText(chat.getMessage());
                }
                holder.text_message_time.setText(chat.getMessage_Time());
                holder.text_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        receiverOptions(chat.getM_type(),"","",chat.getMessage());
                        return true;
                    }
                });
            }else if(("gallery").equals(chat.getM_type())){
                holder.image_message_relative.setVisibility(View.VISIBLE);
                Glide.with(context).load(chat.getMessage()).into(holder.image_message);
                holder.image_message_time.setText(chat.getMessage_Time());
                final String imageURL = chat.getMessage();
                final String imageName = chat.getName();
                holder.image_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent fullScreenIntent = new Intent(holder.image_message.getContext(),FullScreenImageActivity.class);
                        fullScreenIntent.putExtra("ImageURL",imageURL);
                        holder.image_message.getContext().startActivity(fullScreenIntent);
                    }
                });
                holder.image_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        receiverOptions(chat.getM_type(),imageName,imageURL,"");
                        //downloadMedia(imageName,imageURL);
                        return true;
                    }
                });
            }/*else if(("camera").equals(chat.getM_type())){
            holder.image_message.setVisibility(View.VISIBLE);
            Glide.with(context).load(chat.getMessage()).into(holder.image_message);
        }*/else if(("document").equals(chat.getM_type())){
                holder.document_message_relative.setVisibility(View.VISIBLE);
                holder.document_message_doc_name.setText(chat.getName());
                holder.document_message_time.setText(chat.getMessage_Time());
                final String documentURL = chat.getMessage();
                final String documentName = chat.getName();
                holder.document_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(context, "Document Message", Toast.LENGTH_SHORT).show();
                        Intent documentIntent = new Intent();
                        documentIntent.setAction(Intent.ACTION_VIEW);
                        documentIntent.setDataAndType(Uri.parse(chat.getMessage()),"application/pdf");
                        holder.document_message.getContext().startActivity(documentIntent);
                    }
                });
                holder.document_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        receiverOptions(chat.getM_type(),documentName,documentURL,"");
                        //downloadMedia(documentName,documentURL);
                        return true;
                    }
                });
            }else if(("audio").equals(chat.getM_type())){
                holder.audio_message_relative.setVisibility(View.VISIBLE);
                holder.audio_message_time.setText(chat.getMessage_Time());
                final String audioURL = chat.getMessage();
                final String audioName = chat.getName();
                holder.audio_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.audio_message_relative.setVisibility(View.GONE);
                        holder.audio_message_controls.setVisibility(View.VISIBLE);
                        final MediaPlayer mediaPlayer = new MediaPlayer();
                        try {
                            mediaPlayer.setDataSource(holder.itemView.getContext(),Uri.parse(chat.getMessage()));
                            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {
                                    holder.audio_message_seekbar.setMax(mp.getDuration());
                                    mp.start();
                                    //holder.audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_pause);
                                    seekBarStatus();
                                }

                                private void seekBarStatus() {
                                    holder.audio_message_seekbar.setProgress(mediaPlayer.getCurrentPosition());
                                    if(mediaPlayer.isPlaying()){
                                        holder.audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_pause);
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                seekBarStatus();
                                            }
                                        };
                                        Handler handler = new Handler();
                                        handler.postDelayed(runnable,2000);
                                    }else if(!mediaPlayer.isPlaying()){
                                        holder.audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_play);
                                        seekBarStatus();
                                    }
                                }
                            });
                            holder.audio_message_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                @Override
                                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                    if(fromUser){
                                        mediaPlayer.seekTo(progress);
                                    }
                                }

                                @Override
                                public void onStartTrackingTouch(SeekBar seekBar) {

                                }

                                @Override
                                public void onStopTrackingTouch(SeekBar seekBar) {

                                }
                            });
                            mediaPlayer.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                holder.audio_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        receiverOptions(chat.getM_type(),audioName,audioURL,"");
                        //downloadMedia(audioName,audioURL);
                        return true;
                    }
                });
            }
            else if(("video").equals(chat.getM_type())){
                holder.video_message_relative.setVisibility(View.VISIBLE);
                holder.video_message_time.setText(chat.getMessage_Time());
                holder.videoMediaController = new MediaController(holder.itemView.getContext());
                holder.video_message.setMediaController(holder.videoMediaController);
                holder.videoMediaController.setAnchorView(holder.video_message);
                holder.video_message.setVideoURI(Uri.parse(chat.getMessage()));
                final String videoURL = chat.getMessage();
                final String videoName = chat.getName();
                holder.video_message_full_screen_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(holder.video_message_full_screen_btn.getContext(), FullScreenVideoActivity.class);
                        intent.putExtra("url", videoURL);
                        holder.video_message_full_screen_btn.getContext().startActivity(intent);
                    /*metrics = new DisplayMetrics();
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) holder.video_message.getLayoutParams();
                    params.width = metrics.widthPixels;
                    params.height = metrics.heightPixels;
                    holder.video_message.setLayoutParams(params);*/
                    }
                });
                //holder.video_message.start();
                holder.video_message.seekTo(1);
                holder.video_message.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        //mp.start();
                    }
                });
                holder.video_message_relative.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        receiverOptions(chat.getM_type(),videoName,videoURL,"");
                        //downloadMedia(videoName,videoURL);
                        return true;
                    }
                });
            }else if(("camera").equals(chat.getM_type())){
                holder.camera_message_relative.setVisibility(View.VISIBLE);
                Glide.with(context).load(chat.getMessage()).into(holder.camera_message);
                holder.camera_message_time.setText(chat.getMessage_Time());
                final String cameraImageURL = chat.getMessage();
                final String cameraImageName = chat.getName();
                holder.camera_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent fullScreenIntent = new Intent(holder.camera_message.getContext(),FullScreenImageActivity.class);
                        fullScreenIntent.putExtra("ImageURL",cameraImageURL);
                        holder.camera_message.getContext().startActivity(fullScreenIntent);
                    }
                });
                holder.camera_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        receiverOptions(chat.getM_type(),cameraImageName,cameraImageURL,"");
                        //downloadMedia(cameraImageName,cameraImageURL);
                        return true;
                    }
                });
            }else if(("deleted").equals(chat.getM_type())){
                holder.deleted_message_relative.setVisibility(View.VISIBLE);
                holder.deleted_message.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
                holder.deleted_message.setText("This message was deleted");
                holder.deleted_message_time.setText(chat.getMessage_Time());
            }
        }
        /*else if(chat.getM_type().equals("video")){
            holder.exoPlayerView.setVisibility(View.VISIBLE);
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
            holder.exoPlayer = ExoPlayerFactory.newSimpleInstance(holder.itemView.getContext(),trackSelector);
            DefaultHttpDataSourceFactory defaultHttpDataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(chat.getMessage()),defaultHttpDataSourceFactory,extractorsFactory,null,null);
            holder.exoPlayerView.setPlayer(holder.exoPlayer);
            holder.exoPlayer.prepare(mediaSource);
            holder.exoPlayer.setPlayWhenReady(true);
        }*/
        /*else if(chat.getM_type().equals("video")){
            holder.full_screen_video_message.setVisibility(View.VISIBLE);
            holder.full_screen_video_message.videoUrl(String.valueOf(Uri.parse(chat.getMessage())))
                    .addSeekBackwardButton()
                    .rewindSeconds(5)
                    .addSeekForwardButton()
                    .fastForwardSeconds(5);
        }*/
    }

    private void senderOptions(final String message_key, final String message_time, final String message_type, final String mName, final String mURL, final String chatMessage, final String sender, final String receiver){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Options");
        String[] optionsMenu = {"Delete for Everyone","Copy","Forward","Cancel"};
        String[] mediaOptionsMenu = {"Delete for Everyone","Download","Forward","Cancel"};
        if(message_type.equals("text")) {
            builder.setItems(optionsMenu, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            try {
                                deleteMessage(message_key, message_time,sender,receiver);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 1:
                            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData data = ClipData.newPlainText("Message",chatMessage);
                            clipboardManager.setPrimaryClip(data);
                            Toast.makeText(context, "Coiped!", Toast.LENGTH_SHORT).show();
                            break;
                        case 2:
                            forward(message_type,chatMessage);
                            break;
                        case 3:
                            break;
                        default:

                    }
                }
            });
        }else {
            builder.setItems(mediaOptionsMenu, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            try {
                                deleteMessage(message_key, message_time,sender,receiver);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 1:
                            downloadMedia(mName,mURL);
                            break;
                        case 2:
                            forward(message_type,mURL,mName);
                            break;
                        case 3:
                            break;
                        default:

                    }
                }
            });
        }
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void forward(String message_type, String mediaURL, String mediaName) {
        Intent intent = new Intent(context,ForwardActivity.class);
        intent.putExtra("message_type",message_type);
        intent.putExtra("message",mediaURL);
        intent.putExtra("name",mediaName);
        context.startActivity(intent);
    }

    private void forward(String message_type,String message) {
        Intent intent = new Intent(context, ForwardActivity.class);
        intent.putExtra("message_type",message_type);
        intent.putExtra("message",message);
        context.startActivity(intent);
    }

    private void receiverOptions(final String message_type, final String mName, final String mURL, final String chatMessage){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Options");
        String[] optionsMenu = {"Copy","Forward","Cancel"};
        String[] mediaOptionsMenu = {"Download","Forward","Cancel"};
        if(message_type.equals("text")) {
            builder.setItems(optionsMenu, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData data = ClipData.newPlainText("Message",chatMessage);
                            clipboardManager.setPrimaryClip(data);
                            Toast.makeText(context, "Coiped!", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            forward(message_type,chatMessage);
                            break;
                        case 2:
                            break;
                        default:
                    }
                }
            });
        }else {
            builder.setItems(mediaOptionsMenu, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            downloadMedia(mName,mURL);
                            break;
                        case 1:
                            forward(message_type,mURL,mName);
                            break;
                        case 2:
                            break;
                        default:
                    }
                }
            });
        }
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteMessage(String messageKey,String messageTime,String s,String r) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        String currentDateTime = format.format(calendar.getTime());
        Date messageDateTime = format.parse(messageTime);
        Calendar c = Calendar.getInstance();
        c.setTime(messageDateTime);
        c.add(Calendar.MINUTE,10);
        String deadline = format.format(c.getTime());
        if(format.parse(currentDateTime).before(format.parse(deadline))) {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Chats");
            HashMap<String, Object> message_hashmap = new HashMap<>();
            message_hashmap.put("m_type", "deleted");
            message_hashmap.put("message", "");
            //message_hashmap.put("message_status","");
            reference.child(s).child(r).child(messageKey).updateChildren(message_hashmap);
            reference.child(r).child(s).child(messageKey).updateChildren(message_hashmap);
            //reference.child(messageKey).removeValue();
            notifyDataSetChanged();
        }else {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("This Message could only be deleted till " + deadline)
                    .setCancelable(false)
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            //Toast.makeText(context, "Current Time :" + currentDateTime + "Message Deadline :" + deadline, Toast.LENGTH_SHORT).show();
        }
    }

    private void downloadMedia(String mediaName, String mediaURL) {
        Uri downloadUri = Uri.parse(mediaURL);
        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        if(manager != null){
            DownloadManager.Request request = new DownloadManager.Request(downloadUri);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                    .setTitle(mediaName)
                    .setDescription("Download in Progress..." + mediaName)
                    .setAllowedOverMetered(true)
                    .setAllowedOverRoaming(true)
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,mediaName);
            manager.enqueue(request);
            Toast.makeText(context, "Hold On...Downloading", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return chatsList.size();
    }

    public class  ViewHolder extends RecyclerView.ViewHolder{

        public RelativeLayout text_message_relative,image_message_relative,document_message_relative,audio_message_relative,video_message_relative,camera_message_relative,deleted_message_relative,audio_message_controls;
        public TextView text_message,deleted_message;
        public TextView text_message_time,image_message_time,document_message_time,audio_message_time,video_message_time,camera_message_time,deleted_message_time;
        public ImageView image_message,document_message,audio_message,camera_message;
        public VideoView video_message;
        public ImageButton video_message_full_screen_btn;
        public ImageButton audio_message_play_pause_btn;
        public SeekBar audio_message_seekbar;
        public MediaController videoMediaController;
        public TextView document_message_doc_name;
        public ImageView text_message_status,image_message_status,document_message_status,audio_message_status,video_message_status,camera_message_status;
        public TextView chat_message_date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_message_relative = itemView.findViewById(R.id.text_message_relative);
            image_message_relative = itemView.findViewById(R.id.image_message_relative);
            document_message_relative = itemView.findViewById(R.id.document_message_relative);
            audio_message_relative = itemView.findViewById(R.id.audio_message_relative);
            video_message_relative = itemView.findViewById(R.id.video_message_relative);
            camera_message_relative = itemView.findViewById(R.id.camera_message_relative);
            deleted_message_relative = itemView.findViewById(R.id.deleted_message_relative);
            audio_message_controls = itemView.findViewById(R.id.audio_message_controls);
            text_message = itemView.findViewById(R.id.text_message);
            text_message = itemView.findViewById(R.id.text_message);
            image_message = itemView.findViewById(R.id.image_message);
            document_message = itemView.findViewById(R.id.document_message);
            audio_message = itemView.findViewById(R.id.audio_message);
            video_message = itemView.findViewById(R.id.video_message);
            camera_message = itemView.findViewById(R.id.camera_message);
            deleted_message = itemView.findViewById(R.id.deleted_message);
            video_message_full_screen_btn = itemView.findViewById(R.id.video_message_full_screen_btn);
            audio_message_play_pause_btn = itemView.findViewById(R.id.audio_message_play_pause_btn);
            audio_message_seekbar = itemView.findViewById(R.id.audio_message_seekbar);
            text_message_time = itemView.findViewById(R.id.text_message_time);
            document_message_time = itemView.findViewById(R.id.document_message_time);
            image_message_time = itemView.findViewById(R.id.image_message_time);
            audio_message_time = itemView.findViewById(R.id.audio_message_time);
            video_message_time = itemView.findViewById(R.id.video_message_time);
            camera_message_time = itemView.findViewById(R.id.camera_message_time);
            deleted_message_time = itemView.findViewById(R.id.deleted_message_time);
            document_message_doc_name = itemView.findViewById(R.id.document_message_doc_name);
            text_message_status = itemView.findViewById(R.id.text_message_status);
            image_message_status = itemView.findViewById(R.id.image_message_status);
            document_message_status = itemView.findViewById(R.id.document_message_status);
            audio_message_status = itemView.findViewById(R.id.audio_message_status);
            video_message_status = itemView.findViewById(R.id.video_message_status);
            camera_message_status = itemView.findViewById(R.id.camera_message_status);
            chat_message_date = itemView.findViewById(R.id.chat_message_date);
        }
    }

    @Override
    public int getItemViewType(int position) {
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user.getUid().equals(chatsList.get(position).getSender())) {
            return MSG_SENT;
        }else {
            return MSG_RECEIVED;
        }
    }
}

