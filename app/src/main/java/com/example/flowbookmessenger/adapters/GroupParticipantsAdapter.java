package com.example.flowbookmessenger.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.flowbookmessenger.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupParticipantsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> participantsName;


    LayoutInflater inflater;

    public GroupParticipantsAdapter(Context context, ArrayList<String> participantsName) {
        this.context = context;
        this.participantsName = participantsName;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return participantsName.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.group_participants_gridview,null);
        CircleImageView group_participant_dp = v.findViewById(R.id.group_participant_dp);
        TextView group_participant_name = v.findViewById(R.id.group_participant_name);
        group_participant_dp.setImageResource(R.drawable.student_icon);
        group_participant_name.setText(participantsName.get(position));
        return v;
    }
}
