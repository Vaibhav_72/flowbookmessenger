package com.example.flowbookmessenger.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.activities.ChatActivity;
import com.example.flowbookmessenger.models.Chats;
import com.example.flowbookmessenger.models.Contacts;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private Context context;
    private List<Contacts> contacts_list;
    private boolean chatExists;
    private List<String> selectedContactID = new ArrayList<>();
    private List<String> selectedContactName = new ArrayList<>();
    private String last_msg;
    int unreadMsgCount;

    public ContactsAdapter(Context context, List<Contacts> contacts_list, boolean chatExists) {
        this.context = context;
        this.contacts_list = contacts_list;
        this.chatExists = chatExists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.contacts_row_layout,parent,false);
        return new ContactsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Contacts contacts = contacts_list.get(position);
        holder.contact_name.setText(contacts.getName());
        holder.contact_icon.setImageResource(R.drawable.student_icon);

        if(chatExists){
            //chatLastMessage(contacts.getUserid(),holder.chat_last_message,holder.chat_last_message_time,holder.chat_messages_unread_count,holder.chat_last_message_iv,holder.chat_last_message_status);
        }else {
            holder.chat_last_message.setVisibility(View.GONE);
            holder.chat_last_message_time.setVisibility(View.GONE);
            holder.chat_messages_unread_count.setVisibility(View.GONE);
            holder.chat_last_message_iv.setVisibility(View.GONE);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("userid",contacts.getUserid());
                context.startActivity(intent);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final String userID = contacts.getUserid();
                final String userName = contacts.getName();
                holder.contact_checkbox.setVisibility(View.VISIBLE);
                holder.contact_checkbox.setChecked(true);
                if (selectedContactID.contains(userID)){

                }else {
                    selectedContactID.add(userID);
                    selectedContactName.add(userName);
                }
                Toast.makeText(context, selectedContactName + " Selected", Toast.LENGTH_SHORT).show();
                holder.contact_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(holder.contact_checkbox.isChecked()){
                            selectedContactID.add(userID);
                            selectedContactName.add(userName);
                            Toast.makeText(context, selectedContactName + " Selected", Toast.LENGTH_SHORT).show();
                        }else {
                            if(selectedContactID.contains(userID)){
                                selectedContactID.remove(userID);
                                selectedContactName.remove(userName);
                            }
                            Toast.makeText(context, selectedContactName + "", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                //selectedContact.add(contacts);
                //Toast.makeText(context, selectedContact + " Selected", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        /*holder.contact_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final String userID = contacts.getUserid();
                final String userName = contacts.getName();
                if(holder.contact_checkbox.isChecked()){
                    selectedContactID.add(userID);
                    selectedContactName.add(userName);
                    Toast.makeText(context, selectedContactName + " Selected", Toast.LENGTH_SHORT).show();
                }else {
                    if(selectedContactID.contains(userID)){
                        selectedContactID.remove(userID);
                        selectedContactName.remove(userName);
                    }
                    Toast.makeText(context,  selectedContactName + "", Toast.LENGTH_SHORT).show();
                }
            }
        });*/
    }


    public ArrayList<String> getSelectedContactsID(){
        return (ArrayList<String>) selectedContactID;
    }

    public ArrayList<String> getSelectedContactsName(){
        return (ArrayList<String>) selectedContactName;
    }

    @Override
    public int getItemCount() {
        return contacts_list.size();
    }

    public class  ViewHolder extends RecyclerView.ViewHolder{

        public TextView contact_name;
        public CircleImageView contact_icon;
        public CheckBox contact_checkbox;
        public TextView chat_last_message,chat_last_message_time;
        public TextView chat_messages_unread_count;
        public ImageView chat_last_message_iv;
        public ImageView chat_last_message_status;
        //public FloatingActionButton contact_create_grp_btn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            contact_icon = (CircleImageView) itemView.findViewById(R.id.contact_row_icon);
            contact_name = (TextView) itemView.findViewById(R.id.contact_row_name);
            contact_checkbox = (CheckBox) itemView.findViewById(R.id.contact_checkbox);
            chat_last_message = (TextView) itemView.findViewById(R.id.chat_last_message);
            chat_last_message_time = (TextView)itemView.findViewById(R.id.chat_last_message_time);
            chat_messages_unread_count = (TextView) itemView.findViewById(R.id.chat_messages_unread_count);
            chat_last_message_iv = (ImageView) itemView.findViewById(R.id.chat_last_message_iv);
            chat_last_message_status = (ImageView) itemView.findViewById(R.id.chat_last_message_status);
            //contact_create_grp_btn = (FloatingActionButton) itemView.findViewById(R.id.contact_create_grp_btn);
        }

    }

    /*private void unreadMessageCount(String userID,TextView msgCount){

    }*/

    /*private void chatLastMessage(final String userID, final TextView lastMsgTV, final TextView chat_last_message_time, final TextView message_unread_counter, final ImageView lastMsgIV, final ImageView lastMsgStatus){
        Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        final String currentDate = format.format(calendar.getTime());
        unreadMsgCount = 0;
        last_msg = "no_msg";
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference chatsRef = FirebaseDatabase.getInstance().getReference("Chats");
        chatsRef.addValueEventListener(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot s : dataSnapshot.getChildren()){
                    Chats chats = s.getValue(Chats.class);
                    if(firebaseUser != null && chats != null){
                        if((chats.getReceiver().equals(firebaseUser.getUid()) && chats.getSender().equals(userID))
                                || (chats.getReceiver().equals(userID) && chats.getSender().equals(firebaseUser.getUid()))){
                            try {
                                if(format.parse(chats.getMessage_Date()).equals(format.parse(currentDate))){
                                    chat_last_message_time.setText(chats.getMessage_Time());
                                }else {
                                    chat_last_message_time.setText(chats.getMessage_Date());
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(chats.getReceiver().equals(firebaseUser.getUid()) && chats.getSender().equals(userID) && chats.getMessage_status().equals("sent")){
                                unreadMsgCount++;
                                message_unread_counter.setVisibility(View.VISIBLE);
                                message_unread_counter.setText(String.valueOf(unreadMsgCount));
                                lastMsgStatus.setVisibility(View.GONE);
                                lastMsgTV.setTextColor(R.color.greenNotify);
                                chat_last_message_time.setTextColor(R.color.greenNotify);
                                lastMsgTV.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                                chat_last_message_time.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                                if(chats.getM_type().equals("text")){
                                    lastMsgIV.setImageResource(R.drawable.text_sent);
                                    lastMsgTV.setText(chats.getMessage());
                                }else if(chats.getM_type().equals("document")){
                                    lastMsgIV.setImageResource(R.drawable.document_sent);
                                    lastMsgTV.setText("Document");
                                }else if(chats.getM_type().equals("gallery")){
                                    lastMsgIV.setImageResource(R.drawable.gallery_sent);
                                    lastMsgTV.setText("Photo");
                                }else if(chats.getM_type().equals("audio")){
                                    lastMsgIV.setImageResource(R.drawable.audio_sent);
                                    lastMsgTV.setText("Audio");
                                }else if(chats.getM_type().equals("video")){
                                    lastMsgIV.setImageResource(R.drawable.video_sent);
                                    lastMsgTV.setText("Video");
                                }else if(chats.getM_type().equals("deleted")){
                                    lastMsgIV.setImageResource(R.drawable.deleted_sent);
                                    lastMsgTV.setText("Message deleted");
                                }
                            }else if (chats.getReceiver().equals(firebaseUser.getUid()) && chats.getMessage_status().equals("seen")){
                                message_unread_counter.setVisibility(View.GONE);
                                lastMsgStatus.setVisibility(View.GONE);
                                if(chats.getM_type().equals("text")){
                                    lastMsgIV.setImageResource(R.drawable.text);
                                    lastMsgTV.setText(chats.getMessage());
                                }else if(chats.getM_type().equals("document")){
                                    lastMsgIV.setImageResource(R.drawable.document);
                                    lastMsgTV.setText("Document");
                                }else if(chats.getM_type().equals("gallery")){
                                    lastMsgIV.setImageResource(R.drawable.gallery);
                                    lastMsgTV.setText("Photo");
                                }else if(chats.getM_type().equals("audio")){
                                    lastMsgIV.setImageResource(R.drawable.audio);
                                    lastMsgTV.setText("Audio");
                                }else if(chats.getM_type().equals("video")){
                                    lastMsgIV.setImageResource(R.drawable.video);
                                    lastMsgTV.setText("Video");
                                }else if(chats.getM_type().equals("deleted")){
                                    lastMsgIV.setImageResource(R.drawable.deleted);
                                    lastMsgTV.setText("Message deleted");
                                }
                            }else if(chats.getSender().equals(firebaseUser.getUid())){
                                message_unread_counter.setVisibility(View.GONE);
                                lastMsgStatus.setVisibility(View.VISIBLE);
                                if(chats.getMessage_status().equals("sent")){
                                    lastMsgStatus.setImageResource(R.drawable.sent_row);
                                }else if(chats.getMessage_status().equals("seen")){
                                    lastMsgStatus.setImageResource(R.drawable.seen_row);
                                }
                                if(chats.getM_type().equals("text")){
                                    lastMsgIV.setImageResource(R.drawable.text);
                                    lastMsgTV.setText(chats.getMessage());
                                }else if(chats.getM_type().equals("document")){
                                    lastMsgIV.setImageResource(R.drawable.document);
                                    lastMsgTV.setText("Document");
                                }else if(chats.getM_type().equals("gallery")){
                                    lastMsgIV.setImageResource(R.drawable.gallery);
                                    lastMsgTV.setText("Photo");
                                }else if(chats.getM_type().equals("audio")){
                                    lastMsgIV.setImageResource(R.drawable.audio);
                                    lastMsgTV.setText("Audio");
                                }else if(chats.getM_type().equals("video")){
                                    lastMsgIV.setImageResource(R.drawable.video);
                                    lastMsgTV.setText("Video");
                                }else if(chats.getM_type().equals("deleted")){
                                    lastMsgStatus.setVisibility(View.GONE);
                                    lastMsgIV.setImageResource(R.drawable.deleted);
                                    lastMsgTV.setText("Message deleted");
                                }
                            }
                        }
                    }
                }

                last_msg = "no_msg";
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }*/
}