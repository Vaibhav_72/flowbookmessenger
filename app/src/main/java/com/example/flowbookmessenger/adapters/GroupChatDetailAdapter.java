package com.example.flowbookmessenger.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flowbookmessenger.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupChatDetailAdapter extends RecyclerView.Adapter<GroupChatDetailAdapter.ViewHolder> {

    Context context;
    ArrayList<String> stringArrayList;

    public GroupChatDetailAdapter(Context context, ArrayList<String> stringArrayList) {
        this.context = context;
        this.stringArrayList = stringArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v  = LayoutInflater.from(context).inflate(R.layout.group_detail_contacts_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.group_contact_row_name.setText(stringArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout group_contact_row_relative;
        CircleImageView group_contact_row_icon;
        TextView group_contact_row_name;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            group_contact_row_relative = itemView.findViewById(R.id.group_contact_row_relative);
            group_contact_row_icon = itemView.findViewById(R.id.group_contact_row_icon);
            group_contact_row_name = itemView.findViewById(R.id.group_contact_row_name);
        }
    }
}
