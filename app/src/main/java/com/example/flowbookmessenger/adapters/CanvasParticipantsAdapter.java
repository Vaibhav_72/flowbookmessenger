package com.example.flowbookmessenger.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flowbookmessenger.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CanvasParticipantsAdapter extends RecyclerView.Adapter<CanvasParticipantsAdapter.ViewHolder> {

    Context context;
    ArrayList<String> list;

    public CanvasParticipantsAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.canvas_participants_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.canvas_contact_row_name.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout canvas_contact_row_relative;
        CircleImageView canvas_contact_row_icon;
        TextView canvas_contact_row_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            canvas_contact_row_relative = itemView.findViewById(R.id.canvas_contact_row_relative);
            canvas_contact_row_icon = itemView.findViewById(R.id.canvas_contact_row_icon);
            canvas_contact_row_name = itemView.findViewById(R.id.canvas_contact_row_name);
        }
    }
}
