package com.example.flowbookmessenger.adapters;

import android.app.DownloadManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.activities.ForwardActivity;
import com.example.flowbookmessenger.activities.FullScreenImageActivity;
import com.example.flowbookmessenger.activities.FullScreenVideoActivity;
import com.example.flowbookmessenger.models.Contacts;
import com.example.flowbookmessenger.models.GroupChats;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class GroupChatsAdapter extends RecyclerView.Adapter<GroupChatsAdapter.ViewHolder> {

    private Context context;
    private List<GroupChats> groupChatsList;
    public static final int MSG_SENT = 0;
    public static final int MSG_RECEIVED = 1;

    FirebaseUser user;

    DatabaseReference reference;

    public GroupChatsAdapter(Context context, List<GroupChats> groupChatsList) {
        this.context = context;
        this.groupChatsList = groupChatsList;
    }

    @NonNull
    @Override
    public GroupChatsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == MSG_SENT){
            View v = LayoutInflater.from(context).inflate(R.layout.group_chat_send_message,parent,false);
            return new GroupChatsAdapter.ViewHolder(v);
        }else {
            View v = LayoutInflater.from(context).inflate(R.layout.group_chat_receive_message,parent,false);
            return new GroupChatsAdapter.ViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final GroupChatsAdapter.ViewHolder holder, int position) {
        final GroupChats groupChats = groupChatsList.get(position);
        user = FirebaseAuth.getInstance().getCurrentUser();
        holder.group_text_message_relative.setVisibility(View.GONE);
        holder.group_image_message_relative.setVisibility(View.GONE);
        holder.group_document_message_relative.setVisibility(View.GONE);
        holder.group_video_message_relative.setVisibility(View.GONE);
        holder.group_audio_message_relative.setVisibility(View.GONE);
        holder.group_deleted_message_relative.setVisibility(View.GONE);
        holder.group_audio_message_controls.setVisibility(View.GONE);
        holder.group_chat_message_date.setText(groupChats.getGroup_message_Date());
        if(user.getUid().equals(groupChats.getSender())){
            if(("text").equals(groupChats.getM_type())){
                holder.group_text_message_relative.setVisibility(View.VISIBLE);
                holder.group_text_message.setText(groupChats.getGroup_message());
                //holder.group_text_message_name.setText(groupChats.getSender());
                holder.group_text_message_time.setText(groupChats.getGroup_message_Time());
                if("seen".equals(groupChats.getGroup_message_status())){
                    holder.group_text_message_status.setImageResource(R.drawable.message_seen);
                }else if("sent".equals(groupChats.getGroup_message_status())){
                    holder.group_text_message_status.setImageResource(R.drawable.message_sent);
                }
                holder.group_text_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime(),groupChats.getM_type(),"","",groupChats.getGroup_message());
                        //optionsAlert(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime());
                        return true;
                    }
                });
            }else if(("document").equals(groupChats.getM_type())){
                holder.group_document_message_relative.setVisibility(View.VISIBLE);
                //holder.group_document_message_name.setText(groupChats.getSender());
                holder.group_document_message_doc_name.setText(groupChats.getName());
                final String groupDocumentURL = groupChats.getGroup_message();
                final String groupDocumentName = groupChats.getName();
                holder.group_document_message_time.setText(groupChats.getGroup_message_Time());
                if("seen".equals(groupChats.getGroup_message_status())){
                    holder.group_document_message_status.setImageResource(R.drawable.message_seen);
                }else if("sent".equals(groupChats.getGroup_message_status())){
                    holder.group_document_message_status.setImageResource(R.drawable.message_sent);
                }
                holder.group_document_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent grpDocumentIntent = new Intent();
                        grpDocumentIntent.setAction(Intent.ACTION_VIEW);
                        grpDocumentIntent.setDataAndType(Uri.parse(groupChats.getGroup_message()),"application/pdf");
                        holder.group_document_message.getContext().startActivity(grpDocumentIntent);
                    }
                });
                holder.group_document_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime(),groupChats.getM_type(),groupDocumentName,groupDocumentURL,"");
                        //optionsAlert(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime());
                        //downloadGroupMedia(groupDocumentName,groupDocumentURL);
                        return true;
                    }
                });
            }else if(("gallery").equals(groupChats.getM_type())){
                holder.group_image_message_relative.setVisibility(View.VISIBLE);
                //holder.group_image_message_name.setText(groupChats.getSender());
                Glide.with(context.getApplicationContext()).load(groupChats.getGroup_message()).into(holder.group_image_message);
                holder.group_image_message_time.setText(groupChats.getGroup_message_Time());
                if("seen".equals(groupChats.getGroup_message_status())){
                    holder.group_image_message_status.setImageResource(R.drawable.message_seen);
                }else if("sent".equals(groupChats.getGroup_message_status())){
                    holder.group_image_message_status.setImageResource(R.drawable.message_sent);
                }
                final String groupImageURL = groupChats.getGroup_message();
                final String groupImageName = groupChats.getName();
                holder.group_image_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent fullScreenImageIntent = new Intent(holder.group_image_message.getContext(), FullScreenImageActivity.class);
                        fullScreenImageIntent.putExtra("ImageURL",groupImageURL);
                        holder.group_image_message.getContext().startActivity(fullScreenImageIntent);
                    }
                });
                holder.group_image_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime(),groupChats.getM_type(),groupImageName,groupImageURL,"");
                        //optionsAlert(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime());
                        //downloadGroupMedia(groupImageName,groupImageURL);
                        return true;
                    }
                });
            }else if(("audio").equals(groupChats.getM_type())){
                holder.group_audio_message_relative.setVisibility(View.VISIBLE);
                //holder.group_audio_message_name.setText(groupChats.getSender());
                holder.group_audio_message_time.setText(groupChats.getGroup_message_Time());
                if("seen".equals(groupChats.getGroup_message_status())){
                    holder.group_audio_message_status.setImageResource(R.drawable.message_seen);
                }else if("sent".equals(groupChats.getGroup_message_status())){
                    holder.group_audio_message_status.setImageResource(R.drawable.message_sent);
                }
                final String groupAudioName = groupChats.getName();
                final String groupAudioUrl = groupChats.getGroup_message();
                holder.group_audio_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.group_audio_message_relative.setVisibility(View.GONE);
                        holder.group_audio_message_controls.setVisibility(View.VISIBLE);
                        final MediaPlayer mediaPlayer = new MediaPlayer();
                        try {
                            mediaPlayer.setDataSource(holder.itemView.getContext(), Uri.parse(groupChats.getGroup_message()));
                            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {
                                    holder.group_audio_message_seekbar.setMax(mp.getDuration());
                                    mp.start();
                                    groupSeekBarStatus();
                                }

                                private void groupSeekBarStatus() {
                                    holder.group_audio_message_seekbar.setProgress(mediaPlayer.getCurrentPosition());
                                    if(mediaPlayer.isPlaying()){
                                        holder.group_audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_pause);
                                        Runnable runnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                groupSeekBarStatus();
                                            }
                                        };
                                        Handler handler = new Handler();
                                        handler.postDelayed(runnable,2000);
                                    }else if(!(mediaPlayer.isPlaying())){
                                        holder.group_audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_play);
                                        groupSeekBarStatus();
                                    }
                                }
                            });
                            holder.group_audio_message_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                @Override
                                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                    if(fromUser){
                                        mediaPlayer.seekTo(progress);
                                    }
                                }

                                @Override
                                public void onStartTrackingTouch(SeekBar seekBar) {

                                }

                                @Override
                                public void onStopTrackingTouch(SeekBar seekBar) {

                                }
                            });
                            mediaPlayer.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                holder.group_audio_message.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime(),groupChats.getM_type(),groupAudioName,groupAudioUrl,"");
                        //optionsAlert(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime());
                        //downloadGroupMedia(groupAudioName,groupAudioUrl);
                        return true;
                    }
                });
            }else if(("video").equals(groupChats.getM_type())){
                holder.group_video_message_relative.setVisibility(View.VISIBLE);
                //holder.group_video_message_name.setText(groupChats.getSender());
                holder.group_video_message_time.setText(groupChats.getGroup_message_Time());
                if("seen".equals(groupChats.getGroup_message_status())){
                    holder.group_video_message_status.setImageResource(R.drawable.message_seen);
                }else if("sent".equals(groupChats.getGroup_message_status())){
                    holder.group_video_message_status.setImageResource(R.drawable.message_sent);
                }
                holder.groupVideoMediaController = new MediaController(holder.itemView.getContext());
                holder.group_video_message.setMediaController(holder.groupVideoMediaController);
                holder.groupVideoMediaController.setAnchorView(holder.group_video_message);
                holder.group_video_message.setVideoURI(Uri.parse(groupChats.getGroup_message()));
                final String groupVideoURL = groupChats.getGroup_message();
                final String groupVideoName = groupChats.getName();
                holder.group_video_message_full_screen_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent fullScreenIntent = new Intent(holder.group_video_message_full_screen_btn.getContext(), FullScreenVideoActivity.class);
                        fullScreenIntent.putExtra("url",groupVideoURL);
                        holder.group_video_message_full_screen_btn.getContext().startActivity(fullScreenIntent);
                    }
                });
                holder.group_video_message.seekTo(1);
                holder.group_video_message_relative.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        senderOptions(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime(),groupChats.getM_type(),groupVideoName,groupVideoURL,"");
                        //optionsAlert(groupChats.getGroup_message_key(),groupChats.getGroup_message_DateTime());
                        //downloadGroupMedia(groupVideoName,groupVideoURL);
                        return true;
                    }
                });
            }else if(("deleted").equals(groupChats.getM_type())){
                holder.group_deleted_message_relative.setVisibility(View.VISIBLE);
                holder.group_deleted_message.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
                holder.group_deleted_message.setText("You deleted this message");
                holder.group_deleted_message_time.setText(groupChats.getGroup_message_Time());
            }
        }else if(!(user.getUid().equals(groupChats.getSender()))){
            reference = FirebaseDatabase.getInstance().getReference().child("Users");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot s : dataSnapshot.getChildren()){
                        Contacts contacts = s.getValue(Contacts.class);
                        if(contacts.getUserid().equals(groupChats.getSender())){
                            String senderName = contacts.getName();
                            if(("text").equals(groupChats.getM_type())){
                                holder.group_text_message_relative.setVisibility(View.VISIBLE);
                                holder.group_text_message.setText(groupChats.getGroup_message());
                                holder.group_text_message_name.setText(senderName);
                                holder.group_text_message_time.setText(groupChats.getGroup_message_Time());
                                holder.group_text_message.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        receiverOptions(groupChats.getM_type(),"","",groupChats.getGroup_message());
                                        return true;
                                    }
                                });
                            }else if(("document").equals(groupChats.getM_type())){
                                holder.group_document_message_relative.setVisibility(View.VISIBLE);
                                holder.group_document_message_name.setText(senderName);
                                holder.group_document_message_doc_name.setText(groupChats.getName());
                                holder.group_document_message_time.setText(groupChats.getGroup_message_Time());
                                final String groupDocumentName = groupChats.getName();
                                final String groupDocumentURL = groupChats.getGroup_message();
                                holder.group_document_message.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent grpDocumentIntent = new Intent();
                                        grpDocumentIntent.setAction(Intent.ACTION_VIEW);
                                        grpDocumentIntent.setDataAndType(Uri.parse(groupChats.getGroup_message()),"application/pdf");
                                        holder.group_document_message.getContext().startActivity(grpDocumentIntent);
                                    }
                                });
                                holder.group_document_message.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        receiverOptions(groupChats.getM_type(),groupDocumentName,groupDocumentURL,"");
                                        //downloadGroupMedia(groupDocumentName,groupDocumentURL);
                                        return true;
                                    }
                                });
                            }else if(("gallery").equals(groupChats.getM_type())){
                                holder.group_image_message_relative.setVisibility(View.VISIBLE);
                                holder.group_image_message_name.setText(senderName);
                                Glide.with(context.getApplicationContext()).load(groupChats.getGroup_message()).into(holder.group_image_message);
                                holder.group_image_message_time.setText(groupChats.getGroup_message_Time());
                                final String groupImageURL = groupChats.getGroup_message();
                                final String groupImageName = groupChats.getName();
                                holder.group_image_message.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent fullScreenImageIntent = new Intent(holder.group_image_message.getContext(),FullScreenImageActivity.class);
                                        fullScreenImageIntent.putExtra("ImageURL",groupImageURL);
                                        holder.group_image_message.getContext().startActivity(fullScreenImageIntent);
                                    }
                                });
                                holder.group_image_message.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        receiverOptions(groupChats.getM_type(),groupImageName,groupImageURL,"");
                                        //downloadGroupMedia(groupImageName,groupImageURL);
                                        return true;
                                    }
                                });
                            }else if(("audio").equals(groupChats.getM_type())){
                                holder.group_audio_message_relative.setVisibility(View.VISIBLE);
                                holder.group_audio_message_name.setText(senderName);
                                holder.group_audio_message_time.setText(groupChats.getGroup_message_Time());
                                final String groupAudioName = groupChats.getName();
                                final String groupAudioURL = groupChats.getGroup_message();
                                holder.group_audio_message.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        holder.group_audio_message_relative.setVisibility(View.GONE);
                                        holder.group_audio_message_controls.setVisibility(View.VISIBLE);
                                        final MediaPlayer mediaPlayer = new MediaPlayer();
                                        try {
                                            mediaPlayer.setDataSource(holder.itemView.getContext(), Uri.parse(groupChats.getGroup_message()));
                                            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                                @Override
                                                public void onPrepared(MediaPlayer mp) {
                                                    holder.group_audio_message_seekbar.setMax(mp.getDuration());
                                                    mp.start();
                                                    groupSeekBarStatus();
                                                }

                                                private void groupSeekBarStatus() {
                                                    holder.group_audio_message_seekbar.setProgress(mediaPlayer.getCurrentPosition());
                                                    if(mediaPlayer.isPlaying()){
                                                        holder.group_audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_pause);
                                                        Runnable runnable = new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                groupSeekBarStatus();
                                                            }
                                                        };
                                                        Handler handler = new Handler();
                                                        handler.postDelayed(runnable,2000);
                                                    }else if(!(mediaPlayer.isPlaying())){
                                                        holder.group_audio_message_play_pause_btn.setImageResource(R.drawable.audio_message_play);
                                                        groupSeekBarStatus();
                                                    }
                                                }
                                            });
                                            holder.group_audio_message_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                                @Override
                                                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                                    if(fromUser){
                                                        mediaPlayer.seekTo(progress);
                                                    }
                                                }

                                                @Override
                                                public void onStartTrackingTouch(SeekBar seekBar) {

                                                }

                                                @Override
                                                public void onStopTrackingTouch(SeekBar seekBar) {

                                                }
                                            });
                                            mediaPlayer.prepare();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                holder.group_audio_message.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        receiverOptions(groupChats.getM_type(),groupAudioName,groupAudioURL,"");
                                        //downloadGroupMedia(groupAudioName,groupAudioURL);
                                        return true;
                                    }
                                });
                            }else if(("video").equals(groupChats.getM_type())){
                                holder.group_video_message_relative.setVisibility(View.VISIBLE);
                                holder.group_video_message_name.setText(senderName);
                                holder.group_video_message_time.setText(groupChats.getGroup_message_Time());
                                holder.groupVideoMediaController = new MediaController(holder.itemView.getContext());
                                holder.group_video_message.setMediaController(holder.groupVideoMediaController);
                                holder.groupVideoMediaController.setAnchorView(holder.group_video_message);
                                holder.group_video_message.setVideoURI(Uri.parse(groupChats.getGroup_message()));
                                final String groupVideoURL = groupChats.getGroup_message();
                                final String groupVideoName = groupChats.getName();
                                holder.group_video_message_full_screen_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent fullScreenIntent = new Intent(holder.group_video_message_full_screen_btn.getContext(),FullScreenVideoActivity.class);
                                        fullScreenIntent.putExtra("url",groupVideoURL);
                                        holder.group_video_message_full_screen_btn.getContext().startActivity(fullScreenIntent);
                                    }
                                });
                                holder.group_video_message.seekTo(1);
                                holder.group_video_message_relative.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        receiverOptions(groupChats.getM_type(),groupVideoName,groupVideoURL,"");
                                        //downloadGroupMedia(groupVideoName,groupVideoURL);
                                        return true;
                                    }
                                });
                            }else if(("deleted").equals(groupChats.getM_type())){
                                holder.group_deleted_message_relative.setVisibility(View.VISIBLE);
                                holder.group_deleted_message_name.setText(senderName);
                                holder.group_deleted_message.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
                                holder.group_deleted_message.setText("This message was deleted");
                                holder.group_deleted_message_time.setText(groupChats.getGroup_message_Time());
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    /*private void optionsAlert(final String group_message_key, final String group_message_time){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Options");
        String[] optionsMenu = {"Delete for Everyone","Cancel"};
        builder.setItems(optionsMenu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        try {
                            deleteGroupMessage(group_message_key,group_message_time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }*/

    private void senderOptions(final String group_message_key, final String group_message_time, final String group_message_type, final String mName, final String mURL, final String groupChatMessage){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Options");
        String[] optionsMenu = {"Delete for Everyone","Copy","Forward","Cancel"};
        String[] mediaOptionsMenu = {"Delete for Everyone","Download","Forward","Cancel"};
        if(group_message_type.equals("text")) {
            builder.setItems(optionsMenu, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            try {
                                deleteGroupMessage(group_message_key, group_message_time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 1:
                            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData data = ClipData.newPlainText("Message",groupChatMessage);
                            clipboardManager.setPrimaryClip(data);
                            Toast.makeText(context, "Coiped!", Toast.LENGTH_SHORT).show();
                            break;
                        case 2:
                            forward(group_message_type,groupChatMessage);
                            break;
                        case 3:
                            break;
                        default:

                    }
                }
            });
        }else {
            builder.setItems(mediaOptionsMenu, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            try {
                                deleteGroupMessage(group_message_key, group_message_time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 1:
                            downloadGroupMedia(mName,mURL);
                            break;
                        case 2:
                            forward(group_message_type,mURL,mName);
                            break;
                        case 3:
                            break;
                        default:

                    }
                }
            });
        }
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void forward(String group_message_type, String mediaURL, String mediaName) {
        Intent intent = new Intent(context, ForwardActivity.class);
        intent.putExtra("message_type",group_message_type);
        intent.putExtra("message",mediaURL);
        intent.putExtra("name",mediaName);
        context.startActivity(intent);
    }

    private void forward(String group_message_type, String groupChatMessage) {
        Intent intent = new Intent(context,ForwardActivity.class);
        intent.putExtra("message_type",group_message_type);
        intent.putExtra("message",groupChatMessage);
        context.startActivity(intent);
    }

    private void receiverOptions(final String group_message_type, final String mName, final String mURL, final String groupChatMessage){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Options");
        String[] optionsMenu = {"Copy","Forward","Cancel"};
        String[] mediaOptionsMenu = {"Download","Forward","Cancel"};
        if(group_message_type.equals("text")) {
            builder.setItems(optionsMenu, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData data = ClipData.newPlainText("Message",groupChatMessage);
                            clipboardManager.setPrimaryClip(data);
                            Toast.makeText(context, "Coiped!", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            forward(group_message_type,groupChatMessage);
                            break;
                        case 2:
                            break;
                        default:
                    }
                }
            });
        }else {
            builder.setItems(mediaOptionsMenu, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            downloadGroupMedia(mName,mURL);
                            break;
                        case 1:
                            forward(group_message_type,mURL,mName);
                            break;
                        case 2:
                            break;
                        default:

                    }
                }
            });
        }
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteGroupMessage(String groupMessageKey,String groupMessageTime) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        String currentDateTime = format.format(calendar.getTime());
        Date groupMessageDateTime = format.parse(groupMessageTime);
        Calendar c = Calendar.getInstance();
        c.setTime(groupMessageDateTime);
        c.add(Calendar.MINUTE,10);
        String deadline = format.format(c.getTime());
        if(format.parse(currentDateTime).before(format.parse(deadline))) {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Group Chats");
            HashMap<String, Object> group_message_hashmap = new HashMap<>();
            group_message_hashmap.put("m_type", "deleted");
            group_message_hashmap.put("group_message", "");
            //message_hashmap.put("message_status","");
            reference.child(groupMessageKey).updateChildren(group_message_hashmap);
            //reference.child(messageKey).removeValue();
            notifyDataSetChanged();
        }else {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("This Message could only be deleted till " + deadline)
                    .setCancelable(false)
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            //Toast.makeText(context, "Current Time :" + currentDateTime + "Message Deadline :" + deadline, Toast.LENGTH_SHORT).show();
        }
    }

    private void downloadGroupMedia(String mediaName, String mediaURL) {
        Uri downloadUri = Uri.parse(mediaURL);
        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        if(manager != null){
            DownloadManager.Request request = new DownloadManager.Request(downloadUri);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                    .setTitle(mediaName)
                    .setDescription("Download in Progress..." + mediaName)
                    .setAllowedOverMetered(true)
                    .setAllowedOverRoaming(true)
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,mediaName);
            manager.enqueue(request);
            Toast.makeText(context, "Hold On...Downloading", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return groupChatsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView group_text_message,group_deleted_message;
        TextView group_text_message_name,group_image_message_name,group_document_message_name,group_audio_message_name,group_video_message_name,group_deleted_message_name;
        TextView group_text_message_time,group_image_message_time,group_document_message_time,group_audio_message_time,group_video_message_time,group_deleted_message_time;
        ImageView group_image_message,group_document_message,group_audio_message;
        ImageButton group_video_message_full_screen_btn;
        VideoView group_video_message;
        RelativeLayout group_text_message_relative,group_image_message_relative,group_document_message_relative,group_audio_message_relative,group_video_message_relative,group_deleted_message_relative;
        public MediaController groupVideoMediaController;
        RelativeLayout group_audio_message_controls;
        ImageButton group_audio_message_play_pause_btn;
        SeekBar group_audio_message_seekbar;
        TextView group_document_message_doc_name;
        ImageView group_text_message_status,group_image_message_status,group_document_message_status,group_audio_message_status,group_video_message_status;
        TextView group_chat_message_date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            group_text_message_relative = (RelativeLayout) itemView.findViewById(R.id.group_text_message_relative);
            group_image_message_relative = (RelativeLayout) itemView.findViewById(R.id.group_image_message_relative);
            group_document_message_relative = (RelativeLayout) itemView.findViewById(R.id.group_document_message_relative);
            group_audio_message_relative = (RelativeLayout) itemView.findViewById(R.id.group_audio_message_relative);
            group_video_message_relative = (RelativeLayout) itemView.findViewById(R.id.group_video_message_relative);
            group_deleted_message_relative = (RelativeLayout) itemView.findViewById(R.id.group_deleted_message_relative);
            group_text_message_name = (TextView) itemView.findViewById(R.id.group_text_message_name);
            group_image_message_name = (TextView) itemView.findViewById(R.id.group_image_message_name);
            group_document_message_name = (TextView) itemView.findViewById(R.id.group_document_message_name);
            group_video_message_name = (TextView) itemView.findViewById(R.id.group_video_message_name);
            group_audio_message_name = (TextView) itemView.findViewById(R.id.group_audio_message_name);
            group_deleted_message_name = (TextView) itemView.findViewById(R.id.group_deleted_message_name);
            group_text_message = (TextView) itemView.findViewById(R.id.group_text_message);
            group_image_message = (ImageView) itemView.findViewById(R.id.group_image_message);
            group_document_message = (ImageView) itemView.findViewById(R.id.group_document_message);
            group_audio_message = (ImageView) itemView.findViewById(R.id.group_audio_message);
            group_deleted_message = (TextView) itemView.findViewById(R.id.group_deleted_message);
            group_video_message_full_screen_btn = (ImageButton) itemView.findViewById(R.id.group_video_message_full_screen_btn);
            group_video_message = (VideoView) itemView.findViewById(R.id.group_video_message);
            group_audio_message_controls = (RelativeLayout) itemView.findViewById(R.id.group_audio_message_controls);
            group_audio_message_play_pause_btn = (ImageButton) itemView.findViewById(R.id.group_audio_message_play_pause_btn);
            group_audio_message_seekbar = (SeekBar) itemView.findViewById(R.id.group_audio_message_seekbar);
            group_text_message_time = (TextView) itemView.findViewById(R.id.group_text_message_time);
            group_document_message_time = (TextView) itemView.findViewById(R.id.group_document_message_time);
            group_image_message_time = (TextView) itemView.findViewById(R.id.group_image_message_time);
            group_audio_message_time = (TextView) itemView.findViewById(R.id.group_audio_message_time);
            group_video_message_time = (TextView) itemView.findViewById(R.id.group_video_message_time);
            group_deleted_message_time = (TextView) itemView.findViewById(R.id.group_deleted_message_time);
            group_document_message_doc_name = (TextView) itemView.findViewById(R.id.group_document_message_doc_name);
            group_text_message_status = (ImageView) itemView.findViewById(R.id.group_text_message_status);
            group_image_message_status = (ImageView) itemView.findViewById(R.id.group_image_message_status);
            group_document_message_status = (ImageView) itemView.findViewById(R.id.group_document_message_status);
            group_audio_message_status = (ImageView) itemView.findViewById(R.id.group_audio_message_status);
            group_video_message_status = (ImageView) itemView.findViewById(R.id.group_video_message_status);
            group_chat_message_date = (TextView) itemView.findViewById(R.id.group_chat_message_date);
        }
    }

    @Override
    public int getItemViewType(int position) {
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user.getUid().equals(groupChatsList.get(position).getSender())) {
            return MSG_SENT;
        }else {
            return MSG_RECEIVED;
        }
    }
}
