package com.example.flowbookmessenger.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flowbookmessenger.activities.CanvasDrawActivity;
import com.example.flowbookmessenger.R;
import com.example.flowbookmessenger.models.Canvas;

import java.util.List;

public class CanvasDisplayAdapter extends RecyclerView.Adapter<CanvasDisplayAdapter.ViewHolder>{

    Context context;
    List<Canvas> canvasList;

    public CanvasDisplayAdapter(Context context, List<Canvas> canvasList) {
        this.context = context;
        this.canvasList = canvasList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.canvas_layout,parent,false);
        return new CanvasDisplayAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Canvas canvas = canvasList.get(position);
        holder.canvas_title.setText(canvas.getCanvasName());
        holder.canvas_date_time.setText(canvas.getCanvasDate() + " (" + canvas.getCanvasTime() + ")");
        holder.canvas_creator_name.setText(canvas.getCreatorName());
        holder.canvas_participants_count.setText(canvas.getCanvasParticipantsID().size() + " Participants");
        //holder.canvas_thumbnail.setImageResource(R.drawable.flowbook_logo);
        holder.canvas_thumbnail.setImageBitmap(CanvasDrawActivity.decodeCanvasFromBase64(canvas.getCanvasThumbnail()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CanvasDrawActivity.class);
                intent.putExtra("canvasID",canvas.getKey());
                intent.putExtra("canvasName",canvas.getCanvasName());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return canvasList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView canvas_title;
        ImageView canvas_thumbnail;
        TextView canvas_date_time;
        TextView canvas_creator_name;
        TextView canvas_participants_count;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            canvas_title = itemView.findViewById(R.id.canvas_title);
            canvas_thumbnail = itemView.findViewById(R.id.canvas_thumbnail);
            canvas_date_time = itemView.findViewById(R.id.canvas_date_time);
            canvas_creator_name = itemView.findViewById(R.id.canvas_creator_name);
            canvas_participants_count = itemView.findViewById(R.id.canvas_participants_count);
        }
    }
}
