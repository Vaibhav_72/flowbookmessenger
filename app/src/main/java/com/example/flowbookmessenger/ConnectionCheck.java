package com.example.flowbookmessenger;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class ConnectionCheck {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            if (Build.VERSION.SDK_INT < 23) {
                final NetworkInfo ni = cm.getActiveNetworkInfo();
                if (ni != null) {
                    return ni.isConnected() && canConnectToGoogle();
                }
            } else {
                final Network n = cm.getActiveNetwork();
                if (n != null) {
                    final NetworkCapabilities nc = cm.getNetworkCapabilities(n);
                    return nc != null && nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED) && canConnectToGoogle();
                }
            }
        }
        return false;
    }

    private static boolean canConnectToGoogle() {
        try {
            return new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    try {
                        Socket socket = new Socket();
                        socket.connect(new InetSocketAddress("8.8.8.8", 53), 500);
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (e instanceof SocketTimeoutException) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }.execute().get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
