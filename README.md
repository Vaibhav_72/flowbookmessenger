**FlowBook Messenger**


*	FlowBook Messenger features : One to One Chat, Group Chat & Live Sessions.


*	Supported Message Types : Text, Document, Image, Video & Audio. 


*	Multiple Options for Messages like Copy a message, Forward a message, Download (in case of media messages) & Revoke the message within the timeline.


*	Contact details won't be shared directly as the users signing up on the app are assigned a unique ID and the complete app works on it, so the details of the students & mentors won't be shared.


*	Live session feature allows the mentors to host live sessions so that they can help the students with their doubts and provide a visual explaination of the concepts.

Below enclosed screenshots will explain the features of the app in a much better and simplified manner.


![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/Chats.jpg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/Contacts.jpg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/Groups.jpg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/Notifications.jpg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/available_sessions.jpg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/group_chat1.jpg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/group_chat2.jpg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/group_deatils.jpg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/live_session.jpg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/live_session_multi_device.jpeg)
![ImgName](https://bitbucket.org/Vaibhav_72/flowbookmessenger/raw/master/screenshots/one_to_one_chat.jpg)

Also do checkout Screenshots folder for some more screenshots explaining some other features of the project. 

Happy Learning.
